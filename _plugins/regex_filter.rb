# A Filter to do regex subst
# via sameers @ https://stackoverflow.com/a/25802375/3366053
module Jekyll
  module RegexFilter
    def replace_regex(input, reg_str, repl_str)
      re = Regexp.new reg_str, Regexp::MULTILINE

      return input.gsub re, repl_str
    end
  end
end

Liquid::Template.register_filter(Jekyll::RegexFilter)
