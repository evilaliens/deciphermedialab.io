# This is a monkey patch on the Jekyll responsive_image plugin: https://github.com/wildlyinaccurate/jekyll-responsive-image
# which adds the ability to expand a variable from the `image` param into its value
#
# Thus, where a normal use of the responsive_image tag would be:
#   ```liquid
#   {% responsive_image path: path/to/image %}
#   ```
#
#   This tag would take a data structure and fill out the required `path` param from there
#   ```yaml
#   # From some frontmatter, for instance
#   image:
#     path: path/to/image
#     alt: 'alt text for an image'
#     arbitrary_param: 'arbitrary value'
#   ```
#   ```liquid
#   {% imagebydata image: page.image %}
#   ```
module Jekyll
  class ImageByData < ResponsiveImage::Tag
    include Jekyll::LiquidExtensions
    def initialize(tag_name, markup, tokens)
      @markup = markup
      super
    end
    def render(context)
      imagedata = lookup_variable(context, @attributes['image'])
      imagedata.each do |field, value|
        @attributes[field] = value
      end

      super
    end
  end
end

Liquid::Template.register_tag('imagebydata', Jekyll::ImageByData)
