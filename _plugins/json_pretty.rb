module Jekyll
  module JsonPretty
    def json_pretty(text)
      require 'json'

      JSON.pretty_generate(JSON.parse(text))
    end
  end
end
Liquid::Template.register_filter(Jekyll::JsonPretty)
