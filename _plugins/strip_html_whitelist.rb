# Jekyll custom filter to remove non-permitted HTML elements from RSS content:encoded
# as per Apple Podcast specs https://help.apple.com/itc/podcasts_connect/#/itcb54353390

require 'sanitize'
module Jekyll
  module StripHtmlItunesWhitelist
    def strip_html_itunes_whitelist(input)
			# This is the list of permitted tags from apple docs
			permitted_elems = ['p', 'ol', 'ul', 'li', 'a']

			# The contents of these should go away along with the element
			remove_elems = ['script', 'style']

			processed = Sanitize.fragment(
				input,
				:elements => permitted_elems,
				:remove_contents => remove_elems,
				:attributes => {
					'a' => ['href', 'rel'], # rel is important here to support Overcast's donate link method
				},
			)
			return processed
    end
  end
end

Liquid::Template.register_filter(Jekyll::StripHtmlItunesWhitelist)
