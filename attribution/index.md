---
layout: page
title: Attribution - The stuff we use to make our stuff
---

## Attribution if you use our works

Please credit `Decipher Media`, link back to [deciphermedia.tv](https://deciphermedia.tv), and point to the [CC-BY-NC-4.0 license](https://creativecommons.org/licenses/by-nc/4.0/). Here is an example:

```
"Name Of Work/Episode/Etc" by <a href="{{ site.url }}">Decipher Media</a> is licensed under <a href="https://creativecommons.org/licenses/by-nc/4.0/">CC-BY-NC-4.0</a>
```

## Things upon which we rely

{% for attr_category in site.data.resources %}
  {% assign cat_size = attr_category | size %}
  {% if cat_size > 0 %}
#### {{ attr_category[0] | capitalize }}

```json
{{ attr_category[1] | jsonify | json_pretty }}
```
  {% endif %}
{% endfor %}
