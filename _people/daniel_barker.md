---
layout: person_page
person_data:
  name: Daniel Barker
  data_name: daniel_barker
  image_path: assets/imgs/people/daniel_barker.png
  title: Science communicator, Podcaster
  bio: 'Daniel knows things about things and is the brains behind some seriously-good science podcasts.'
  websites:
    - name: Uncertainty Principle The Podcast
      url: 'https://soundcloud.com/uncertaintyprinciple'
    - name: Game Science
      url: 'https://soundcloud.com/gamescience'
  social:
    - facebook: gamesciencepodcast
  roles:
    - show: decipherscifi
      role: answerer
    - show: decipherscifi
      role: guest_cohost
---
