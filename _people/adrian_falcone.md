---
layout: person_page
person_data:
  name: Adrian Falcone
  data_name: adrian_falcone
  image_path: assets/imgs/people/adrian_falcone.png
  title: Internet Nerd
  bio: 'He loves him some math and physics and we love taking advantage of his enthusiasm and expertise whenever possible. One of our first listeners now become one of our most-featured co-hosts!'
  social:
    - name: twitter
      url: https://www.twitter.com/adrianbfalcone
    - name: instagram
      url: https://www.instagram.com/pleaseapostrophe
  roles:
    - show: decipherscifi
      role: answerer
    - show: decipherscifi
      role: guest_cohost
---
