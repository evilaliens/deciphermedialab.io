---
layout: person_page
person_data:
  name: Liam Ginty
  data_name: liam_ginty
  image_path: assets/imgs/people/liam_ginty.png
  title: Nerd
  bio: 'Noted space nerd podcaster, now RPG publishing mogul.'
  websites:
    - name: Voices From L5
      url: 'https://www.patreon.com/VoicesFromL5/posts'
    - name: Sandy Pug Games
      url: 'http://www.drivethrurpg.com/browse/pub/7386/Sandy-Pug-Games'
  social:
    - name: twitter
      url: https://www.twitter.com/sandypuggames
    - facebook: sandypuggames
  roles:
    - show: decipherscifi
      role: guest_cohost
    - show: decipherrpg
      role: host
---
