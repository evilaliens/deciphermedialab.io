---
layout: person_page
person_data:
  name: Brandon Rollins
  data_name: brandon_rollins
  image_path: assets/imgs/people/brandon_rollins.png
  title: Game Designer
  bio: 'He is probably, as ever, working on another game. He also keeps a really great developer log where he goes deep on all of his successes and mistakes and anyone who makes things can probably get something out of it.'
  websites:
    - name: Brandon the Game Dev
      url: 'http://brandonthegamedev.com/'
    - name: War Co., The Expandable Card Game
      url: 'http://warcothegame.com/'
  social:
    - name: twitter
      url: https://www.twitter.com/brandongamedev
    - name: instagram
      url: https://www.instagram.com/brandongamedev
    - facebook: brandongamedev
  roles:
    - show: decipherscifi
      role: guest_cohost
---
