---
layout: person_page
person_data:
  name: Fraser Cain
  data_name: fraser_cain
  image_path: assets/imgs/people/fraser_cain.png
  title: Space Science Communicator
  bio: 'Fraser has been on the internet talking about space for a while. He’s really good at it now.'
  websites:
    - name: Astronomy Cast
      url: 'http://www.astronomycast.com/'
    - name: 'Fraser Cain @ YouTube'
      url: 'https://www.youtube.com/user/universetoday'
    - name: Universe Today
      url: 'https://www.universetoday.com/'
  social:
    - name: twitter
      url: https://www.twitter.com/fcain
    - name: instagram
      url: https://www.instagram.com/fcain
    - facebook: fraser.cain
  roles:
    - show: decipherscifi
      role: answerer
    - show: decipherscifi
      role: guest_cohost
---
