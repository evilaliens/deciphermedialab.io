---
layout: person_page
person_data:
  name: Ryan Bingham
  data_name: ryan_bingham
  image_path: assets/imgs/people/ryan_bingham.png
  title: Lawyer Nerd
  bio: 'Boring corporate lawyer by day, nerd on the internet by night.'
  social:
  roles:
    - show: decipherscifi
      role: guest_cohost
---
