---
layout: person_page
person_data:
  name: Joe Ruppel
  data_name: joe_ruppel
  image_path: assets/imgs/people/joe_ruppel.png
  title: Co-BFF
  bio: 'He has a history degree that has never been useful except when he’s recording the podcast with us. He also spends an awful lot of time thinking about and then riding motorcycles. Loves Arnold Schwarzenegger.'
  roles:
    - show: decipherscifi
      role: guest_cohost
---
