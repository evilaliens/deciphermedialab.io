---
layout: person_page
person_data:
  name: Lee Accomando
  data_name: lee_accomando
  image_path: assets/imgs/people/lee_accomando.png
  title: Viking Enthusiast
  bio: 'Professional nerd and amateur historian with a keen interest in viking history.'
  websites:
    - name: Viking Age Podcast
      url: 'http://vikingagepodcast.com/'
  social:
    - name: twitter
      url: https://www.twitter.com/vikingagepod
    - facebook: vikingagepod
  roles:
    - show: decipherscifi
      role: guest_cohost
---
