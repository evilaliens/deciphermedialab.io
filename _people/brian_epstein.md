---
layout: person_page
person_data:
  name: Brian Epstein
  data_name: brian_epstein
  image_path: assets/imgs/people/brian_epstein.png
  title: Information Security Professional
  bio: 'The leader for Network Security at the Institute for Advanced Study and lifelong hacker nerd.'
  websites:
    - name: IAS Security
      url: 'https://security.ias.edu/'
  social:
    - name: twitter
      url: https://www.twitter.com/epepepep
    - name: instagram
      url: https://www.instagram.com/epepepepepep
  roles:
    - show: decipherscifi
      role: guest_cohost
---
