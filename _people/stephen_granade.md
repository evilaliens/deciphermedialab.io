---
layout: person_page
person_data:
  name: Stephen Granade
  data_name: stephen_granade
  image_path: assets/imgs/people/stephen_granade.png
  title: Scientist
  bio: 'He says it well enough on his own website: "I’m a physicist, speaker, and recovering writer. I think science is awesome and should be accessible to everyone. I believe entertainment and education can complement each other. I love creating things and want everyone to take part."'
  websites:
    - name: Stephen Granade
      url: 'http://stephen.granades.com/'
  social:
    - name: twitter
      url: https://www.twitter.com/sargent
    - facebook: stephen.granade.5
  roles:
    - show: decipherscifi
      role: guest_cohost
---
