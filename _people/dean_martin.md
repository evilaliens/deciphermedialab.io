---
layout: person_page
person_data:
  name: Dean Martin
  data_name: dean_martin
  image_path: assets/imgs/people/dean_martin.png
  title: Podcaster
  bio: 'A fellow podcaster with a whole network of great shows, the quality and volume of his output is an inspiration for us.'
  websites:
    - name: LSG Media
      url: 'http://www.libertystreetgeek.net'
  social:
    - name: twitter
      url: https://www.twitter.com/dean_lsgmedia
    - name: instagram
      url: https://www.instagram.com/deanlsgmedia
  roles:
    - show: decipherscifi
      role: guest_cohost
---
