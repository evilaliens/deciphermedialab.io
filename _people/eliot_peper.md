---
layout: person_page
person_data:
  name: Eliot Peper
  data_name: eliot_peper
  image_path: assets/imgs/people/eliot_peper.png
  title: Author
  bio: 'Eliot Peper is a novelist and strategist based in Oakland, CA. He writes fast-paced, deeply-researched stories with diverse casts that explore the intersection of technology and society. -Wikipedia'
  websites:
    - name: Eliot Peper
      url: 'http://www.eliotpeper.com/'
  social:
    - name: twitter
      url: https://www.twitter.com/eliotpeper
    - name: instagram
      url: https://www.instagram.com/elpeper
  roles:
    - show: decipherscifi
      role: guest_cohost
---
