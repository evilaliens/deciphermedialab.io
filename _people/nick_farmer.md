---
layout: person_page
person_data:
  name: Nick Farmer
  data_name: nick_farmer
  image_path: assets/imgs/people/nick_farmer.png
  title: Linguist, Writer
  bio: 'Nick Farmer is a writer and linguist based in Oakland, CA. He created the Belter conlang for Syfy’s The Expanse, contributed to the best-selling introductory linguistics textbook published by MIT Press, and works to support endangered and indigenous languages'
  websites:
    - name: Nick Farmer Linguist
      url: 'http://www.nickfarmerlinguist.com/'
  social:
    - name: twitter
      url: https://www.twitter.com/nfarmerlinguist
    - name: instagram
      url: https://www.instagram.com/nfarmerlinguist
  roles:
    - show: decipherscifi
      role: answerer
    - show: decipherscifi
      role: guest_cohost
---
