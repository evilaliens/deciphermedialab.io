---
layout: person_page
person_data:
  name: Melissa Marquez
  data_name: melissa_marquez
  image_path: assets/imgs/people/melissa_marquez.png
  title: Marine biologist
  bio: 'Marine biologist who sometimes gets bit by things. Also a podcaster, speaker, and shark promoter.'
  social:
    - name: twitter
      url: https://twitter.com/mcmsharksxx
    - name: instagram
      url: https://www.instagram.com/melissacristinamarquez/?hl=en
  roles:
    - show: decipherscifi
      role: guest_cohost
---
