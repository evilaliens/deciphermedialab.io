---
author: christopher_peterson
categories:
  - irl
date: 2016-11-08 07:00:04+00:00
layout: post
slug: decipherscifi-arrival-nyc-movie-meetup-nov-11
title: 'Decipher SciFi Arrival NYC movie & meetup (Nov 11)'
image:
  path: assets/imgs/misc/meetup-arrival.jpg
  alt: Arrival movie meetup announcement image

---
Hey, people!

We'll be getting together with some of you on Friday, Nov 11 2016 to watch and discuss the Ted Chiang movie, Arrival.

So go the [Facebook event page](https://www.facebook.com/events/1771845099736204/) and confirm your attendance if you're in the area. There will be a good handful of Decipher SciFi type people there, and we'll go drink and eat afterwards. 😃
