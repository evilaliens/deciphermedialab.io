---
author: christopher_peterson
categories:
  - irl
date: 2016-11-14 04:06:02+00:00
layout: post
slug: arrival-meetup-nyc
title: 'Arrival Meetup in NYC: Trip Report!'
image: 
  path: 'assets/imgs/misc/meetup-arrival-selfie.jpg'
  alt: 'Decipher SciFi Arrival movie meetup with fans'

---
Our Arrival movie meetup in NYC was a great time! Met some new people, and you may recognize some previous co-hosts!

Keep an eye out - we're definitely gonna do this again soon. :)
