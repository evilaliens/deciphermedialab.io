The Site Theme
==========================

We have adapted the [Poca] theme from Colorlib as a Jekyll gem-based theme.

You shouldn't need to actually work with this most of the time. This is purely if there is some modification or update to the theme/dependencies.

# Changes from the original theme

The only Javascripty thing we brought to this ourselves is [Flexsearch.js] for client-side site search. We also updated the font-awesome version. Everything else should mostly be as we got it.

# Working with the theme

AKA "regenerating the dependencies for the theme" i.e. Bootstrap and FontAwesome.

Make sure you have node:

```sh
# Ubuntu
apt-get install nodejs-dev npm
```

Install node dependencies using bundle and the provided manifest:

```sh
cd /poca/working/directory
npm install
```

Run gulp in the source directory:

```sh
# This will run gulp based on the instructions in `gulpfile.js`
cd /poca/working/directory
npm run start
```

[Flexsearch.js]: https://github.com/nextapps-de/flexsearch
[Gulp]: https://gulpjs.com/
[Poca]: https://colorlib.com/wp/template/poca/
