Images
======

In most cases with Jekyll (including ours), images are served from the `assets` directory. Any files in `assets` are served as-is without interpretation, e.g.documents for download, or images. Probably the only type of such file we should be serving are images.

# Posting

The thing to do would be to pull the template [_docs/templates/image.md](_docs/templates/image.md) into your document and just fill it in.

To pull in the image template in vim:

```viml
" With a document open from the root of the repo...
:read _docs/templates/image.md
```

And then just fill in the fields. Vim's insert-mode filename auto-completion is handy here (`:help i_CTRL-X_CTRL-F`).

## Examples

To include an image with full credit and a reference to the source (always necessary except with our own creations), using the template above:

```liquid
{% responsive_image_block %}
  path: assets/imgs/image.jpg
  alt: Something descriptive and accurate
  caption: An optional caption
  attr_text: The author of the work CC-BY-3.0
  attr_url: https://domain.tld/path/to/source
{% endresponsive_image_block %}
```

*Note*: it is possible to use liquid tags inside these values!

# Options

Regarding images in general it's important to us that we:

* try not to use images from anyone without permission or fair transformative use
* publish allowable images with credit and licensing information, as well as a link back to source where possible
* include at least basic accessibiity features, namely actually-descriptive `alt` tags

## "Responsive" images

What are [responsive images]?

By default, the plugin that we are using in our inage template will convert the given source image to multiple sizes and give it to the client such that it can pick the most appropriate size/pizel density for its display. *You will not need to resize images yourself*. Just toss them in and let the computer do it.

## Image link

Also by default, the displayed image will link directly back to the larger canonical version of the image. The linking can be disabled with `link_original`:

```liquid
{% responsive_image_block %}
  path: 'assets/imgs/image.jpg'
  alt: 'A descriptive alt tag'
  link_original: false
{% endresponsive_image_block %}
```

# Changing the template

The default include for this on our site is `_include/image_inline.html`. You can specify the alternative for a mere thumbnail (as we see on our [people] page on the site:

```liquid
{% responsive_image_block %}
  path: 'assets/imgs/image.jpg'
  alt: 'A descriptive alt tag'
  include: thumbnail.html
{% endresponsive_image_block %}
```

Or you could create an entirely new template if it was necessary. But, that would be beyond the scope of this document.


[responsive images]: https://developer.mozilla.org/en-US/docs/Learn/HTML/Multimedia_and_embedding/Responsive_images
[people]: https://deciphermedia.tv/people/
