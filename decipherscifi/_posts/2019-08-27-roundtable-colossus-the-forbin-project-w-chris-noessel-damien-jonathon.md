---
# General post frontmatter
categories:
  - episode
  - roundtable
date: 2019-08-27 08:45:21+00:00
description: "Artificial superintelligence way ahead of its time. AI portrayals. Computer vision depictions. Instrumental convergence and 'world peace' maximization."
layout: podcast_post
permalink: /decipherscifi/roundtable-colossus-the-forbin-project-w-chris-noessel-damien-jonathon
redirect_from:
  - /decipherscifi/208
slug: roundtable-colossus-the-forbin-project-w-chris-noessel-damien-jonathon
title: 'Roundtable: Colossus The Forbin Project w/ Chris Noessel, Damien Williams, and Jonathon Korman'
tags:
  - artificial intelligence
  - benevolent ai dictator
  - realism
  - science
  - science fiction
  - superintelligence
  - telecommunications

# Episode particulars
image:
  path: assets/imgs/media/movies/colossus_the_forbin_project_1970.jpg
  alt: '1950s jumpsuit-man in front of a self-aware mainframe'
links:
  - text: 'Works by Madeline Ashby'
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Madeline-Ashby/e/B008MAJOTI'
  - text: 'Person of Interest'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/person-of-interest-the-complete-series/id1192550899?mt=4&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Pilot/dp/B0095R630A'
      - text: Netflix
        url: 'https://www.netflix.com/watch/70296224'
media:
  links:
    - text: Amazon
      url: 'https://www.amazon.com/Colossus-Project-Blu-ray-Eric-Braeden/dp/B0776221Y2'
  title: 'Colossus: The Forbin Project'
  type: movie
  year: 1970
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: damien_williams
    roles:
      - roundtable_guest
  - data_name: chris_noessel
    roles:
      - roundtable_guest
  - data_name: jonathan_korman
    roles:
      - roundtable_guest
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/colossus_the_forbin_project_1970.jpg
  number: 208
  guid: 'https://decipherscifi.com/?p=10755'
  media:
    audio:
      audio/mpeg:
        content_length: 58090164
        duration: '01:09:03'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/colossus_the_forbin_project.mp3'

---
#### Weirdly prescient and quietly influential

AI portrayal in film. Recognizing 2001: A Space Odyssey. Command and control. Unintended consequences. Personal movie connections and impact decades before [Bostrom's Superintelligence](https://www.amazon.com/Superintelligence-Dangers-Strategies-Nick-Bostrom-ebook-dp-B00LOOCGB2). Game of Thrones??? [70s scifi cynicism](https://www.youtube.com/watch?v=v3XR9VNcaxA).

#### Benevolent AI dictator

The Cold War and looming climate catastrophe. What are Colossus' goals? What goals does it think it has?

#### AI Portrayal

Period conceptions of computing as centralized and institutional. Computing in the era of the first moon landing. The "big board." Computer scientists a la Mad Men. Colossus as a Golem story. How to take over the world with no subterfuge or tact. Gendering AI.

#### Realism

Portrayal of telecommunications literally before the invention of networking. Packet switching was first implemented after the novel was written and only just before the film! Computer communication syntax. Computing language. Computer interfaces and code checking.

#### Getting weird

The books get weird. And weirder! How sometimes books are accidentally way better than their author is capable of.

#### Paperclip maximization

Colossus as utilitarian "peace maximizer." Showing the monkeys the gun to get your point across.
