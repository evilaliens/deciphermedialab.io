---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-02-26 07:45:02+00:00
description: "The life of a star. Exploring 'close' to our sun with the Parker Solar Probe. Living deep underground. Smuggling things to space in surprising places. Etc."
layout: podcast_post
permalink: /decipherscifi/the-wandering-earth-altered-goldilocks-zones-earths-heat-budget-and-smuggling-pickles-in-your-pants
redirect_from:
  - /decipherscifi/182
slug: the-wandering-earth-altered-goldilocks-zones-earths-heat-budget-and-smuggling-pickles-in-your-pants
title: 'The Wandering Earth: altered goldilocks zones, Earth''s heat budget, and smuggling pickles in your pants'
tags:
  - deep space
  - habitability zones
  - science
  - science fiction
  - space
  - space settlement
  - space travel
  - stars

# Episode particulars
image:
  path: assets/imgs/media/movies/the_wandering_earth_2019.jpg
  alt: 'An apocalyptic-looking sky event'
links:
  - text: 'The AMAZING Design of the Parker Solar Probe by Smarter Every Day'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=aQaCY7wlQEc'

  - text: 'The Three Body Problem by Liu Cixin'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/the-three-body-problem/id856893409?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Three-Body-Problem-Cixin-Liu/dp/0765382032'
  - text: 'The Wandering Earth (book) by Liu Cixin'
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Wandering-Earth-Cixin-Liu/dp/1784978515/'
media:
  title: 'The Wandering Earth'
  type: movie
  year: 2019
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_wandering_earth_2019.jpg
  number: 182
  guid: 'https://decipherscifi.com/?p=10170'
  media:
    audio:
      audio/mpeg:
        content_length: 30687889
        duration: '36:31'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Wandering_Earth_--_--_Decipher_SciFi.mp3'

---
#### Life and death of a star

A hundred years to uninhabitability is a little bit quicker than we thought. Timelines here to red dwarf and all the unpleasantness in between.

#### Probing your star

The surprising difficulty of actually reaching the sun. The [Parker Solar Probe](http://parkersolarprobe.jhuapl.edu/). Taking a close peek at the corona and the young solar wind. _Close_ as in millions of miles.

#### Moving Earth

Shifting [circumstellar habitability zones](https://en.wikipedia.org/wiki/Circumstellar_habitable_zone). Moving the planet out nearer Jupiter as our sun expands. Throwing moderation to the wind and ditching the solar system. Tidal forces.

#### Planetary travel in deep space

Relativistic impact vulnerability. Appreciating Earth's protected position within the heliosphere, blocked from impactors by two gas giants and its own moon.

#### Earth's new home

Traveling across deep space with no sun. If you dig deep enough, mayyyyyyybe Kelvin's "[secular cooling](https://zapatopi.net/kelvin/papers/on_the_secular_cooling_of_the_earth.html)" would keep everybody warm for the trip. Signing up for some [eye shine]({% link decipherscifi/_posts/2017-10-03-pitch-black-feat-fraser-cain-of-universe-today-episode-110.md %}). The three-body problem.

#### Going deep

Living underground. Shielding from the radiation and frigid cold of deep space with kilometers and kilometers of Earth. Learning to eat lots of algae, crickets, and fungus. Taking advantage of geothermal energy at depth. Dealing with Earthquakes while being _under_ some of them. The safety of tunnels.

#### Space station

[Smuggling pickles](https://www.rbth.com/science_and_tech/2017/04/12/space-smugglers-how-russian-cosmonauts-sneak-booze-into-outer-space_740566) in your pants into space. And maybe some vodka or cognac. "_Who wants a warm pickle_?"
