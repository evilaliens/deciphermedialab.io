---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-03-08 10:45:23+00:00
description: 'Miles Greb helps us understand the Dune movie via the book. Victims of prophecy, fulfilling prophecy, spiiiice, homophobia, David Lynch weirdness.'
layout: podcast_post
permalink: /decipherscifi/dune-movie-podcast-miles-greb-episode-28
redirect_from:
  - /decipherscifi/28
slug: dune-movie-podcast-miles-greb-episode-28
title: 'Dune: spice, David Lynch is weird, and wetware technology w/ Miles Greb'
tags:
  - science fiction
  - science
  - ecology
  - dystopia
  - insurgence
  - monster
  - nuclear weapons
  - war

# Episode particulars
image:
  path: assets/imgs/media/movies/dune_1984.jpg
  alt: 'Dune 1984 Paul backdrop'
links:
  - text: After the Gold Rush by Miles Greb
    urls:
      - text: Gold Rush Comics
        url: 'http://www.afterthegoldrush.space/'
  - text: Dune + Jodorowsky's Dune by Double Feature podcast
    urls:
      - text: Double Feature
        url: 'https://doublefeature.fm/2015/dune-jodorowskys-dune'
  - text: Dune by Frank Herbert
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/dune/id597944491?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Dune-Frank-Herbert-ebook/dp/B00B7NPRY8'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/dune/id283648369?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Dune-Francesca-Annis/dp/B000I9S64U'
  title: Dune
  type: movie
  year: 1970
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: miles_greb
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/dune_1984.jpg
  number: 28
  guid: 'http://www.decipherscifi.com/?p=594'
  media:
    audio:
      audio/mpeg:
        content_length: 24722781
        duration: '41:11'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Dune_Miles_Gren_decipherSciFi.mp3'
---
#### This movie is weird

David Lynch. Return of the Jedi. Different editions of the film. Alan Smithee. Internal monologues.

#### Dune politics

Before Game of Thrones, this was a new level of deep. Game theory and politics.

#### Anti-digitalism

Mentats. Eyebrows.

#### Spice

Like oil? Drugs?

#### Treatment of Homosexuality

Was Frank Herbert homophobic? Considering the man and the time in which it was written.

#### Gods and Cults

How the movie and the book had opposite lessons. Making it rain.
