---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-06-12 07:45:32+00:00
description: 'Enemy Mine science! Space warfare, the trouble with dogfighting spacecraft, craft design for space vs design for atmosphere. Planetary situations. More!'
layout: podcast_post
permalink: /decipherscifi/enemy-mine-episode-146
redirect_from:
  - /decipherscifi/146
slug: enemy-mine-episode-146
title: 'Enemy Mine : space dogfights, alien wildlifem and interstellar slavery economics'
tags:
  - aliens
  - economics
  - evolution
  - interstellar civilization
  - reproduction
  - science
  - science fiction
  - space

# Episode particulars
image:
  path: assets/imgs/media/movies/enemy_mine_1985.jpg
  alt: A human and an alien sharing a sunset
links:
  - text: Space Warfare by Isaac Arthur
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=xvs_f5MwT04'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/enemy-mine/id280623676?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Enemy-Mine-Dennis-Quaid/dp/B000I9U9ZE'
  title: Enemy Mine
  type: movie
  year: 1985
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/enemy_mine_1985.jpg
  number: 146
  guid: 'https://decipherscifi.com/?p=6631'
  media:
    audio:
      audio/mpeg:
        content_length: 34539864
        duration: '41:06'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Enemy_Mine_--_--_Decipher_SciFi.mp3'
---
#### Old-school space dogfighting

Stealth and the economics of close-distance space combat. Ship designs for different combat settings. Space vs atmosphere travel. Underwater?

#### Alien planets

Hostile, but really lucky to have breathable air and drinkable water. Storms. Periodic heavy meteor showers. Comet tails vs close planetary bodies for meteor shower material.

#### Alien wildlife

Space horseshoe crabs. Sarlac antlions. Half-Life 2 interpretations. Antlion larvae.

#### Drac

The aliens as portrayed by Louis Gossett Jr. Alien physiology. Evolutionary body-heat dissipation adaptations. The spectrum of binocularity in Earth-life.

#### Space-faring civilization

Time scales for change on the technological curve.

#### Alien reproduction

Hermaphroditism? Asexuality? Parthenogenesis? This pregnancy is a weird situation. One man and an alien baby.

#### Interstellar enslavement economics

Transporting slaves to remote mining colonies. Where are the robots? Possible Dune connections.
