---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-02-21 07:45:04+00:00 
description: "Geology and alien morphology and putting things in butts! Adventures in piercing the earth's crust, magnetospheres, neutronium, more" 
layout: podcast_post
permalink: /decipherscifi/independence-day-resurgence-episode-78
redirect_from:
  - /decipherscifi/78
slug: 
title: "Independence Day Resurgence: Earth's center, gravity wells, and a hole in the bottom of the sea" 
tags:
  - science fiction
  - science
  - future
  - aliens
  - apocalypse
  - space ship

# Episode particulars
image:
  path: assets/imgs/media/movies/independence_day_resurgence_2016.jpg
  alt: Planet-scale attacking spaceship Independence Day Resurgence movie backdrop
links:
  - text: Why Earth Is A Prison and How To Escape It by Kurtzgesagt
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=RVMZxH1TIIQ'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/independence-day-resurgence/id1121888040?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Independence-Day-Resurgence-Liam-Hemsworth/dp/B01HDR2AOG'
  title: Independence Day Resurgence
  type: movie
  year: 2016
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/independence_day_resurgence_2016.jpg
  number: 78
  guid: 'http://www.decipherscifi.com/?p=1355'
  media:
    audio:
      audio/mpeg:
        content_length: 42696704
        duration: '50:49'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Independence_Day_Resurgence_--_--_decipherSciFi.mp3'
---

{% responsive_image path: assets/imgs/selfies/independence_day_resurgence.jpg alt: "Decipher SciFi co-hosts re-enacting Independence Day" %}

#### Alien Reconnaissance

Distress calls. Radio broadcasts and other ways to watch the best.speech.ever.

{% youtube "https://www.youtube.com/watch?v=9t1IK_9apWs" %}

#### Alien Arrival

Wormholes. Telefragging. In-system ship speeds and an incredible ability to slow down.

#### Human Reverse Engineering

Twenty years of reverse engineering alien tech has yielded great fruit. Propulsion! Anti-gravity.

#### Getting Stuff in Space

It's hard! Gravity wells. Magic fusion antigravity obviates the main utility of Lagrange points.

#### Inertial Dampening

Fluid _sack_ inertial dampening.Breathable oxygenated fluids. Vectors!

#### Data in a Coma

Muscular atrophy. Twenty years without moving. [Communicating with locked-in patients](https://sciencebasedmedicine.org/communicating-with-the-locked-in-update/).

#### City Destroyers

Gratiuitous city destruction. Tactics and strategy. Are the aliens just jerks?

{% youtube "https://www.youtube.com/watch?v=oIiO7bL2yFI" %}

> They flew into my butthole! My only weakness, how did you know?!

#### Harvester Ships

Why do cities lift off of the ground?

#### Earth is just a ball of rock.

And metal. The creamy center is actually not creamy but rather solid iron. It's like a Kinder Egg and a Cadbury Cream Egg had a baby. The sorts of doom we'd face by having our core disrupted/extracted. Magnetosphere and the molten core. The crust ranges from 3-25 miles thick, fwiw.

#### Drilling the Core

Massive energy to not only drill but keep the hole into the earth open. A hole in the bottom of the sea.

{% youtube "https://www.youtube.com/watch?v=bsB8tvQMUT8" %}

#### 'Baitin

Jeff Goldblum is the master baiter. Alien queen fight. Insectoid queen morphology. Sexual dimorphism. Putting stuff up their butts for victory. Original idea, original idea, original idea:

{% youtube "https://www.youtube.com/watch?v=xFO0Zo0IU5A" %}

#### Final Answer

{% responsive_image path: assets/imgs/misc/zap_brannigan_butt.jpg alt: "Zap Brannigan butt meme" %}
