---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-08-16 07:25:58+00:00
description: 'We have Hardcore Henry on the show so we can talk FPS visual design, Biting Elbows, motion sickness vs simulation sickness, and Sharlto Copley. And a horse.'
layout: podcast_post
permalink: /decipherscifi/hardcore-henry-movie-podcast-episode-51
redirect_from:
  - /decipherscifi/51
slug: hardcore-henry-movie-podcast-episode-51
title: 'Hardcore Henry: super fun action, VR media, and motion sickness'
tags:
  - science fiction
  - science
  - vfx
  - practical fx
  - parkour
  - stuntwork
  - video games
  - proproception
  - motion sickness

# Episode particulars
image:
  path: assets/imgs/media/movies/hardcore_henry_2015.jpg
  alt: Hardcore Henry POV guns movie backdrop
links:
  - text: How Hardcore Henry's POV shots were made by Ian Failes
    urls:
      - text: FXGuide
        url: 'https://www.fxguide.com/featured/how-hardcore-henrys-pov-shots-were-made/'
  - text: The Stampede by Biting Elbows
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=QYm-dT24iRY'
  - text: Bad Motherfucker by Biting Elbows
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Rgox84KE7iY'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/album/hardcore-henry-original-motion/id1094454495?at=1001l7hP&mt=1&app=music'
    - text: Amazon
      url: 'https://www.amazon.com/Hardcore-Henry-Sharlto-Copley/dp/B01DWYJXVW'
  title: Hardcore Henry
  type: movie
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/hardcore_henry_2015.jpg
  number: 51
  guid: 'http://www.decipherscifi.com/?p=957'
  media:
    audio:
      audio/mpeg:
        content_length: 23632945
        duration: '39:22'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Hardcore_Henry_decipherSciFi.mp3'
---

{% responsive_image path: assets/imgs/selfies/hardcore_henry.jpg alt: "Decipher Media hosts doing the Hardcore Henry thing" %}

#### The movie's history

Crowdfunding.  Timur Bekmambetov.  Biting Elbows music videos.

#### First-Person film and video games

Visual language of first person shooters. Our affection for the genre and this film by extension.

#### Filming and FX

"Adventure Mask" iterative design. Camera mounting and how bets to capture first person action. VFX difficulties with Go Pros.

#### Motion Sickness

Surprising study results with old and young players. Motion sickness vs simulation sickness.

#### Sharlto

A Sharlto appreciation party. Remote surrogate body control.

#### Horse

horse, lol.
