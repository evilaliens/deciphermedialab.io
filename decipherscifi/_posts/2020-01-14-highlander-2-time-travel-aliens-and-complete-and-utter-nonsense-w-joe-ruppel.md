---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2020-01-14 07:45:19+00:00
description: "Breaking movie mythologies. A notoriously low point in scifi history. Magical Spanish bagpipes. Multiple movie versions. Ol' Ironsides at his best."
layout: podcast_post
permalink: /decipherscifi/highlander-2-time-travel-aliens-and-complete-and-utter-nonsense-w-joe-ruppel
redirect_from:
  - /decipherscifi/224
slug: highlander-2-time-travel-aliens-and-complete-and-utter-nonsense-w-joe-ruppel
title: 'Highlander 2: time travel, aliens, and complete and utter nonsense w/ Joe Ruppel'
tags:
  - mythology
  - ozone layer
  - science
  - science fiction
  - time travel
  - weapons technology

# Episode particulars
image:
  path: assets/imgs/media/movies/highlander_2_1991.jpg
  alt: 'Connor MacLeod and Sean connery standing with swords and lightning over dystopia..?'
links:
  - text: 'Highlander 2 Fairytale Ending'
    urls:
      - text: 'YouTube'
        url: 'https://www.youtube.com/watch?v=NW1qHfhe_TM'
  - text: 'Highlander 2 BTS Interviews'
    urls:
      - text: 'YouTube'
        url: 'https://www.youtube.com/watch?v=WTQC3IIzhj4'
  - text: 'Sean Connery The Musical by legolambs'
    urls:
      - text: 'YouTube'
        url: 'https://www.youtube.com/watch?v=LB7f_kZQ9hY'
media:
  links:
    - text: Amazon
      url: 'https://www.amazon.com/Highlander-II-Quickening-Sean-Connery/dp/ref=sr_1_1?keywords=highlander+2&qid=1582042557&sr=8-1'
  title: 'Highlander 2'
  type: movie
  year: 1991
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/highlander_2_1991.jpg
  number: 224
  guid: 'https://decipherscifi.com/?p=10904'
  media:
    audio:
      audio/mpeg:
        content_length: 50889236
        duration: '01:00:28'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/highlander_2.output.mp3'

---
#### Mythology

Taking a simple mysterious mythology and explaining it for fun and profit*.

#### Meta

The original Highlander's original, surprising success story. Deciding to make a sequel. Budgets and revenues and how did we get here, really?

#### Versions

Aliens vs time travel. The clincher: neither actually makes any more sense than the other.

#### People

Not only Connnor MacLeod of the Clan MacLeod, but _ALSO_ his best friend who totally just died in the last movie. but hey, lol jk it's a kind of magic. Ol' Ironsides hamming it up and honestly stealing the show. Recasting Highlander 2 with characters from The Room. The 80s/90s: when bad guys were really bad guys and just didn't need a reason.

#### The ozone layer

The actual environmental problem that the world actually got together amd potentially resolved! Fixing the ozone layer. The "shield" in the film and blocking all of the visible light in addition to the UV.

#### Scenes

Some scenes from this film were truly mind-boggling and/or amazing and it's hard to tell which were which. Ironsides ham. The car scene, OR sacrificing Yoshi to make the jump. Sean Connery being rogueish/trash on a plane. Scottish fan-death. Offscreen sword-switching antics.


\* not really very much profit tbh
