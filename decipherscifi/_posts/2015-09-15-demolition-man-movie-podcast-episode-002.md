---
categories:
  - episode
  - just the two of us
  - movie
date: '2015-09-15 05:02:38+00:00'
description: 'With Demolition Man we talk science fiction and rehab vs punishment, cryogenic freezing, implantable skills, behavior modification, nonviolent dystopia.'
layout: podcast_post
permalink: /decipherscifi/demolition-man-movie-podcast-episode-002
redirect_from:
  - /decipherscifi/2
slug: demolition-man-movie-podcast-episode-002
title: 'Demolition Man: cryogenic criminal justice and three seashells'
tags:
  - censorship
  - crime
  - cryogenics
  - culture shock
  - cyberpunk
  - dystopia
  - dystopia
  - future
  - police
  - psychopath
  - science fiction
  - utopia
  - violence

image:
  path: assets/imgs/media/movies/demolition_man_1993.jpg
  alt: 'Demolition Man 1993 backdrop'
links:
  - text: Futurama
    url:  'http://www.amazon.com/Futurama-Complete-Billy-West/dp/B00F77MAC2/'
  - text: 'The Lost Fleet Books by Jack Campbell'
    url: 'http://www.amazon.com/Dauntless-Lost-Fleet-Book-1/dp/0441014186/'
  - text: Rambo
    url: 'https://www.amazon.com/Rambo-Sylvester-Stallone/dp/B001AMVKOC/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/demolition-man/id279386406?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Demolition-Man-Sylvester-Stallone/dp/B001AIY58O/'
  title: Demolition Man
  type: movie
  year: 1993
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:



episode:
  number: 2
  guid: 'http://www.decipherscifi.com/?p=190'
  media:
    audio:
      audio/mpeg:
        content_length: 19086096
        duration: '31:48'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Demolition_Man.mp3'

---

Wesley Snipes, irl criminal, fights Sylvester Stallone in the future while wearing hammer pants and gaudy earrings. Future sci-fi hijinx ensue.

#### Criminal Rehabilitation


Rehab or punishment? Synaptic suggestion for true rehabilitation? The time needed for the rehab process in the film. Cryogenic freezing. Stallone says he was awake, probably wasn't. Better than prison. What if evil dictators had this technology?


#### Implantable Skills


Huge benefits. Whoah, I know kung fu. Free will. No need for college, just plug in. But who will be the lower class?


#### Nonviolence


Unreasonable to have a pacifistic society in a world with self-interested human actors? Silliness. Stagnation of martial skills.


#### Thought Policing


Anything not good for you is bad, hence illegal. Fluid transfer illegal. Can humanity move beyond sex? Maybe after the singularity. Should we even call it humanity at that point?


#### Future Tech


Communications technology prescience. Video calls on tablets. Self driving cars. Stallone is a curmudgeonly caveman. Cryogenics. Sewer maintenance. No internet? **The three sea shells mystery, solved.**
