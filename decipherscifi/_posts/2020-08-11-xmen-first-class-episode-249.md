---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2020-08-11T02:56:30-04:00
description: 'Post-war atomic anxiety callbacks. Even MORE Neanderthal DNA in modern humans. Bad genocidal plans - again. Destructive resonance. Brown-notes in combat. Submarines. Etc.'
layout: podcast_post
redirect_from:
  - /decipherscifi/249
slug: xmen-first-class-nuclear-panic-destructive-resonance-woolly-mammoth-pinatas
title: 'X-Men First Class: nuclear panic, destructive resonance, and woolly mammoth piñatas'
tags:
  - comics
  - evolution
  - homo neanderthalensis
  - nuclear weapons
  - radiation
  - science
  - science fiction
  - submarines

# Episode particulars
image:
  path: assets/imgs/media/movies/x-men_first_class_2011.jpg
  alt: 
links:
  - text: 'How I Boarded a US NAVY NUCLEAR SUBMARINE in the Arctic (ICEX 2020) by Smarter Every Day'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=5d6SEQQbwtU'
  - text: 'Boarding a US NAVY NUCLEAR SUBMARINE in the Arctic by Smarter Every Day'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=RXXMJAU6vY8'
  - text: 'What if We Nuke a City? by Kurtzgesagt'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=5iPH-br_eJQ'
media:
  links:
    - text: YouTube
    - url: 'https://www.youtube.com/watch?v=iOBjIq844iM'
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/x-men-first-class/id446076340?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/X-men-First-Class-James-McAvoy/dp/B005HNMPBM'
  title: 'X-Men: First Class'
  type: movie
  year: 2011
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/x-men_first_class_2011.jpg
  number: 249
  guid: xmen-first-class-nuclear-panic-destructive-resonance-woolly-mammoth-pinatas
  media:
    audio:
      audio/mpeg:
        content_length: 33629973
        duration: '39:59'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/xmen_first_class.final.mp3'

---
# Fear of mutation

Our recent [X-Men]({% link decipherscifi/_posts/2020-07-28-xmen-episode-247.md %}). Irl post-war [nuclear anxieties]({% link decipherscifi/_posts/2019-04-09-them-nuclear-optimism-amateur-myrmecology-and-wilhelm-screaming-w-joe-ruppel.md %}). More mutant panic and more mutant supremacy bad guys.

# Irradiating Earth

Not enough to make superpowers, but we sure did irradiate a lot. The clear mark of human civilization in the geological record. [Past discussions]({% link decipherscifi/_posts/2019-03-19-atlantis-the-silurian-hypothesis-ancient-language-and-updating-the-drake-equation.md %}) of the Silurian Hypothesis.

# Neanderthal interactions

2010 Neanderthal DNA news and the possibility of this informing the film. Extincting things more by accident than on purpose in pre-agricultural times. Consistent neanderthal pronunciation consistency issues.

# The plan(s)

Causing global nuclear war so you can... rule over the ashes? At least the Magneto version of the plan didn't destroy infrastructure.

# Powers

The energy levels required for useful destructive resonance as a superpower. Brown-noting your enemies in combat. Once more determining that functional immortality is *the best* power that w actually want.

# Submarining

Fitting nuclear submarines in your megayacht. Ice floes vs icebergs vs glaciers. Where to get the best ice for your drinks.
