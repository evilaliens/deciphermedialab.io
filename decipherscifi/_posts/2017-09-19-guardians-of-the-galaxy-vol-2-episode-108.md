---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-09-19 07:45:25+00:00
description: 'Science in the Guardians of the Galaxy sequel! Aurora breath, breeding planned societies, drones, hexagonal wormholes, empathy, Yondu love, and more!'
layout: podcast_post
permalink: /decipherscifi/guardians-of-the-galaxy-vol-2-episode-108
redirect_from:
  - /decipherscifi/108
slug: guardians-of-the-galaxy-vol-2-episode-108
title: 'Guardians of the Galaxy Vol 2: space jumps, empathy telepathy, and ❤️  Yondu'
tags:
  - faster-than-light travel
  - genetic design
  - planets
  - science
  - science fiction
  - space
  - space travel
  - star trek

# Episode particulars
image:
  path: assets/imgs/media/movies/guardians_of_the_galaxy_vol_2_2017.jpg
  alt: The Guardians and Yondu sashaying out of a nebula or something
links:
  - text: The Complex Feels of Guardians of the Galaxy v.2 by Lindsay Ellis
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=8VulkN5OLEM'
  - text: Does water freeze or boil in space?
    urls:
      - text: Starts With a Bang
        url: 'https://medium.com/starts-with-a-bang/does-water-freeze-or-boil-in-space-7889856d7f36'
  - text: The Trelenburg Gate by Andy P
    urls:
      - text: Batch 25 Comics
        url: 'https://batch25comics.com/the-trelenburg-gate/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/guardians-of-the-galaxy-vol-2/id1225848804?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Guardians-Galaxy-Vol-Bonus-Features/dp/B0718Z9G5W'
  title: Guardians of the Galaxy Vol 2
  type: movie
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/guardians_of_the_galaxy_vol_2.jpg
  number: 108
  guid: 'https://decipherscifi.com/?p=3340'
  media:
    audio:
      audio/mpeg:
        content_length: 44537856
        duration: '53:00'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Guardians_of_the_Galaxy_Vol_2_--_--_decipherSciFi.mp3'
---
#### Confetti breath

Colorful monster breath. Aurora borealis or australis and how the solar wind interacts with our atmosphere at the poles. How "confetti breath" is a better name than "Taserface" and how Taserface looks like Mr Lordi:

{% youtube "https://www.youtube.com/watch?v=uGe8qID9gSs" %}

#### Skin

Drax's misunderstanding about "thickness," and our approval of his intuition that the inside might be easier to pierce. Pick a [meatus](https://en.wikipedia.org/wiki/Meatus). And... it can be gold! How not to die from using bodypaint.

#### The Sovereign

The gold people with the gold parts. Grills? Genetic design of a whole civilization and levels of caution for personal safety graphed against length of life.

#### Drones

For once in space-scale scifi: drones! The levels of cost and complexity when designing craft to safely transport squishy humanoids. Galaga!

#### Space jumps/wormholes

Hexagons! And what they reveal about interstellar travel in this universe. Space straws.

#### Brain Planet

Escape velocity and atmosphere and centering your vulnerable mass. The life lost when a creature that lives for eternity is vanquished. Inability to take on perspectives.

#### Empath...y?

Empath vs telepath and imagining how this ability could communicate "emotions" but not "thoughts." FMRI. Lizard brain. Fight or flight. Side-channel data leakage and influencing the system via things like [Trans-cranial direct current stimulation](https://en.wikipedia.org/wiki/Transcranial_direct-current_stimulation).

#### Yondu

A Yondu-appreciation party and exploring the control of the arrow via the fin. Controlling the arrow not by his whistle but by direct bionic neuro-feedback.
