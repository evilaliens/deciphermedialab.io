---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-05-16 08:11:44+00:00
description: 'Prometheus is our first episode on the Alien universe. Origins of life on Earth, objective-oriented evolution, DNA, star maps, alien pregnancy, more'
layout: podcast_post
permalink: /decipherscifi/prometheus-episode-90
redirect_from:
  - /decipherscifi/90
slug: prometheus-episode-90
title: 'Prometheus: yoked aliens, cave paintings, and seeding life' 
tags:
  - science fiction
  - science
  - space
  - aliens
  - evolution
  - astronomy

# Episode particulars
image:
  path: assets/imgs/media/movies/prometheus_2012.jpg
  alt: Fassbenderbot looking in wonder at a palantír or something
links:
  - text: Messages for the Future by VSauce
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=GDrBIKOR01c'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/prometheus/id547496947?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Prometheus-Noomi-Rapace/dp/B009NQKPUW'
  title: Prometheus
  type: movie
  year: 2012
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/prometheus_2012.jpg
  number: 90
  guid: 'http://www.decipherscifi.com/?p=1531'
  media:
    audio:
      audio/mpeg:
        content_length: 47938789
        duration: '57:03'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Prometheus_--_--_decipherSciFi.mp3'
---
#### Love and hate

A story of love for Prometheus with recognition of its faults. Surprisingly high tomato ratings. So much stuff to think about?

#### Seeding Earth

Natural selection, evolution, and engineers seeding life on Earth. This would require seeding _all_ life on earth, to explain our shared genetic heritage with everything else that lives here.

#### Objective-based evolution

In this movie, I guess [Lamarck Was Right](http://tvtropes.org/pmwiki/pmwiki.php/Main/LamarckWasRight). Continued Egineer presence over the course of the history of life on earth, guiding things toward their goals in order to make us like them.

#### The Engineers

Yoked as hell. Albinos with allopecia.

#### Cave Paintings

List of cultures visited. Radiocarbon dating (carbon-14). Uranium dating. Informational density.

#### Star Maps

Constellations and hemispheres and directions to solar systems.

#### Alien System

FTL. Moons and ringed planets. How _not_ to safely approach and explore a new planet/moon. "Eh, seems fine" as a modus operandi for your space exploration mission.

#### Alien Pregnancy

Parasites. Superparasites, Tearing the umbilical cord. Placenta.
