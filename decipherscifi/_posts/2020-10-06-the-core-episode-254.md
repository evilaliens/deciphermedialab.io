---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2020-10-06T02:52:26-04:00
description: "The forgetabilty valley. Space shuttle landings. Bird navigation by magnetic field. Pacemakers. Earth's energetic spinning core. Etc."
layout: podcast_post
redirect_from:
  - /decipherscifi/254
slug: core-flying-bricks-pacemaker-quantum-avian-eyeballs
title: 'The Core: Flying bricks, pacemakers, and quantum avian eyeballs'
tags:
  - earth
  - geology
  - science
  - science fiction
  - space
  - space shuttle

# Episode particulars
image:
  path: assets/imgs/media/movies/the_core_2003.jpg
  alt: 'The Earth, but broken'
links:
  - text: "Phil Plait's Bad Astronomy Review of The Core"
    urls:
      - text: BadAstronomy.com
        url: 'http://www.badastronomy.com/bad/movies/thecore_review.html'
media:
  links:
    - text: iTunes
      url: 'https://tv.apple.com/movie/the-core/umc.cmc.yvdyg3p3l4njvh6secdj6ozx'
    - text: Amazon
      url: 'https://www.amazon.com/Core-Aaron-Eckhart/dp/B088PLB35C'
  title: The Core
  type: movie
  year: 2003
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_core_2003.jpg
  number: 254
  guid: core-flying-bricks-pacemaker-quantum-avian-eyeballs
  media:
    audio:
      audio/mpeg:
        content_length: 20223781
        duration: '23:56'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/the_core.final.mp3'

---
# Forgetability

The valley of forgetability (The Core) between science-respecting sci-fi (e.g. Arrival) and totally bonkers nonsense (e.g. Jupiter Ascending).

# Space shuttle

Landing the "flying brick."

# Bird navigation

Magnetite beaks. The possibility of quantum eyeball magnetic navigation HUDs. Corvid appreciation. Why birds don't all fly into our windows and our eyeballs.

# Pacemakers

Keeping your heart on-rhythm. Not as immediate a death sentence as portrayed.

# Earth's outer core

The absurd energies in the spinning of Earth's core - a ball of iron the size of Mars, spinning a thousand miles an hour

>  That's a whole lot of energy. If it doesn't sound like much, let's convert it to megatons: it's the equivalent energy of five trillion one megaton bombs going off. 
> Phil Plait [on the spinning outer core](http://www.badastronomy.com/bad/movies/thecore_review.html)
