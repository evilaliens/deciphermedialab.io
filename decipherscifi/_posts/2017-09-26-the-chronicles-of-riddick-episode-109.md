---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-09-26 07:45:52+00:00
description: 'Riddick! Strong jaws, ambush predators, interstellar ion drives and acceleration, Crematoria planetary science, solar terminators,color-changing creatures'
layout: podcast_post
permalink: /decipherscifi/the-chronicles-of-riddick-episode-109
redirect_from:
  - /decipherscifi/109
slug: the-chronicles-of-riddick-episode-109
title: 'The Chronicles of Riddick: Crematoria, necromongers, and ambush predator defense'
tags:
  - cults
  - dogs
  - evolution
  - faster-than-light travel
  - predators and prey
  - science
  - science fiction
  - space travel

# Episode particulars
image:
  path: assets/imgs/media/movies/the_chronicles_of_riddick_2004.jpg
  alt: Riddick standing dramatically over an army of Necromongers
links:
  - text: 'The Chronicles of Riddick: Escape From Butcher Bay/Assault on Dark Athena'
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Chronicles-Riddick-Assault-Dark-Athena-Xbox/dp/B001L18SBK'
  - text: The Chronicles of Riddick by Folding Ideas
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=X_CBtjBxiLY'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-chronicles-of-riddick-unrated/id280320495?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Chronicles-Riddick-Unrated-Directors-Cut/dp/B001BR5NLW'
  title: The Chronicles of Riddick
  type: movie
  year: 2004
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_chronicles_of_riddick_2004.jpg
  number: 109
  guid: 'https://decipherscifi.com/?p=3425'
  media:
    audio:
      audio/mpeg:
        content_length: 33509376
        duration: '39:52'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Chronicles_of_Riddick_--_--_decipherSciFi.mp3'
---
#### Necromongers

Where do I sign up? Scary space death cults. Necromonger gothic architecture and interior design. Frowny-faces.

#### Face on your face

Avoiding ambush predators via application of faces. Strong jaws. Designing against ambush predators. FACES EVWERYWHERE.

[Why Google’s Deep Dream A.I. Hallucinates In Dog Faces](https://www.fastcompany.com/3048941/why-googles-deep-dream-ai-hallucinates-in-dog-faces)

#### (Atmospheric) penetration

Atmospheric entry bombs. Landing invasion ships, overpressure, and displays of power. Also the blue ball planet-killing explosion thing.

#### Interstellar travel

Ion drive acceleration and speed and average distances between stars. The unlikeliness of non-FTL travel between star systems during the span of human lifetimes.

#### Private prison

The economics of the private prisons in this universe and how it's cost-effective to basically put a prison facility on Mercury.

#### Crematoria

Hot planets close to their sun. Lack of atmosphere and lack of green-house effect causing extremely quick extreme temperature changes.  This planet was pretty much Mercury. The impossibility of breathable atmosphere in these circumstances. The speed of the sunset and solar terminators.

#### Crematorian dog-things

Robustness of skin. Comparison to other color-changing creatures. Luminescence vs passive color-change.
