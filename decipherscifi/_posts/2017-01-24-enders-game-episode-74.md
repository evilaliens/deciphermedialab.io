---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-01-24 05:59:34+00:00 
description: "Ender's Game! Fighting space bugs, finding water in space, a story of dying planets and the canals on Mars, communicating with aliens, giant weapons, more!"
layout: podcast_post
permalink: /decipherscifi/enders-game-episode-74
redirect_from:
  - /decipherscifi/74
slug: enders-game-episode-74 
title: "Ender's Game: insectoid aliens, water in space, and lightspeed communications" 
tags:
  - aliens
  - fantasy
  - future
  - insects
  - military
  - science
  - science fiction
  - space
  - space combat
  - spaceships
  - war

# Episode particulars
image:
  path: assets/imgs/media/movies/enders_game_2013.jpg
  alt: "Asa Butterfield looking dramatic and some folks' heads, Ender's Game movie backdrop"
links:
  - text: Ender's Game by Orson Scott Card
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/enders-game/id378317492?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Enders-Game-Ender-Quartet-Book-ebook/dp/B003G4W49C'
  - text: Starship Stroopers on Decipher SciFi
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/6'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/enders-game/id723143800?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Enders-Game-Harrison-Ford/dp/B00ID4I6LA'
  title: "Ender's Game"
  type: movie
  year: 2013
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/enders_game_2013.jpg
  number: 74
  guid: 'http://www.decipherscifi.com/?p=1286'
  media:
    audio:
      audio/mpeg:
        content_length: 25729024
        duration: '35:43'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Enders_Game_--_--_DecipherSciFi.mp3'
---
#### Alien attacks

Alien attacks and death tolls. Independence Day and flying planes up the butt.

#### Water in spaaaaaaace

Liquid water and the Goldilocks Zone. Frozen water all over the galaxy. [Water vapor](http://www.space.com/12400-universe-biggest-oldest-cloud-water.html) too!

#### Dying Planets

Schiaparelli, Percival  Lowell and the Martian "canals."

#### Insectoid Aliens

Anatomy surprisingly like that of our tiny earth-based insects. Tool use? Telepathy? Queens controlling drones, but _directly_, apparently.

#### Alien Communication

Distance and lightspeed considerations in trying to communicate with aliens who _may_ pose an existential threat to humanity.

#### Deciphering Speeds and Distances

28 days. Distances to other stars.

#### Space Combat Command

Action per minute. Starcraft and command delegation.

#### Doomsday Weapons

[Very very expensive weapons](http://moneyinc.com/most-expensive-military-weapons-ever/) and the meaning of a dollar.
