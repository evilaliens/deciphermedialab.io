---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-03-26 07:45:58+00:00
description: 'Falling back in love with the Wachowskis. Teenage Power fantasies. Humans as horrible batteries. Benevolent AI dictators. Neuro-adaptations to The Matrix.'
layout: podcast_post
permalink: /decipherscifi/the-matrix-green-phosphors-neuroplasticity-and-rejecting-utopia-w-adrian-falcone
redirect_from:
  - /decipherscifi/186
slug: the-matrix-green-phosphors-neuroplasticity-and-rejecting-utopia-w-adrian-falcone
title: 'The Matrix: green phosphors, neuroplasticity, and rejecting utopia  w/ Adrian Falcone'
tags:
  - artificial intelligence
  - computing
  - cyberpunk
  - hacking
  - science
  - science fiction
  - simulation hypothesis
  - the matrix
  - virtual reality

# Episode particulars
image:
  path: assets/imgs/media/movies/the_matrix_1999.jpg
  alt: 'Really cools 90s leather-goth hackers looking dramatic, Matrix movie backdrop'
links:
  - text: 'The Backwards Brain Bicycle by Smarter Every Day'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=MFzDaBzBlL0'
  - text: 'The Animatrix'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/the-animatrix/id575781229?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Animatrix-Complete-First-Season/dp/B004G7W9IY'
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=iieuwaHJ9us'
  - text: "The Pizza Matrix (it's terrible)"
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=KhvhmtUS128'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-matrix/id271469518?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Matrix-Keanu-Reeves/dp/B001XVD2Z0'
    - text: YouTube
      url: 'https://www.youtube.com/watch?v=qEXv-rVWAu8'
  title: The Matrix
  type: movie
  year: 1999
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: adrian_falcone
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_matrix_1999.jpg
  number: 186
  guid: 'https://decipherscifi.com/?p=10347'
  media:
    audio:
      audio/mpeg:
        content_length: 46460031
        duration: '55:17'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Matrix_--_Adrian_Falcone_--_Decipher_SciFi.mp3'

---
#### This movie is awesome

It is! And also we're old. But honestly this is still unironically the best thing. The legacy of the Matrix. Falling back in [love]({% link decipherscifi/_posts/2016-06-28-jupiter-ascending-episode-44.md %}) with the Wachowskis. Addressing the sequels.

#### Cyberpunk

The Matrix as cyberpunk. Comparing against other touchstones. Philip K Dick. William Gibson's [review](http://www.williamgibsonbooks.com/archive/2003_01_28_archive.asp#90244012) from the time.

> When I began to write NEUROMANCER, there was no “cyberpunk”. THE MATRIX is arguably the ultimate “cyberpunk” artifact. Or will be, if the sequels don’t blow. I hope they don’t, and somehow have a hunch they won’t, but I’m glad I’m not the one who has to worry about it.
> 
> -William Gibson, lol

#### Old school computing

Damn kids and their pixels. CRT displays and low/high persistence phosphorus and how we wound up irl with the bright glowing green text aesthetic that informed The Matrix. Eye fatigue back in the day. Computer-touchers. Staring at people at work for fun and profit. The [Solarized color schemes](https://ethanschoonover.com/solarized/).

#### The Matrix

Taking the red pill. How easy it actually really is to explain what The Matrix is. Neo is a giant, slimy baby. The inefficiencies of using humans as batteries when you have fusion power. Grass, _tastes bad_.

#### Benevolent AI Dictators

But _WHAT IF_ The Matrix actually has humanity's best interests in mind? The possibility that they really tried to give us our utopia.

#### Virtual worlds

Plasticity and your brain's willingness to accept new input schemes. Designing The Matrix with a "failsafe" so the humans are unadapted to their physical bodies irl. Acquired synaesthesia. Supernumerary limbs and phallic cartwheels. Upside down goggles and adaptation to a new "up." Permanence in sensory adaptations.

{% youtube "https://www.youtube.com/watch?v=jKUVpBJalNQ" %}

{% youtube "https://www.youtube.com/watch?v=Wm8ZoVQ_OJo" %}

#### Hacking The Matrix

Bendable physics. The difficulty with bending or breaking the rules of computing hardware.
