---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - tv
date: 2020-02-11 07:45:29+00:00
description: 'Picard is back! The best star Fleet captain. The life-cycles of stars. Detecting supernovae. Federation economics. Data and Lore. Borg self-protection. Etc.'
layout: podcast_post
permalink: /decipherscifi/picard-the-moral-center-of-trek-supernovae-and-romulan-stormtroopers
redirect_from:
  - /decipherscifi/228
slug: picard-the-moral-center-of-trek-supernovae-and-romulan-stormtroopers
title: 'Picard: the moral center of Trek, supernovae, and Romulan stormtroopers'
tags:
  - science fiction
  - science
  - artificial intelligence
  - robots
  - androids
  - economics
  - supernovae
  - star

# Episode particulars
image:
  path: assets/imgs/media/tv/picard_2020.jpg
  alt: 'Picard and his dog on a peaceful vineyard in the future'
links:
  - text: 'Star Trek Beyond: drone swarms, teleporters, and decompression w/ Jolene Creighton'
    urls:
      - text: 'Decipher SciFi'
        url: 'https://deciphermedia.tv/decipherscifi/63'
  - text: 'Star Trek First Contact: transhumanism technology and ant behavior w/ Jolene Creighton'
    urls:
      - text: 'Decipher SciFi'
        url: 'https://deciphermedia.tv/decipherscifi/52'
  - text: 'Star Trek IV – The Journey Home: whale margarine, transparent aluminum, and time travel'
    urls:
      - text: 'Decipher SciFi'
        url: 'https://deciphermedia.tv/decipherscifi/195'
  - text: 'Star Trek Voyager: water, galactic scales, and coffee replicators'
    urls:
      - text: 'Decipher SciFi'
        url: 'https://deciphermedia.tv/decipherscifi/214'
  - text: 'Galaxy Quest: the best Star Trek movie'
    urls:
      - text: 'Decipher SciFi'
        url: 'https://deciphermedia.tv/decipherscifi/7'
  - text: 'Star Trek TNG'
    urls:
      - text: 'Netflix'
        url: 'https://www.netflix.com/title/70158329'
  - text: 'The Picard Song by DarkMateria'
    urls:
      - text: 'YouTube'
        url: 'https://www.youtube.com/watch?v=caIsN_PjaHY'
media:
  links:
    - text: 'CBS All Access'
      url: 'https://www.cbs.com/shows/star-trek-picard/'
    - text: Amazon
      url: 'https://www.amazon.com/Star-Trek-Picard/dp/B07S5HX14T'
  title: Picard
  type: tv
  year: 2020
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/picard_2020.jpg
  number: 228
  guid: 'https://decipherscifi.com/?p=10929'
  media:
    audio:
      audio/mpeg:
        content_length: 37063764
        duration: '44:01'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/picard.output.mp3'

---
In which we discuss the first three episodes

#### Picard

We missed you Jean-Luc! Picard as the moral center of Star Trek, and the greatest of its captains.

#### Romulan supernova

Romulans. How to tell a Vulkan from a Romulan? Exploding stars. Detecting supernovae. The death cycle of a red supergiant. The reliability of physics across timelines/universes. The dangers of cleaving planets.

#### Federation Economics

“Post scarcity” with holodecks and replicators, and the realization that there is still a limited resource: energy. Star Trek [Kardashev levels](https://en.wikipedia.org/wiki/Kardashev_scale).

#### Synthetic life

Data. Lore. Data’s “daughter” from that one time on TNG. Bio-synthetic intelligent life. How important is embodiment?
