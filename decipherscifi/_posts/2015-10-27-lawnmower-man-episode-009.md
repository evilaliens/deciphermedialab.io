---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2015-10-27 01:45:58+00:00'
description: 'With Lawnmower Man on the podcast we talk virtual reality, 3d graphics in 1992, the past present and future of VR, Oculus Rift, Holodecks, nootropics'
layout: podcast_post
permalink: /decipherscifi/lawnmower-man-episode-009
redirect_from:
  - /decipherscifi/9
slug: lawnmower-man-episode-009
title: 'Lawnmower Man: mind uploading, vr, and 90s computer games'
tags:
  - 3d animation
  - augmented reality
  - classic gaming
  - computer generated graphics
  - holodeck
  - intelligence
  - man versus machine
  - nootropics
  - science fiction
  - virtual reality

# Episode particulars
image:
  path: assets/imgs/media/movies/the_lawnmower_man_1992.jpg
  alt: Lawnmower Man VR backdrop
links:
  - text: 'Community: Lawnmower Maintenance and Postnatal Care'
    urls:
      - text: 'Stream on Yahoo!'
        url: 'https://screen.yahoo.com/community/lawnmower-maintenance-postnatal-care-070001912.html'
      - text: 'Episode info on the Community Wiki'
        url: 'http://community-sitcom.wikia.com/wiki/Lawnmower_Maintenance_and_Postnatal_Care'
  - text: 'Daemon - by Daniel Suarez'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/daemon/id357997796?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Daemon-Daniel-Suarez-ebook/dp/B003QP4NPE'
  - text: 'Freedom TM - by Daniel Suarez'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/audiobook/daemon-unabridged/id301933874?at=1001l7hP&mt=3'
      - text: Amazon
        url: 'http://www.amazon.com/Freedom-TM-Daemon-Book-2-ebook/dp/B002VUFKDY'
  - text: 'Altered Carbon - by Richard K Morgan'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/altered-carbon/id419980024?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Altered-Carbon-Takeshi-Kovacs-Novels-ebook/dp/B000FBFMZ2'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/lawnmower-man/id310466742?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Lawnmower-Man-Pierce-Brosnan/dp/B0013YW7SU'
  title: Lawnmower Man
  type: movie
  year: 1992
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 9
  guid: 'http://www.decipherscifi.com/?p=308'
  media:
    audio:
      audio/mpeg:
        content_length: 28255368
        duration: '39:14'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Lawnmower_Man_decipherSciFi.mp3'
---
#### The Novel

The title was taken from a Stephen King short story which pretty much otherwise has no relationship at all to the film. This actually led to a lawsuit.

#### Director Brett Leonard

Was praised at the time for his vision. Virtuosity.

#### 3D Graphics at the Time of the Film

Wolfenstein 3D. Compact discs technology. Sega CD. The cost of this film in time and money. How impressed we all were at the time. Sega console monstrosity. The [Sega Tower of Power](https://www.usgamer.net/articles/sega-sends-out-mini-genesis-towers-of-power-teases-more-to-come).

#### Virtual Reality: Past

What we thought it would be at the time of the film.

#### Virtual Reality: Present

360° "VR" movies. Google cardboard. AR. How directed do we want our experiences with art to be?

#### Virtual Reality: Future

VR training. Holodeck. Pornography. Awesome-sounding VR "theme park" in Utah.
[youtube]cML814JD09g[/youtube]

#### Becoming the Lawnmower Man

Nootropics. The singularity. Controlling the real world via the internet.
