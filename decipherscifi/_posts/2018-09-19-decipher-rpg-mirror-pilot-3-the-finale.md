---
# General post frontmatter
categories:
  - episode
  - rpg
date: 2018-09-19 07:45:19+00:00
description: 'Airships, and FINALLY Bodyguard Chris manages to turn the tables & make something of his repeated failures. Emperor Colbert tries not to die before the end.'
layout: podcast_post
permalink: /decipherscifi/decipher-rpg-mirror-pilot-3-the-finale
redirect_from:
  - /decipherscifi/rpg3
slug: decipher-rpg-mirror-pilot-3-the-finale
title: 'Decipher RPG - Mirror Pilot #3 (the finale!)'
tags:
  - actual play
  - fantasy
  - rpg

# Episode particulars
image:
  path: assets/imgs/media/rpg/mirror03.jpg
  alt: 'Mirror RPG character illustrations for episode 3 of 3'
links:
media:
  links:
    - text: Itch.io
      url: 'https://sandypuggames.itch.io/mirror-a-micro-rpg'
  title: Mirror
  type: rpg
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - player
  - data_name: lee_colbert
    roles:
      - player
  - data_name: liam_ginty
    roles:
      - game_master
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/rpg/mirror03.jpg
  guid: 'https://decipherscifi.com/?p=7701'
  media:
    audio:
      audio/mpeg:
        content_length: 25389056
        duration: '30:03'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/rpg.01.mirror.ep03.final.mp3'
---

_Our heroes make a real mess of an airship or two, and FINALLY_ Bodyguard Chris manages to turn the tables and make something of his repeated failures. Emperor Colbert tries not to die before the end.

(This podcast is the finale of our pilot series for Decipher RPG where we play [Mirror](http://www.sandypuggames.com/sandy-pug-orginals/) by Sandy Pug Games with [Liam Ginty]({% link _people/liam_ginty.md %}).

Previously: [Part 1]({% link decipherscifi/_posts/2018-09-11-decipher-rpg-mirror-test-1.md %}) & [Part 2]({% link decipherscifi/_posts/2018-09-12-decipher-rpg-mirror-pilot-2.md %}))
