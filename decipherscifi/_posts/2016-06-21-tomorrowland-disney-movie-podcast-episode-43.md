---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-06-21 07:00:40+00:00
description: 'With Tomorrowland on the podcast we talk science fiction utopianism, rational optimism, Walt Disney, Worlds Fair, feeding wolves, past visions of the future'
layout: podcast_post
permalink: /decipherscifi/tomorrowland-disney-movie-podcast-episode-43
redirect_from:
  - /decipherscifi/43
slug: tomorrowland-disney-movie-podcast-episode-43
title: "Tomorrowland: the World's Fair, retrofuturism, and making dreams reality"
tags:
  - cities
  - disney
  - dreams
  - futurism
  - invention
  - retro futurism
  - science
  - science fiction
  - walt disney

# Episode particulars
image:
  path: assets/imgs/media/movies/tomorrowland_2015.jpg
  alt: Tomorrowland corn field city movie backdrop
links:
  - text: 'Walt Disney - The Triumph of the American Imagination by Neal Gabler'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/walt-disney/id419282479?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Walt-Disney-Neal-Gabler-ebook/dp/B000MAH7N6'
  - text: A 19th Century Vision of the Year 2000
    urls:
      - text: The Public Domain Review
        url: 'http://publicdomainreview.org/collections/france-in-the-year-2000-1899-1910/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/tomorrowland/id990865721?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Tomorrowland-Bonus-Features-George-Clooney/dp/B00Y2DV3PM'
  title: Tomorrowland
  type: movie
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/tomorrowland_2015.jpg
  number: 43
  guid: 'http://www.decipherscifi.com/?p=840'
  media:
    audio:
      audio/mpeg:
        content_length: 19174364
        duration: '31:57'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Tomorrowland_decipherSciFi.mp3'
---
#### Brad Bird

50s and 60s techno optimism. The Iron Giant. The Incredibles.

#### World's Fair

Stages of the worlds fair. Our tech and relationship to it.

#### Walt Disney

Futurism. Techno optimism. Shaping reality to fit his vision.

#### Tomorrowland

Disneyland. EPCOT. Disney's vision for a designed city.

#### A sense of wonder

Childlike optimism. Positivity in the face of pessimism. Three wolf moon.

#### Future Visions

Visions of technology and the future  from different periods.

{% responsive_image_block %}
  path: 'assets/imgs/site/404-bearisdriving.jpg'
  alt: 'omg car is driving how can that be'
  caption: 'omg car is driving how can that be'
{% endresponsive_image_block %}
