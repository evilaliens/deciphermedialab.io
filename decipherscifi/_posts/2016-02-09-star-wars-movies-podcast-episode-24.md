---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-02-09 07:13:48+00:00
description: 'On the podcast this week we finally talk about the content of the Star Wars movies! Fandom, childlike wonder, death stars, Luke is a mass murderer, more'
layout: podcast_post
permalink: /decipherscifi/star-wars-movies-podcast-episode-24
redirect_from:
  - /decipherscifi/24
slug: star-wars-movies-podcast-episode-24
title: 'Star Wars: the death star, planet science, and appreciating the original Star Wars trilogy'
tags:
  - science fiction
  - science
  - star wars
  - mass murder
  - fandom
  - space
  - death star
  - osha

# Episode particulars
image:
  path: assets/imgs/media/movies/star_wars_movies.jpg
  alt: Star Wars movies backdrop
links:
  - text: Star Wars Movies
    urls:
  - text: Star Wars 'Filmumentaries' by Jamie Benning
    urls:
      - text: Filmumentaries
        url: 'http://filmumentaries.com/fullvideos/'
  - text: Robot Chicken Star Wars Parodies
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/robot-chicken-star-wars/id295878933?i=296424736&at=1001l7hP&mt=4'
      - text: Amazon
        url: 'http://www.amazon.com/Robot-Chicken-Star-Wars/dp/B00JZXHAAG'
  - text: Bad Lip Reading Star Wars (especially the songs!)
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Sv_hGITmNuo&list=PLDWOaLX7rvz2c9FovuAIFcocP--3o2oI7'
  - text: Emo Kylo Ren
    urls:
      - text: Twitter
        url: 'https://twitter.com/kylor3n'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie-collection/star-wars-digital-movie-collection/id982709307?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Star-Wars-Digital-Movie-Collection/dp/B00VJ04TH0'
  title: Star Wars Movies
  type: movie
  year: '1977-2015'
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 24 
  guid: 'http://www.decipherscifi.com/?p=507'
  media:
    audio:
      audio/mpeg:
        content_length: 26932181
        duration: '44:52'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Star_Wars_Movies_decipherSciFi.mp3'
---
#### People Really Love This Franchise

Fandom. Why do people love this so much?

#### Why Are We Not Bananas About Star Wars?

Fantasy vs. science fiction. Humanities, sciences. Star Wars is a "fairy tale."

#### Childhood Memories

People bonding with Star Wars in their youth. Watching it with the kids. There are younger people who actually dig the prequel trilogy, believe it or not.

#### Science in Star Wars

Canons. Robots. Spaceships. FTL. Gravity. The Force. Planets. Language.

#### Death Star Scales

Big, bigger, and biggest! Engineering and construction. Military and civilian populations.

#### Technology and The Force

"I find your lack of faith disturbing."
