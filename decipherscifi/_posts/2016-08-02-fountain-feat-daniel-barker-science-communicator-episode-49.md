---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-08-02 07:30:32+00:00
description: 'Daniel James Barker of Uncertainty Principle Podcast joins us to decipher The Fountain - stellar nurseries, immortality, views on death, mayan civ, more'
layout: podcast_post
permalink: /decipherscifi/fountain-feat-daniel-barker-science-communicator-episode-49
redirect_from:
  - /decipherscifi/49
slug: 
title: 'The Fountain: nebulae, mesoamerica, and finding meaning w/ Daniel Barker'
tags:
  - science fiction
  - science
  - immortality
  - medicine
  - fantasy
  - love
  - religion
  - faith
  - space
  - nebulae
  - history

# Episode particulars
image:
  path: assets/imgs/media/movies/the_fountain_2006.jpg
  alt: Bald Hugh Jackman in a floating tree ball in space with Rachel Weiss The Fountain movie backdrop
links:
  - text: The Fountain graphic novel by Daren Aronofsky and Kent Williams
    urls:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/book/the-fountain/id882991278?mt=11&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Fountain-DARREN-ARONOFSKY-ebook/dp/B00K7EIY7K'
  - text: Darren Aronofsky remastered and mixed Director Commentary
    urls:
      - text: David Weisbrod
        url: 'http://www.weisbrodimaging.com/blog/2010/11/18/directors-commentary-of-aronofskys-the-fountain-remastered-for-download/'
  - text: Uncertainty Principle the Podcast
    urls:
      - text: Uncertainty Principle
        url: 'https://uncertaintyprinciplethepodcast.com/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-fountain-2006/id315781038?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Fountain-Hugh-Jackman/dp/B000RZAENG'
  title: The Fountain
  type: movie
  year: 2006
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: daniel_barker
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_fountain_2006.jpg
  number: 49
  guid: 'http://www.decipherscifi.com/?p=933'
  media:
    audio:
      audio/mpeg:
        content_length: 27470636
        duration: '45:46'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Fountain_Daniel_Barker_decipherSciFi.mp3'
---
#### Aronofsky

Shooting for the moon. Making beautiful film.

#### History

Olmec, Mayans, Aztec. Murder factories.

#### Nebulae

Particularity. Stellar nurseries.

#### Life and Death

Differing viewpoints, religious and atheist.
