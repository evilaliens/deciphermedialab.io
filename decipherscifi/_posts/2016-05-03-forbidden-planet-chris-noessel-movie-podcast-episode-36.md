---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-05-03 07:22:39+00:00
description: 'Guest co-host Chris Noessel is on the show to talk about Forbidden Planet, robot design, scuttling procedures, 50s gender roles, alien biology, linguistics'
layout: podcast_post
permalink: /decipherscifi/forbidden-planet-chris-noessel-movie-podcast-episode-36
redirect_from:
  - /decipherscifi/36
slug: forbidden-planet-chris-noessel-movie-podcast-episode-36
title: 'Forbidden Planet: robust design in AI, exobiology, and philology w/ Chris Noessel'
tags:
  - science fiction
  - science
  - language
  - linguistics
  - deep space exploration
  - design
  - user interfaces
  - space ship
  - star trek
  - artificial intelligence

# Episode particulars
image:
  path: assets/imgs/media/movies/forbidden_planet_1956.jpg
  alt: Forbidden Planet robot on alien landscape movie backdrop
links:
  - text: The Design of Everyday Things by Donald Norman
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/the-design-of-everyday-things/id677234093?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Design-Everyday-Things-Revised-Expanded-ebook/dp/B00E257T6C'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/forbidden-planet/id289899327?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Forbidden-Planet-Walter-Pidgeon/dp/B00902S0IS'
  title: Forbidden Planet
  type: movie
  year: 1956
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: chris_noessel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/forbidden_planet_1956.jpg
  number: 36
  guid: 'http://www.decipherscifi.com/?p=744'
  media:
    audio:
      audio/mpeg:
        content_length: 43791136
        duration: '01:12:58'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Forbidden_Planet_Chris_Noessel_decipherSciFi.mp3'
---
#### Robbie the Robot

Prop design. AI language understanding. Super strength and a matter replicator. Bad reactions to conflicting orders. Like a mechanical calculator dividing by zero!

{% youtube "https://www.youtube.com/watch?v=ByWXR7WzkIE" %}

#### Morbius

He's totally Zod! Philology. Engineering. Dangerous tech designs.

#### Gender Stuff

Cutting edge sexiness in the 50s. Innocent virgins. Blaming the victim. To the captain go the spoils. The deleted scenes that explain the animal behaviour:

{% youtube "https://www.youtube.com/watch?v=P8aL06zsPIU" %}

#### Decoding the Krell

Best attempts at judging Krell biology from their technology and architecture. Their "plastic educator." Deciphering their language. Using alien Wikipedia on microfilm. Vannevar Bush's [Memex](https://en.wikipedia.org/wiki/Memex). The alien arithmetic problem.

{% responsive_image_block %}
  path: 'assets/imgs/misc/jef_raskin_alien_math.png'
  alt: "Jef Raskin's Alien Arithmetic Problem"
  caption: "Jef Raskin's Alien Arithmetic Problem"
  attr_text: 'Jef Raskin'
{% endresponsive_image_block %}

#### Self Destruct

A comparison of scuttling procedures in Alien and Forbidden Planet. Speculation about Krell hearing and sight frequencies.
