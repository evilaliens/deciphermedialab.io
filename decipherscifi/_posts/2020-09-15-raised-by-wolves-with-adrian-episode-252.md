---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - tv
date: 2020-09-15T02:22:48-04:00
description: 'Subluminal interstellar travel. The fraigility of human cargo. Special floaty screaming robots. Face replacement. Etc.'
layout: podcast_post
redirect_from:
  - /decipherscifi/252
slug: raised-by-wolves-exoplanets-eating-rats-fake-karate-floaty-physics
title: 'Raised by Wolves: exoplanets, eating rats, and fake-karate floaty physics'
tags:
  - aliens
  - cooking
  - exoplanets
  - gestation
  - ridley scott
  - science
  - science fiction
  - space exploration
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/tv/raised_by_wolves_2020.jpg
  alt: 'Ominous android lady considering a tiny baby'
links:
  - text: 'Exoplanet Exploration - Planets Beyond our Solar System'
    urls:
      - text: NASA
        url: 'https://exoplanets.nasa.gov/'
media:
  links:
    - text: HBO Max
      url: 'https://www.hbomax.com/series/urn:hbo:series:GX0WFcAlf5r5cuAEAAADu'
  title: Raised by Wolves
  type: tv
  year: 2020
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: adrian_falcone
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/raised_by_wolves_2020.jpg
  number: 252
  guid: raised-by-wolves-exoplanets-eating-rats-fake-karate-floaty-physics
  media:
    audio:
      audio/mpeg:
        content_length: 39543107
        duration: '47:01'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/raised_by_wolves.final.mp3'

---
# Happy 5-year anniversary whoooooo

🥳🎉

# Subluminal interstellar travel

Accelerating for half of the ride, and braking for the other half. The advantage of avoiding squishy human cargo for high acceleration. The difference between the Alucard and the [Alcubierre](https://en.wikipedia.org/wiki/Alcubierre_drive) drive technologies (hint: a miserable pile of secrets). Special floaty physics, magnets on your feet, and fake karate.

# Exoplanets

Exploring the growing options for Earth-like life in the solar-system. Kepler 22b. Where did the megafauna go? Exogestation - growing babies on alien soil.

# Meat stuff

Sourcing and preparing rat meat. Snails. Face/Off and changing your identity.
