---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-06-26 04:00:10+00:00
description: "The science of aircraft rescue systems. Hypospray. Our difficulties with John Carpenter's cynicism. Chandelier-cars as a sign of success. Fusion energy!"
layout: podcast_post
permalink: /decipherscifi/escape-from-new-york-ultralight-aircraft-escape-pods-and-fusion-reactors
redirect_from:
  - /decipherscifi/148
slug: escape-from-new-york-ultralight-aircraft-escape-pods-and-fusion-reactors
title: 'Escape From New York: ultralight aircraft, escape pods, and fusion reactors'
tags:
  - aircraft
  - dystopia
  - fusion
  - medicine
  - new york city
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/escape_from_new_york_1981.jpg
  alt: Snake Plissken crouching like a total badass
links:
  - text: 'The Thing: antarctic explorers, extreme doctoring, and figuring out aliens w/ Cae & Ryan'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/148'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/escape-from-new-york/id849434760?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Escape-New-York-Kurt-Russell/dp/B00B19DW0E'
  title: Escape From New York
  type: movie
  year: 1981
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/escape_from_new_york_1981.jpg
  number: 148
  guid: 'https://decipherscifi.com/?p=6741'
  media:
    audio:
      audio/mpeg:
        content_length: 28814617
        duration: '34:17'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Escape_From_New_York_--_--_Decipher_SciFi.mp3'
---
#### Dystopia

New York city post WW3. Crime in the US in the early 80s.

#### Airplane failure recovery

The good old days when Presidents could be bald. Plane escape mechanisms. Parachutes vs ejection seats vs ejection pods.

{% youtube "https://www.youtube.com/watch?v=kQyrPVIIQdE" %}

#### Jet injectors

Vaccinations by high-pressure jet. Finding out these are real-life. Polio vs Hepatitis.

#### Gliders

Large wingspan, light body. The Twin Towers. Clever practical FX for wireframe nighttime navigation display. Comparing budgets with Tron.

#### Fusion

Time projections. Cold fusion and hot fusion. Taking the shortest route and simply building a Dyson sphere. Waste production and failure modes.
