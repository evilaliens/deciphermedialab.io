---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-07-30 07:45:35+00:00
description: "Lockout! Remaking 'Escape From New York,' but in space. Flying the 'un-flyable' space shuttle. Skydiving from 'the egde of space.' Breaking sound barriers."
layout: podcast_post
permalink: decipherscifi/lockout-plagiarism-space-shuttle-design-and-how-to-skydive-from-space
redirect_from:
  - /decipherscifi/204
slug: lockout-plagiarism-space-shuttle-design-and-how-to-skydive-from-space
title: 'Lockout: plagiarism, space shuttle design, and how to skydive... from space!'
tags:
  - inert gas asphyxiation
  - orbits
  - science
  - science fiction
  - sound
  - space
  - space shuttle
  - space travel

# Episode particulars
image:
  path: assets/imgs/media/movies/lockout_2012.jpg
  alt: 'Skinny space-prisoner drinking space-milk'
links:
  - text: 'How to Land the Space Shuttle... from Space by Bret Copeland'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Jb4prVsXkZU'
  - text: 'Escape From New York: ultralight aircraft, escape pods, and fusion reactors'
    urls:
      - text: Decipher SciFi
        url: 'https://decipherscifi.com/148'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/lockout-unrated/id540138002?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Lockout-Guy-Pearce/dp/B008JH5CVS'
  title: Lockout
  type: movie
  year: 2012
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/lockout_2012.jpg
  number: 204
  guid: 'https://decipherscifi.com/?p=10714'
  media:
    audio:
      audio/mpeg:
        content_length: 26697598
        duration: '31:40'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/lockout_2012.mp3'

---
#### Plagiarism

John Carpenter, this movie, and [Escape from New York]({% link decipherscifi/_posts/2018-06-26-escape-from-new-york-ultralight-aircraft-escape-pods-and-fusion-reactors.md %}) plagiarism. Metal Gear Solid _also_ as an _Escape_ ripoff. Running popular media through a "Hideo Kojima filter" to see what comes out.

#### Space Shuttles

Side-mounting your spacecraft. Flying  a brick. Deorbiting and landing in the space shuttle's "orbiter." Wiggle-worm descent. Having only one chance because all of your fuel was spent getting out of orbit. Returning to shuttle-like designs with mag-lev mountain-ramps.

#### Inert gas asphyxiation

Nitrogen! Biologically inactive gas asphyxia vs potentially-toxic: CO2, CO, etc. Hypoxia.

#### Falling out of space

The slim odds of hitting the ISS when accidentally de-orbiting. Space is... _small_? Understanding what "orbit" really means. HALO/HAHO jumping, [Baumgartner's Stratos jump](https://www.youtube.com/watch?v=dYw4meRWGd4&t=149s), and the incredible difficulty of jumping out of _orbit ffs_. Copyrighting all of the "totally radical" stunts.

#### Breaking the sound barrier

The difference in the "speed of sound" at different atmospheric densities. Terminal velocity at high altitudes. Air is more "sticky" than you might expect. Why didn't Felix Baumgartner "burn up" on reentry?

#### Orbital decay

"Jumping down" from a space station and how that is not sufficient for deorbiting. Megaconstellations and space junk.
