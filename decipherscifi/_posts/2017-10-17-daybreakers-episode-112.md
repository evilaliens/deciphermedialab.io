---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-10-17 07:45:04+00:00
description: 'Daybeakers: vampire science! Blood transfusion history, pre-modern medicine, classic vampires, vampire bats, and the possibility of medical vampirism!'
layout: podcast_post
permalink: /decipherscifi/daybreakers-episode-112
redirect_from:
  - /decipherscifi/112
slug: daybreakers-episode-112
title: 'Daybreakers: medical vampirism, living without a heartbeat, and blood substitutes'
tags:
  - biology
  - blood
  - science
  - science fiction
  - vampires
  - vampirism

# Episode particulars
image:
  path: assets/imgs/media/movies/daybreakers_2009.jpg
  alt: A deep warehouse of human bloodbags
links:
  - text: "Dracula: Vlad the Impaler, Elizabethan sexuality, and Kenau's awesome accent w/ Joe Ruppel"
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/58'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/daybreakers/id361114212?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Daybreakers-Ethan-Hawke/dp/B003H54E20'
  title: Daybreakers
  type: movie
  year: 2009
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/daybreakers_2009.jpg
  number: 112
  guid: 'https://decipherscifi.com/?p=3713'
  media:
    audio:
      audio/mpeg:
        content_length: 27463680
        duration: '32:41'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Daybreakers_--_--_decipherSciFi.mp3'
---
#### Classical vampires...?

And is this a horror movie? Better than the sparkly Twilight ones.

#### Blood-squirting lizards

Just because that's awesome

#### Blood substitutes

Substitutes for what part or what function of blood? Making a blood sword on our [Conan episode]({% link decipherscifi/_posts/2016-12-27-conan-the-barbarian-82-feat-joe-ruppel-history-nerd-episode-70.md %}). What blood is made of. What blood does for us biologically.

> It's got plasmatic electrolytes
>
> It's what bodies crave

#### Blood transfusion

Blood types and our developing understanding during the 17th and 19th centuries. [Carlos Lammerstammer's](https://en.wikipedia.org/wiki/Karl_Landsteiner) initial work distinguishing the blood types. Pre-modern medicine and the throw-everything-at-the-wall-and-see-who-dies method of medical research, e.g. injecting milk or ale or whiskey into the blood to see what happens.

#### Vampire blood flow

We're told and see demonstrated that the vampires have no heartbeat, BUT - the other things we observe suggest that they do have blood flow, somehow. Intravenous vampire blood. Vampire blood extraction. Vampire peristalsis.

#### Medical vampirism

"In case of human frailty, break glass." With a "cure" for vampirism available, the ensuring rise of using vampirism as a cure for deadly ailments. But you only get to use it once, so make it count! Connections between vampire lore and the possibility of [parabiosis](https://futurism.com/young-blood-combat-aging-thanks-just-one-protein/).

#### Downsides to vampirism

Blood isn't really usually considered delicious. Ethical concerns with surviving on human blood UNTIL the "blood substitute" is developed.
