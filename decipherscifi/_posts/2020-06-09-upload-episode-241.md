---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - tv
date: 2020-06-09T02:45:06-04:00
description: 'Autonomous vehicle safety. The economics of digital eternity. Destructive brain scanning. Brain damage and resuscitation. VR haptic suits. Sharding. Memory manipulations. Etc.'
layout: podcast_post
redirect_from:
  - /decipherscifi/241
slug: upload-living-forever-digital-personhood-hacking-reality
title: 'Upload: living forever, digital personhood, and hacking your own reality'
tags:
  - augmented reality
  - autonomous vehicles
  - brain scanning
  - economics
  - science
  - science fiction
  - virtual reality

# Episode particulars
image:
  path: assets/imgs/media/tv/upload_2020.jpg
  alt: 'Girl with a VR headset Upload media image'
links:
  - text: 'The Teslasuit literally shocked me by CNET'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=KC83goKZXK8'
media:
  links:
    - text: Prime Video
      url: 'https://www.amazon.com/Five-Stars/dp/B08597SL96'
  title: Upload
  type: tv
  year: 2020
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/upload_2020.jpg
  guid: upload-living-forever-digital-personhood-hacking-reality
  number: 241
  media:
    audio:
      audio/mpeg:
        content_length: 38458025
        duration: '45:44'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/upload.final.mp3'

---
# Self-driving cars

Garbage in/garbage out with ML training. Whether or not you allow the user to protect the driver at the expense of the pedestrian, those deicisions are still being made. The advantage of rolling blacked-out rooms on the commute. Hacking autonomous vehicles.

# Economics

Job competition in "knowledge work" when people live functionally infinitely and tirelessly on electricity. Loopholes for the wealthy.

# Brain scanning

Electrical mapping of the brain. Desctructive neural scanning. Do we even have any other options for mapping our brain structure? Brain damage and resuscitation.

# Digital personhood

Creating immortal and infinite digital slaves. Multiple instantiation of persons. Hormonal simulation. Physical, mental, and emotional growth when your life is "virtual." Living longer and learning not to be the horrible old racist grandpa. Sense of self over time when uploaded. What virtual conditions could be so bad that it's not worth "living" anymore? Waiting for democratized afforable versions of necessary technologies. "[ROM hacking]({% link decipherscifi/_posts/2020-04-07-isolated-bs.md %})" your reality.

# VR

Sharding (not sharting) your virtual worlds. Manipulating and filtering memories into re-experience. VR suit options: piezoelectric, pneumatic, and [TENS](https://en.wikipedia.org/wiki/Transcutaneous_electrical_nerve_stimulation).
