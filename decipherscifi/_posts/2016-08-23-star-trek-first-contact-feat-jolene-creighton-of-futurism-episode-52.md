---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-08-23 07:25:05+00:00
description: 'Jolene Creighton of Futurism.com joins us to talk Star Trek First Contact. Which brings us to much borg, ant and bee behavior, time travel, warp drive, more'
layout: podcast_post
permalink: /decipherscifi/star-trek-first-contact-feat-jolene-creighton-of-futurism-episode-52
redirect_from:
  - /decipherscifi/52
slug: star-trek-first-contact-feat-jolene-creighton-of-futurism-episode-52
title: 'Star Trek First Contact: transhumanism technology and ant behavior w/ Jolene Creighton'
tags:
  - science fiction
  - science
  - star trek
  - borg
  - insects
  - time travel
  - economics
  - evolution
  - ftl
  - antimatter

# Episode particulars
image:
  path: assets/imgs/media/movies/star_trek_first_contact_1996.jpg
  alt: Sytar Trek borg heads movie backdrop
links:
  - text: Star Trek
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/s/ref=nb_ss_gw?url=search-alias%3Daps&field-keywords=star%20trek'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/star-trek-viii-first-contact/id209240113?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Star-Trek-VIII-First-Contact/dp/B000IZ8SD8'
  title: 'Star Trek: First Contact'
  type: movie
  year: 1996
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: jolene_creighton
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/star_trek_first_contact_1996.jpg
  number: 52
  guid: 'http://www.decipherscifi.com/?p=967'
  media:
    audio:
      audio/mpeg:
        content_length: 31491409
        duration: '52:28'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Star_Trek_First_Contact_Jolene_Creighton_decipherSciFi.mp3'
---
#### Time Travel Choices

Why 2063? World War 3. Population. Booming warp drive economy.

#### Borg Fighting

Defensive strategies. Best weapons for fighting the borg: bats or sneakers?

#### Borg Origins

"Borgigins." Transhumanism. Definitions of evolution.

#### Borg Behaviour/Design

Like ants! And maybe bees a little. But mostly ants! Comm techs. Bee-dance grammar.

#### Warp Drive

Alcubbiere drives, distorting space-time. Antimatter is expensive!
