---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - topic
  - tv
date: '2015-12-29 05:45:41+00:00'
description: 'Some of our favorite sci-fi tv shows, new and old. Starting with Star Trek! Then more, and a discussion of the obvious omissions.'
layout: podcast_post
permalink: /decipherscifi/top-5-science-fiction-tv-shows-movie-podcast-episode-18
redirect_from:
  - /decipherscifi/18
slug: top-5-science-fiction-tv-shows-movie-podcast-episode-18
title: 'Our Favorite SciFi TV: dystopias, sentient machines, Star Trek, etc'
tags:
  - science fiction
  - science
  - cyborg
  - robot
  - artificial intelligence
  - life
  - future
  - star trek
  - black mirror
  - universe

# Episode particulars
image:
  path: assets/imgs/media/misc/top_5_scifi_tv_shows_header.png
  alt: 'Top 5 SciFi TV shows collage'
links:
  - text: Science Fiction Film Podcast - Top 5 Sci-Fi TV Shows
    url: 'http://www.libertystreetgeek.net/bonus-series-top-5-science-fiction-tv-shows/'
  - text: Black Mirror
    urls:
      - text: Netflix
        url: 'https://www.netflix.com/title/70264888'
  - text: Babylon 5
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/babylon-5-season-1/id168268569?at=1001l7hP&mt=4'
      - text: Amazon
        url: 'http://www.amazon.com/Babylon-5-Complete-Seasons-1-5/dp/B002DUJ9Q6'
  - text: Battlestar Galactica
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/bsg-the-complete-series-vol.-1/id583461291?at=1001l7hP&mt=4'
      - text: Amazon
        url: 'http://www.amazon.com/Battlestar-Galactica-Edward-James-Olmos/dp/B0036EH3U2'
  - text: Humans
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/humans/id1006961139?at=1001l7hP&mt=4'
      - text: Amazon
        url: 'http://www.amazon.com/Episode-1/dp/B010O43LVQ'
  - text: Futurama
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/futurama-season-1/id274549734?at=1001l7hP&mt=4'
      - text: Amazon
        url: 'http://www.amazon.com/Futurama-Complete-Billy-West/dp/B00F77MAC2'
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 18
  guid: 'http://www.decipherscifi.com/?p=422'
  media:
    audio:
      audio/mpeg:
        content_length: 22735176
        duration: '31:34'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Top_5_Scifi_Shows_decipherSciFi.mp3'
---
We realize that there are some very obvious omissions from our lists. In the episode we address these, so check out the episode to see how we treat your favorite thing!

We welcome you to contact us to tell us how wrong we are. Please! Especially if we left out your very obvious #1. We'd like to hear from you.
