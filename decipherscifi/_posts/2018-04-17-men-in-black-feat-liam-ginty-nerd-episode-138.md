---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-04-17 07:45:45+00:00
description: 'Men in Black science - aliens and the effects of Earth stimulants. Thanking coffee for the enlightenment. The "real" Men in Black. Human evolution and the race between cranial and hip sizes. The evolution of assisted human childbirth. Memory wiping and neuralizers as the "roofie stick." Earth as a backwater. More!'
layout: podcast_post
permalink: /decipherscifi/men-in-black-feat-liam-ginty-nerd-episode-138
redirect_from:
  - /decipherscifi/138
slug: men-in-black-feat-liam-ginty-nerd-episode-138
title: 'Men in Black: human gestation, stimulants, and Earth as backwater  w/ Liam Ginty'
tags:
  - aliens
  - earth
  - evolution
  - fermi paradox
  - intelligence
  - science
  - science fiction
  - stimulants

# Episode particulars
image:
  path: assets/imgs/media/movies/men_in_black_1997.jpg
  alt: 
links:
  - text: A History of the World in Six Glasses by Tom Standage
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/a-history-of-the-world-in-6-glasses/id537167025?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/History-World-6-Glasses-ebook/dp/B002STNBRK'
  - text: Mirror - A single-page PnP micro RPG for you and your friends by Liam Ginty
    urls:
      - text: DrivethruRPG
        url: 'http://www.drivethrurpg.com/product/238405/Mirror?src=hottest_filtered'
      - text: Itch.io
        url: 'https://sandypuggames.itch.io/mirror-a-micro-rpg'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/men-in-black/id270782729?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Men-Black-Tommy-Lee-Jones/dp/B0011E7JBW'
  title: Men in Black
  type: movie
  year: 1997
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: liam_ginty
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/men_in_black_1997.jpg
  number: 138
  guid: 'https://decipherscifi.com/?p=6015'
  media:
    audio:
      audio/mpeg:
        content_length: 36359320
        duration: '43:16'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Men_in_Black_--_Liam_Ginty_--_Decipher_SciFi.mp3'
---
#### Will Smith

He's the best!

#### Alien life

And the forms that it takes. Arthropoids. Cephalapoids. The names we give the aliens and the names they give themselves.

#### Aliens and stimulants

Coffee!

#### Humanity's beverage history

And in particular how wonderfully wonderful caffeine is. Dwarf Fortress and ale supplies. Depressants vs stimulants and the shape society.

#### The "real" Men in Black

Frustration with being made a joke.

#### Human gestation and intelligence

Evolution, hip size, and cranial size. Bipedalism. Early homo adaptations and social assisted delivery.

#### Earth as universal backwater

Alien advocates for the ethical treatment of dumb humanity.

#### Memory wiping

Possible unreliable narrators. Neuralizers vs "memory sticks." This is my roofie stick. Short term and long-term memory "wiping."
