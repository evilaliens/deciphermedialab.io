---
# General post frontmatter
categories:
  - episode
  - answerers
  - qa
date: 2018-01-09 07:45:56+00:00
description: 'A collection of scifi and science questions and answers with some of our friends. Black holes, math, gasoline spoilage in the zombie apocalypse. Universal translators with Nick Farmer of The Expanse. Fraser Cain talks X-ray vision. More!'
layout: podcast_post
permalink: /decipherscifi/nothing-but-gifs-qa-1-episode-124
redirect_from:
  - /decipherscifi/140
slug: nothing-but-gifs-qa-1-episode-124
title: 'Nothing but GIFs, all day - Q&A #2 - Episode 124'
tags:
  - black holes
  - math
  - physics
  - science
  - science fiction
  - x-ray vision
  - zombie apocalypse

# Episode particulars
image:
  path: assets/imgs/media/misc/qa_02.jpg
  alt: "Q&A topic word cloud"
links:
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: fraser_cain
    roles:
      - answerer
  - data_name: adrian_falcone
    roles:
      - answerer
  - data_name: daniel_barker
    roles:
      - answerer
  - data_name: james_peterson
    roles:
      - answerer
  - data_name: nick_farmer
    roles:
      - answerer
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/qa_02.jpg
  number: 124
  guid: 'https://decipherscifi.com/?p=4936'
  media:
    audio:
      audio/mpeg:
        content_length: 28100608
        duration: '33:26'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/QA2_--_--_decipherSciFi.mp3'
---
Our second Q&A episode, wherein we ask our friends to help us answer the the _MOST INTERESTING QUESTIONS FROM THE INTERNET_!

#### Question: "Could you have x-ray vision?"

Answerer: [Fraser Cain]({% link _people/fraser_cain.md %})

X-ray vision. Cybernetic eye replacements. Wavelengths of light. The visible spectrum. Ghost in the Shell. Riddick. Light amplification. Gamma rays. Cancer avoidance.

#### Question: "[What if numbers are wrong?](https://www.quora.com/What-if-numbers-are-wrong)"

Answerer: [Adrian Falcone]({% link _people/adrian_falcone.md %})

Math. Numbers. Logic. Ted Chiang's short story "Divide by Zero" was in the collection [Stories of Your Life and Others](https://www.amazon.com/Stories-Your-Life-Others-Chiang-ebook/dp/B0048EKOP0/ref=sr_1_1?ie=UTF8&qid=1515473788&sr=8-1&keywords=ted+chiang). Adrian mentioned [Faith Physics](https://www.amazon.com/Faith-Physics-Introduction-Science-Afterlife-ebook/dp/B01N0PS87Q/ref=sr_1_2?ie=UTF8&qid=1515473709&sr=8-2&keywords=faith+physics).

#### Question: "[What if black holes didn't exist?](https://www.quora.com/What-if-black-holes-didnt-exist)"

Answerer: [Daniel James Barker]({% link _people/daniel_barker.md %})

Physics! Black holes. Neutron stars. Gravity. Collapse.

#### Question: How long before all the gasoline spoils in the zombie apocalypse?

Answerer: [James Peterson]({% link _people/james_peterson.md %})

The eternally-unanswered concern that zombie shows ignore always. How long after the zombie apocalypse before all the gas is spoiled? Chemical engineers vs chemists. Oxidation. Inert gas vapor displacement.

#### Question: Could there be a Star Trek-style universal translator?

Answerer: [Nick Farmer]({% link _people/nick_farmer.md %})

Please also see our [Arrival episode with Nick]({% link decipherscifi/_posts/2016-11-29-arrival-part-2-feat-nick-farmer-linguist-episode-66.md %}) for a _ton_ more on _alien_ languages. Machine learning. Man vs machine. Man vs alien languages. Universality of language. GIF communication.
