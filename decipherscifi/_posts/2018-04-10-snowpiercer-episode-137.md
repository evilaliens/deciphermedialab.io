---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-04-10 07:45:52+00:00
description: 'Snowpiercer science! Climate control, snowball earth. radioactive laser-shark hurricanes. Eating insects, freezing people-pieces in liquid nitrogen, polar bears, The Simpsons problem-solving methodologies. More!'
layout: podcast_post
permalink: /decipherscifi/snowpiercer-episode-137
redirect_from:
  - /decipherscifi/137
slug: snowpiercer-episode-137
title: 'Snowpiercer: entomophagy, what caused the hole in the ozone layer, and extreme cold survival'
tags:
  - climate
  - climate change
  - cold
  - entomophagy
  - life explosions
  - science
  - science fiction
  - weather

# Episode particulars
image:
  path: assets/imgs/media/movies/snowpiercer_2013.jpg
  alt: Snowpiercer cast movie backdrop
links:
  - text: Overpopulation – The Human Explosion Explained by Kurzgesagt
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=QsBT5EQt348'
  - text: 'Edible: An Adventure into the World of Eating Insects and the Last Great Hope to Save the Planet by Daniella Martin'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/audiobook/edible-adventure-into-world-eating-insects-last-great/id814547557?mt=3&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Edible-Adventure-Eating-Insects-Planet/dp/B00I8VT6WG'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/snowpiercer/id891829986?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Snowpiercer-Chris-Evans/dp/B00LFF3MKO'
  title: Snowpiercer
  type: movie
  year: 2013
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/snowpiercer_2013.jpg
  number: 137
  guid: 'https://decipherscifi.com/?p=6001'
  media:
    audio:
      audio/mpeg:
        content_length: 35362357
        duration: '42:05'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Snowpiercer_--_--_Decipher_SciFi.mp3'
---
#### Climate control

CW7 climate change-fixer compound. "Artificial cooling." Climate manipulation. The end of the world. "[Snowball earth](https://en.wikipedia.org/wiki/Snowball_Earth)" capped by massive volcanic activity and leading into life explosions. Weather manipulation vs climate manipulation. Radioactive hurricanes and sharks with frickin lasers. Micro-life survival.

#### The ozone layer

What is that for. Chlorofleorocarbons (CFCs) and ozone depletion.

{% youtube "https://www.youtube.com/watch?v=P9yruQM1ggc" %}

#### Cold

The frozen arm punishment. Cooling rates and atmospheric density. Christopher's experience (or lack thereof) freezing limbs with liquid nitrogen.

#### Food in the cryopocalypse (entomophagy)

Efficiency in energy conversion in food sources. Disgust reactions in the face of starvation. [Feed conversion ratio](https://en.wikipedia.org/wiki/Feed_conversion_ratio). Indiginous food sources and feeding a growing human population. Competing with dung beetles. Christopher's adventures in preparing wax worms. [Maggot cheese](https://en.wikipedia.org/wiki/Casu_marzu) and knowing your limits.
