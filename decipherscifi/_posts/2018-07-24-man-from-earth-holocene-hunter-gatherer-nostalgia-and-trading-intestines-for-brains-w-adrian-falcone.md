---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-07-24 07:45:11+00:00
description: 'Love for the original film. Nostalgia at 14,000 years old. Bronze iPads. "Free time" in hunter-gatherer societies. Cro-Magnon evolution. And Jesus!'
layout: podcast_post
permalink: /decipherscifi/man-from-earth-holocene-hunter-gatherer-nostalgia-and-trading-intestines-for-brains-w-adrian-falcone
redirect_from:
  - /decipherscifi/152
slug: man-from-earth-holocene-hunter-gatherer-nostalgia-and-trading-intestines-for-brains-w-adrian-falcone
title: 'Man From Earth Holocene: hunter gatherer nostalgia and trading intestines'
tags:
  - aging
  - christianity
  - cro-magnon
  - nostalgia
  - religion
  - science
  - science fiction
  - technology

# Episode particulars
image:
  path: assets/imgs/media/movies/the_man_from_earth_holocene_2017.jpg
  alt: Old John Oldman sitting in the sunset
links:
  - text: Primitive Technology
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/channel/UCAL3JXZSzSm8AlZyD3nQdBA'
  - text: 'The Man From Earth: biological immortality, and what is a micromort w/ Adrian Falcone'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/41'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/man-from-earth-holocene/id1327320568?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Man-Earth-David-Lee-Smith/dp/B077XP3SX9'
  title: The Man From Earth Holocene
  type: movie
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: adrian_falcone
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_man_from_earth_holocene_2017.jpg
  number: 152
  guid: 'https://decipherscifi.com/?p=7173'
  media:
    audio:
      audio/mpeg:
        content_length: 29134258
        duration: '34:40'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Man_From_Earth_Holocene_--_Adrian_Falcone_--_Decipher_SciFi.mp3'
---
#### The Man From Earth

[Our love for the original]({% link decipherscifi/_posts/2016-06-07-man-from-earth-adrian-falcone-movie-podcast-episode-41.md %}). Considering the sequel. People talking in a room.

#### Nostalgia

The coming-on of the bronze age. [Grocking the rate of progress w/ Dominick Perry]({% link decipherscifi/_posts/2018-04-03-stargate-feat-dominic-perry-of-the-history-of-egypt-podcast-episode-136.md %}). "Free time" in hunter-gatherer societies. Nostalgia as a way to bridge from what was old and comfortable to what is new and scary.

#### Cro-magnon

Anatomically modern humans vs socially modern humans. The evolutionary advantage of controlled fire and beginning to cook food. Getting away from all-day mastication and looooooooong intestines in favor of the big brain on Brad.

#### Primitive Technology

Getting boy without modern technology. The earliest evidence for humans cooking with fire. Differentiating human-controlled fire from millennia ago from other sources (like iguanas, or wildfires).

#### Growing old

Life expectancies over time. Appreciation for antibiotics. Life expectancy vs life_span_. Evolving into a centauranarian.

#### Jesus

Less-than-generous portrayals of religious characters. Third-act surprises. Jesus vs the antichrist.

#### Resolutions

Resolutions?
