---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-04-30 07:45:22+00:00
description: 'A review of the MCU up to now. Appreciating what has been accomplished here. Concern about the Disney media monopoly. Spoiler-laden science discussion. Etc.'
layout: podcast_post
permalink: /decipherscifi/avengers-endgame-appreciating-the-enormity-of-the-mcu
redirect_from:
  - /decipherscifi/191
slug: avengers-endgame-appreciating-the-enormity-of-the-mcu
title: 'Avengers Endgame: Appreciating the enormity of the MCU'
tags:
  - avengers
  - disney
  - marvel
  - nostalgia
  - science
  - science fiction
  - time travel

# Episode particulars
image:
  path: assets/imgs/media/movies/avengers_endgame_2019.jpg
  alt: 'A big pile of superheroes with mismatched lighting and lots of purple'
links:
  - text: 'Avengers: Endgame timeline diagram'
    urls:
      - text: imgur
        url: 'https://imgur.com/4L6tN5u'
  - text: 'Marvel movie budgets and box office revenue'
    urls:
      - text: Reddit
        url: 'https://www.reddit.com/r/dataisbeautiful/comments/8g11da/marvel_movie_budgets_and_box_office_revenue_oc/'
  - text: '"The Limitations of the Marvel Cinematic Universe" by Patrick Willems Part 1'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=mzIJ4HzLiIE'
  - text: '"The Limitations of the Marvel Cinematic Universe" by Patrick Willems Part 2'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=XNqeHJCCKbo'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/avengers-endgame/id1459467957?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Marvel-Studios-Avengers-Robert-Downey/dp/B07R21NC3J'
    - text: YouTube
      url: 'https://www.youtube.com/watch?v=ePpJDKfRAyM'
  title: 'Avengers: Endgame'
  type: movie
  year: 2019
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/avengers_endgame_2019.jpg
  number: 191
  guid: 'https://decipherscifi.com/?p=10392'
  media:
    audio:
      audio/mpeg:
        content_length: 32501417
        duration: '38:40'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/avengers_endgame_--_--_decipher_scifi.mp3'

---
#### A review

Spoiler alert: we liked this movie

#### Phase 1

Appreciating the establishment of the characters of Iron Man and Captain America. _That grenade scene_. Joss Whedon writing and directing teams of characters learning to work together. Realizing the first movie in the MCU came out in 2008. Realizing just how long ago 2008 was.

#### Phase 2

James Gunn appreciation (ps [Super](https://www.imdb.com/title/tt1512235/?ref_=fn_al_tt_1) was really really good!). _Guardians_ introducing a new level of comedy into the MCU.

Phase 3

_Guardians_ dad feels. Our favorite MCU movies. Appreciating _Spiderverse_ even though it doesn't count.

#### Money and The Mouse

The [huge money-making power](https://www.reddit.com/r/dataisbeautiful/comments/8g11da/marvel_movie_budgets_and_box_office_revenue_oc/) of this franchise. A review of budgets and revenue in the MCU. Feeling conflicted about enjoying these otherwise-impossible projects while Disney slowly buys up the entirety of western media.

#### Time travel quantum gobbledlygook

Time travel. Single-timeline time travel vs branching-timelines. The classic scientific "Loki-Hypercube paradox." [Timeline diagram](https://imgur.com/4L6tN5u) for Avengers: Endgame. Avoiding the [Novikov self-consistency principle](https://en.wikipedia.org/wiki/Novikov_self-consistency_principle) for paradox avoidance. The "Deutsch proposition." Robbing Peter in one timeline to pay Paul in another. Playing the time travel genie to wish for more wishes.

#### Characters

Character growth. Going over Cap, Iron Man, Hulk, Thor, etc. Observing how these characters have grown over time.

#### Past Coverage

* [Guardians of the Galaxy: Groot language, intelligent animals, and where do trees get their mass?]({% link decipherscifi/_posts/2017-05-09-guardians-of-the-galaxy-episode-89.md %})
* [Guardians of the Galaxy Vol 2: space jumps, empathy telepathy, and ❤️  Yondu]({% link decipherscifi/_posts/2017-09-19-guardians-of-the-galaxy-vol-2-episode-108.md %})
* [Avengers Infinity War: space mapping, Dyson spheres, and the purple coin purse]({% link decipherscifi/_posts/2018-05-15-avengers-infinity-war-episode-142.md %})
