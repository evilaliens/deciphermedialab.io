---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2020-01-28 07:45:21+00:00
description: 'Turtles, tortoises, and terrapins. Irl toxic ooze of the 80s: superfund sites. Pizza in the US. Nuclear waste disposal. Safeguarding future generations.'
layout: podcast_post
permalink: /decipherscifi/the-secret-of-the-ooze-indie-success-pizza-and-irl-toxic-sludge
redirect_from:
  - /decipherscifi/226
slug: the-secret-of-the-ooze-indie-success-pizza-and-irl-toxic-sludge
title: 'The Secret of the Ooze: turtles, pizza, and irl toxic sludge'
tags:
  - biology
  - food
  - mutation
  - nostalgia
  - nuclear power
  - nuclear waste
  - pizza
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/teenage_mutant_ninja_turtles_2_1991.jpg
  alt: 'Ninja turtles standing worriedly around a broken vial of toxic ooze'
links:
  - text: 'TMNT decayed costume nightmare fuel'
    urls:
      - text: Twitter
        url: 'https://twitter.com/brainexploderrr/status/1178802877193691137'
  - text: 'Why danger symbols can’t last forever by Vox & 99pi'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=lOEqzt36JEM'
  - text: '10 Thousand Years'
    urls:
      - text: '99% Invisible'
        url: 'https://99percentinvisible.org/episode/ten-thousand-years/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/teenage-mutant-ninja-turtles-ii-the-secret-of-the-ooze/id295431978?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Teenage-Mutant-Ninja-Turtles-2/dp/B001QC6QRC'
  title: 'Teenage Mutant Ninja Turtles II: The Secret of the Ooze'
  type: movie
  year: 1991
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/teenage_mutant_ninja_turtles_2_1991.jpg
  number: 226
  guid: 'https://decipherscifi.com/?p=10913'
  media:
    audio:
      audio/mpeg:
        content_length: 49037605
        duration: '58:16'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/tmnt_2_the_secret_of_the_ooze.output.mp3'

---
#### Teenage Mutant Ninja Turtles

An incredible indie success story.

#### Pizza

Pizza popularity in the US post-WWII. Pizza classification arguments. Bread sauce cheese vs bread cheese sauce and the DRASTIC difference between the two. NYC Pizza ftw.

#### Wtf is a turtle

Turtles, tortoises, terrapins, and the difficulty with colloquial names. Putting them in the water to see which ones drows. Telescoping vs hinging necks.

#### Toxic ooze

Realizing the horrifying reason “toxic sludge” was in all of our childhood media. Joe’s story about that one superfund site that I can’t spell. Creating better ooze monsters. Honey badger? Mantis shrimp?

#### Nuclear waste

Short term nuclear waste storage and the problem with active maintenance. Long-term nuclear waste storage and keeping things safe after humanity. [Long-term nuclear waste warning messages](https://en.wikipedia.org/wiki/Long-time_nuclear_waste_warning_messages). The limits of language and pictograms. Designing for “bad vibes.”

> This place is a message… and part of a system of messages …pay attention to it! Sending this message was important to us. We considered ourselves to be a powerful culture. This place is not a place of honor … no highly esteemed deed is commemorated here… nothing valued is here. What is here was dangerous and repulsive to us. This message is a warning about danger. The danger is in a particular location… it increases towards a center… the center of danger is here… of a particular size and shape, and below us. The danger is still present, in your time, as it was in ours. The danger is to the body, and it can kill. The form of the danger is an emanation of energy. The danger is unleashed only if you substantially disturb this place physically. This place is best shunned and left uninhabited.
