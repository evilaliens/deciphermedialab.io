---
# General post frontmatter
categories:
  - episode
  - bonus
date: 2019-12-24 02:35:04+00:00
layout: podcast_post
permalink: /decipherscifi/no-episode-this-week-happy-holidays
redirect_from:
slug: no-episode-this-week-happy-holidays
title: No episode this week - happy holidays!
tags:

# Episode particulars
image:
  path: assets/imgs/selfies/cartoon-christmas.png
links:
media:
people:
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/selfies/cartoon-christmas.png
  guid: 'https://decipherscifi.com/?p=10892'
  media:
    audio:
      audio/mpeg:
        content_length: 392068
        duration: '00:21'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/221b_no_episode_happy_holidays.final.mp3'

---
In this Christmas story, Christopher gets very ill and then we are forced to push back our new Star Wars episode.

Sorry! Happy holidays!
