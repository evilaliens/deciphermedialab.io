---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - tv
date: 2019-11-26 07:45:42+00:00
description: 'Star Wars space western. Corporate oversight of our shared mythology. Pooping in space. Safely freezing living things. Ancient rhinos! Droids with DRM :('
layout: podcast_post
permalink: /decipherscifi/the-mandalorian-modern-mythology-space-toilets-and-hairy-rhinos
redirect_from:
  - /decipherscifi/218
slug: the-mandalorian-modern-mythology-space-toilets-and-hairy-rhinos
title: 'The Mandalorian: modern mythology, space toilets, and hairy rhinos'
tags:
  - art
  - biology
  - eggs
  - evolution
  - filmmaking
  - poop
  - robots
  - science
  - science fiction
  - star wars

# Episode particulars
image:
  path: assets/imgs/media/tv/the_mandalorian_2019.jpg
  alt: 'The Mandalorian walking with purpose on a desert planet'
links:
  - text: 'The Last Jedi: FTL ramming, salt geology, and real green milk'
    urls:
      - text: 'Decipher SciFi'
        url: 'https://deciphermedia.tv/decipherscifi/122'
  - text: 'Star Wars: the death star, planet science, and appreciating the original Star Wars trilogy'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/24'
  - text: 'Star Wars Meta: How to Star Wars, machete order, and the best Star Wars fan edit'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/23'
media:
  links:
    - text: 'Disney+'
      url: 'https://disneyplusoriginals.disney.com/show/the-mandalorian'
  title: The Mandalorian
  type: tv
  year: 2019
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/the_mandalorian_2019.jpg
  number: 218
  guid: 'https://decipherscifi.com/?p=10865'
  media:
    audio:
      audio/mpeg:
        content_length: 36819572
        duration: '43:43'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/the_mandalorian.final.mp3'

---
#### Western

The best genre of Star Wars media? Brandishing yourself. Old west forced sack removal.

#### “Real filmmaking”

Werner Herzog’s surprising(?) praise. “Real filmmaking” in the wake of the prequel trilogies. Wooden green-screen acting vs sweet puppets and actual human beings. Unreal Engine LED walls and volumetric projection and camera tracking.

#### Our new mythology

Star Wars as our new shared cultural mythology the world over. Concern about disney’s stewardship and the private ownership of our cultural heritage. Streamboat Willie entering The Commons.

#### Mandalorians

Some past famous Mandalorians, this particular Mandalorian, and martial prowess. Mandalorian history and why the Mandalorians are the only ones who figured out how to fight the space wizards for some reason?

#### Poop!

Pooping space. Weightless waste elimination can be tricky. Floating turds.

#### Freezing in carbonite

Cryostasis. Cryopreservation. Avoiding ice crystals. Replacing blood with “antifreeze”

#### Robo slavery

Taking the most depressing angle on droid consciousness and freedom (or lack thereof). And then to make it worse: [DRM](https://en.wikipedia.org/wiki/Digital_rights_management)!

#### Baby Yoda

Extra-long postnatal development. Big brains and incomplete gestation. Cuteness. Animatronics.

#### Hairy rhinos!

Elasmotherium. Whooly rhinos. Hiary…. eggs? Historical rhinos. Rhino evolution. [Balut](https://en.wikipedia.org/wiki/Balut_(food)).

{% responsive_image_block %}
  path: assets/imgs/misc/elasmotherium_model.jpg
  alt: Elasmotherium prehistoric rhino
  caption: Elasmotherium prehistoric rhino
  attr_text: Boris Dimitrov CC-BY-SA-3.0 
  attr_url: https://sco.wikipedia.org/wiki/File:Elasmotherium_model.jpg
{% endresponsive_image_block %}

{% responsive_image_block %}
  path: assets/imgs/misc/rhino_sizes.jpg
  alt: Rhino sizes evolution chart
  caption: Rhino sizes evolution chart
  attr_text: DagdaMor CC-BY-3.0 
  attr_url: https://de.wikipedia.org/wiki/Datei:Rhino_sizes.png
{% endresponsive_image_block %}
