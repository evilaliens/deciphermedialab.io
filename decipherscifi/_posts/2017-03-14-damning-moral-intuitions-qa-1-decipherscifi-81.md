---
# General post frontmatter
categories:
  - episode
  - qa
date: 2017-03-14 07:45:16+00:00
description: "Railing against anthropocentrism. Orbital battle. Would you like yourself? Arrival's aliens. Sci-Fi pets. Moon formation. Avoiding planetary elimination."
layout: podcast_post
permalink: /decipherscifi/damning-moral-intuitions-qa-1-decipherscifi-81
redirect_from:
  - /decipherscifi/81
slug: damning-moral-intuitions-qa-1-decipherscifi-81
title: 'Q&A: damning moral intuitions'
tags:
  - science fiction
  - science
  - immortality
  - aliens
  - black holes
  - earth
  - apocalypse
  - language

# Episode particulars
image:
  path: assets/imgs/media/misc/qa_01.jpg
  alt: 'Q&A #1 word cloud'
links:
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/qa_01.jpg
  number: 81
  guid: 'http://www.decipherscifi.com/?p=1413'
  media:
    audio:
      audio/mpeg:
        content_length: 35135488
        duration: '41:48'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/QA01_--_--_decipherSciFi.mp3'
---

# Q&A #1

**What:** Our first Q&A episode! Wherein we answer the _MOST INTERESTING QUESTIONS FROM THE INTERNET_!

#### "What would you do for immortality?" ([Question](https://twitter.com/Starstraeder/status/715862551758655488) from [CF Villion](https://twitter.com/Starstraeder) on Twitter)

Trolley problems. Types of immortality, especially "virtual" immortality. Consciousness uploading.

#### "#SciFi #Space: what would happen if a #BlackHole showed up in our #SolarSystem?" ([Question](https://twitter.com/maximaxoo/status/830769935496466436) from [Maxime Duprez](https://twitter.com/maximaxoo) on Twitter)

Rogue black holes. Passing through the Kuiper belt. Or passing through the asteroid belt. The myriad ways in which this would totally mess up our solar system and probably kill us all.

{% youtube "https://www.youtube.com/watch?v=v3hd3AI2CAA" %}

#### "In SciFi movies we are Earthlings but what about galaxy VS galaxy stuff. What would we be called? Cuz I'm not down with being called Milkies" ([Question](https://twitter.com/MBCaceT/status/818350723914043394) from [Michael](https://twitter.com/MBCaceT) on Twitter)

Earth-centric point-of-view. Milkies? Maybe the aliens can call us "[milksopp](https://en.wiktionary.org/wiki/milksop)." Don't forget there could be many many other intelligent species amongst the hundreds of billions of stars in our galaxy, though.

#### "What would an orbital battle be like?" ([Question](https://twitter.com/MadScientistJo/status/803935425266126848) from [John Freeman](https://twitter.com/MadScientistJo) on Twitter)

_Messy_. Defining what we mean by orbital. Flotsam and jetsam. Yet more references to [Seveneves]({% link decipherscifi/_posts/2016-08-09-seveneves-feat-daniel-mann-software-pro-episode-50.md %}). Kinetic weaponry and orbital bombardment.

{% responsive_image path: assets/imgs/misc/zap_brannigan_butt.jpg alt: "Zap Brannigan butt meme" caption: "Zap Brannigan style" %}

#### "What would you do if you met yourself? Would be disappointed or in awe?" ([Question](https://twitter.com/ntemplewriter/status/801899764321239040) by [Nick Temple](https://twitter.com/ntemplewriter) on Twitter)

Not liking oneself from the outside. Antisocial magnets of the same polarity.

#### "If you could have any creature as a pet from SciFi, what would it be?" ([Question](https://twitter.com/TheHuskyViking/status/824380649574330369) by [Cameron](https://twitter.com/TheHuskyViking) on Twitter)

Ethical implications with trying to take in a creature of unknown intelligence as a pet. Bad pets: Dune sandworms,

#### "Why Hasn't Some Disaster Destroyed The Earth?" ([Question](https://twitter.com/MetaphysicalPI/status/830424635426557953) by [MysticInvestigations](https://twitter.com/MetaphysicalPI) on Twitter)

Impactors. Protections offered by our moon, Jupiter, and other bodies in our solar system. The formation of our moon, with accretion disks and whatnot.

#### "Which Newspeak word do you like most? Why?" ([Question](https://twitter.com/DavidRozansky/status/830152116853407746) by [David Rozansky](https://twitter.com/DavidRozansky))

Doubleplus good.

#### "Watching Arrival. Why do artsy scifi movies have abstract expressionist art aliens? I mean sure, avoid lil green men, but featureless?" ([Question](https://twitter.com/vgr/status/833137983519805440) by [vgr](https://twitter.com/vgr) on Twitter)

We worked a lot out about the aliens from arrival in our [two]({% link decipherscifi/_posts/2016-11-15-arrival-feat-jolene-adrian-episode-64.md %}) [episodes]({% link decipherscifi/_posts/2016-11-29-arrival-part-2-feat-nick-farmer-linguist-episode-66.md %}). Alien morphology. More anthropocentrism. Invisible (to us) differentiating markings. Here's a whole blog dedicated to seeing through the eyes of other creatures in their own areas of the spectrum: [Photography of the Invisible World](http://photographyoftheinvisibleworld.blogspot.com/).
