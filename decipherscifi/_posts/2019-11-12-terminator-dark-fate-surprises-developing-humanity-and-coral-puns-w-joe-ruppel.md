---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-11-12 07:45:40+00:00
description: 'The best Terminator since T2! Upgrading humans. Developing *humanity*. James Cameron Leeroy Jenkins. Looking back and appreciating our journey w/ the movies'
layout: podcast_post
permalink: /decipherscifi/terminator-dark-fate-surprises-developing-humanity-and-coral-puns-w-joe-ruppel
redirect_from:
  - /decipherscifi/216
slug: terminator-dark-fate-surprises-developing-humanity-and-coral-puns-w-joe-ruppel
title: 'Terminator Dark Fate: surprises, developing humanity, and CORAL puns w/ Joe Ruppel'
tags:
  - cyborgs
  - future
  - humanity
  - man versus machine
  - robots
  - schwarzenegger
  - science
  - science fiction
  - skynet
  - terminator
  - time travel

# Episode particulars
image:
  path: assets/imgs/media/movies/terminator_dark_fate_2019.jpg
  alt: 'The old Terminator and the new sorta-Terminator and the old Terminator-fighting lady and a new girl'
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/terminator-dark-fate/id1483823959?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Terminator-Dark-Fate-Linda-Hamilton/dp/B07ZP8J83T'
  title: 'Terminator: Dark Fate'
  type: movie
  year: 2019
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/terminator_dark_fate_2019.jpg
  number: 216
  guid: 'https://decipherscifi.com/?p=10851'
  media:
    audio:
      audio/mpeg:
        content_length: 54674394
        duration: '01:04:59'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/terminator_dark_fate.final.mp3'

---
Hey so we covered the entirety of the Terminator series in the past few months and it was really fun!
 	
* [The Terminator: bootstrapping, machine uprising, and time traveling meatballs w/ Joe Ruppel]({% link decipherscifi/_posts/2019-08-06-the-terminator-bootstrapping-machine-uprising-and-time-traveling-meatballs.md %})
* [Terminator 2: advanced puppetry, Skynet, and liquid nitrogen handling w/ Joe Ruppel]({% link decipherscifi/_posts/2019-08-20-terminator-2-advanced-puppetry-skynet-and-liquid-nitrogen-dangers.md %})
* [Terminator 3 Rise of the Machines: hacking cars and supercomputing Skynet FLOPS w/ Joe Ruppel]({% link decipherscifi/_posts/2019-09-10-terminator-3-rise-of-the-machines-hacking-cars-and-supercomputing-skynet-flops-w-joe-ruppel.md %})
* [Terminator Salvation: body donation, realistic time travel, and the legal shmegal defense w/ Joe Ruppel]({% link decipherscifi/_posts/2019-09-24-terminator-salvation-body-donation-realistic-time-travel-and-the-legal-schmegal-defense-w-joe-ruppel.md %})
* [Terminator Genisys: surprise anthologies & time travel spaghetti w/ Joe Ruppel]({% link decipherscifi/_posts/2019-09-24-terminator-salvation-body-donation-realistic-time-travel-and-the-legal-schmegal-defense-w-joe-ruppel.md %})

# Meta

Hooray! Another legit good Terminator sequel! Nutso revenue requirements. Linda Hamilton and Arnold physique maintenance. Arnold appreciation as _always_.

{% youtube "https://www.youtube.com/watch?v=HAbRdbH5fmE" %}

# Opener

No spoilers but wow what a way to begin. CG face replacements. Terminator battle plans. More alternate timelines.

# Human upgrades

Grace. Imposing! "Design" considerations. Relying on human tissues. Comparing and contrasting with the Terminator Salvation's [Sam Worthingtonbot]({% link decipherscifi/_posts/2019-09-24-terminator-salvation-body-donation-realistic-time-travel-and-the-legal-schmegal-defense-w-joe-ruppel.md %}). Bio-aumentation. More than muscle! Nervous system response shortcuts.

# Robots taking our jobs

Robot literally taking the guy's job this time. Learning to work with the robots. Extended robot anxieties throughout the history of the Terminator franchise. Robots surveilling and monitoring our jobs and making it easy for Terminators to find us.

# Terminators, revisited

New Terminator! The Rev-9 taking the best bits of previous no-longer-canon sequels. Infiltrating and cracking wise.

# Legion

Synet rebranded. AI built on human data and maybe no matter which platform becomes sentient it will come to similar conclusions and methods. Cyberwarfare platforms.

# Humanity

Becoming human. Understanding humanity. What happens to a Terminator after their mission is complete? Post-mission Terminators getting creative.

