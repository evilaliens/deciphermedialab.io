---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-01-29 07:45:30+00:00
description: 'Reviewing something we love. Film framerates and hand-cranked camera technology. Persistence  of vision and framerate. Academic controversy over enhancement.'
layout: podcast_post
permalink: /decipherscifi/they-shall-not-grow-old-vfx-restoration-and-controversial-history-communication
redirect_from:
  - /decipherscifi/178
slug: they-shall-not-grow-old-vfx-restoration-and-controversial-history-communication
title: 'They Shall Not Grow Old: film restoration, persistance of vision, and vfx'
tags:
  - film
  - history
  - optical illusion
  - science
  - science fiction
  - vision
  - world war 1

# Episode particulars
image:
  path: assets/imgs/media/movies/they_shall_not_grow_old_2018.jpg
  alt: 'Dudes in WWI but in COLOR'
links:
  - text: '"YOU WONT BELIEVE YOUR EYES!" by Smarter Every Day'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=_FlV6pgwlrk'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/they-shall-not-grow-old/id1443053454?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/They-Shall-Not-Grow-Old/dp/B07PRZXV9C'
    - text: YouTube
      url: 'https://www.youtube.com/watch?v=LSf9eRjR-oA'
  title: They Shall Not Grow Old
  type: movie
  year: 2019
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/they_shall_not_grow_old_2018.jpg
  number: 178
  guid: 'https://decipherscifi.com/?p=9917'
  media:
    audio:
      audio/mpeg:
        content_length: 34290876
        duration: '40:48'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/They_Shall_Not_Grow_Old_--_--_Decipher_SciFi.mp3'

---

#### Review

Well, we don't normally do this sorta thing, but _oh man this was so good_! What the film is about, how it is about, and why it was so special. Appreciating those who do the best work of making history relatable (_cough_ _[Dan Carlin](https://www.dancarlin.com/) cough_).

#### Restoration

Film degradation. Scratching, exposure, shrinkage, physical tearing. The crapshoot that is video frame rates in early 20th century film footage. Crank cameras and gearing and compressed-air constant-framerate cameras. Correcting for all of this and the challenge of bringing it all up to a standard 24 fps.

#### Film is an illusion

The optical illusion that we perceive as movement on a screen. Persistence of vision. Video framerates and the "refresh rate" of the human eye. Motion interpolation as an offense against art and all things good. Tom Cruise [fighting the good fight](https://www.youtube.com/watch?v=UbCZpcy0eAk). Motion interpolation as a really neato technology which is actually really cool when not on by default on new televisions. Motion interpolation in VR or video games.

#### Academic controversy

Historian types and objections to the "enhancements" introduced by the filmmakers. Art vs education and the importance of relatability. Modernizing history. Colorization. 3d-ification. Finding it hard to complain about digital enhancement and remastering which leaves the original materials untouched.

{% youtube "https://www.youtube.com/watch?v=IrabKK9Bhds" %}
