---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-11-05 07:45:27+00:00
description: "Señor Eiffel's and his tower's science endeavors. Alien research. Tracking interstellar objects. Hacking Hubble. Blue giant energies. Fake memories. Etc."
layout: podcast_post
permalink: /decipherscifi/men-in-black-international-eiffel-backfilling-memory-space-hacking
redirect_from:
  - /decipherscifi/215
slug: men-in-black-international-eiffel-backfilling-memory-space-hacking
title: 'Men in Black International: Eiffel, backfilling memory, space hacking'
tags:
  - aliens
  - engineering
  - memory
  - science
  - science fiction
  - space
  - stars

# Episode particulars
image:
  path: assets/imgs/media/movies/men_in_black_international_2019.jpg
  alt: 'Thor and Valkyrie with the memory buttplug'
links:
  - text: 'Men in Black: human gestation, stimulants, and Earth as backwater w/ Liam Ginty'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/138'
  - text: 'Isaac Arthur'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/channel/UCZFipeZtQM5CKUjx6grh54g'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/mib-international/id1466004437?mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Men-Black-International-Chris-Hemsworth/dp/B07SZ4CTN2'
  title: 'Men in Black: International'
  type: movie
  year: 2019
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/men_in_black_international_2019.jpg
  number: 215
  guid: 'https://decipherscifi.com/?p=10840'
  media:
    audio:
      audio/mpeg:
        content_length: 32678486
        duration: '38:48'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/men_in_black_international.decipherscifi.mp3'

---
#### Eiffel

LPT: you can get pommes frites in Europe. Could Eiffel have been an agent? The history of the Eiffel tower. The Eiffel Tower's science history. Cosmic rays. Wireless telepraphy (radio). Outliving its intended lifespan.

{% youtube "https://www.youtube.com/watch?v=FBN3xfGrx_U" %}

#### Aliens

Which government agency should you be applying to in order to research alien stuff? Remembering the MiB is an NGO.

#### Space tracking

Increasing ability to track nearby space objects. Tracking objects in our solar system's plane. The difficulty of tracking interstellar objects. Being a good space neighbor. Confirmed tracked interstellar objects. Failing to pronounce "[ʻOumuamua](https://en.wikipedia.org/wiki/%CA%BBOumuamua)."

#### A Danzig tale

> justin: dude
> 
> Danzig lives next door to me in l.a.
> 
> down the street
> 
> and he is the worst neighbor ever
> 
> 
> 
> justin: so he has this huge pile of bricks in his front yard
> 
> and the house looks like an evil pixar house
> 
> so anyway, his neighbor was like, “dude, danzig your bringing property values down with these bricks in your yard.”
> 
> and danzig was pissed
> 
> so anyway, back and forth with his neighbor and danzig
> 
> and finally one day
> 
> i see danzig outside
> 
> in his front yard
> 
> and he’s hurling bricks into this drumpster and he’s screaming, “HERE I AM MOTHERFUCKER, JUST CLEANING UP MY FUCKING BRICKS BITCH!”
> 
> Just super loud
> 
> to no one in particular
> 
> for two hours
> 
> it was amazing
> 
> like, i couldn’t even think about other things
> 
> because it was so amazing
> 
> 
> 
> me: oh my god
> 
> this is amazing
> 
> 
> 
> justin: dude
> 
> it blew my mind
> 
> because it was danzig as just a really poor homeowner
> 

#### Space security

Hacking satellites! Actually totally possible and has happened. Air Force satellites at DEFCON. DDoS in space. The possibilities for catastrophy in hacking things in space. The many ways an attacker could damage or destroy our space telescopes.

#### Stars!

Blue giants! They're rather large, and luminous. And hot. Capturing star energy. [Kardashev scales](https://en.wikipedia.org/wiki/Kardashev_scale).

#### Memory

The tendency of our minds to backfill missing information. Using neuralizers _without_ providing a cover story is probably actually good enough, really.
