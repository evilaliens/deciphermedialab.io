---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-03-13 04:13:40+00:00
description: 'Johnny Mnemonic science, computer and otherwise. Fuzzy encryption, brute forcing, data couriers. Bird-based datagram RFCs, science fiction storage doubling, Ice-T is cyberpunk Amish, the swing back from silly computer interfaces, more!'
layout: podcast_post
permalink: /decipherscifi/johnny-mnemonic-episode-133
redirect_from:
  - /decipherscifi/133
slug: johnny-mnemonic-episode-133
title: 'Johnny Mnemonic: data couriers, brain storage capacity, and the cyberpunk Amish'
tags:
  - computing
  - cyberpunk
  - data storage
  - facial recognition
  - internet
  - luddites
  - science
  - science fiction
  - virtual reality
  - william gibson

# Episode particulars
image:
  path: assets/imgs/media/movies/johnny_mnemonic_1995.jpg
  alt: Keanu being all cyber and virtual pre-Matrix
links:
  - text: 'Science Fiction Film Podcast #247: Johnny Mnemonic featuring Christopher Peterson'
    urls:
      - text: LSG Media
        url: 'https://www.libertystreetgeek.net/johnny-mnemonic/'
  - text: 'Wing Commander: PC games nostalgia, pilgrims, and space combat w/ Dean from LSG Media'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/27'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/johnny-mnemonic/id276820754?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Johnny-Mnemonic-Keanu-Reeves/dp/B000I8ES4S'
  title: Johnny Mnemonic
  type: movie
  year: 1995
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/johnny_mnemonic_1995.jpg
  number: 133
  guid: 'https://decipherscifi.com/?p=5799'
  media:
    audio:
      audio/mpeg:
        content_length: 34209995
        duration: '40:42'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Johnny_Mnemonic_--_--_Decipher_SciFi.mp3'
---
#### William Gibson

Cyberpunk!

#### Data couriers

When to move the data physically by human. CYA. The _impression_ of security. Offline attacks. quantum supremacy. [RFC 1149](https://tools.ietf.org/html/rfc1149) (A Standard for the Transmission of IP Datagrams on Avian Carriers) and [RFC 2549](https://tools.ietf.org/html/rfc2549) (IP over Avian Carriers with Quality of Service).

#### Storage doubling

And exceeding limits. The lack of connection between physical brain material and storage of particular sorts of memory, _especially_ temporal sequences.

#### Data encryption and uploading

"[Light therapy](http://www.bbc.com/news/health-38220670)" and affecting brain states. [Fuzzy encryption keys](https://en.wikipedia.org/wiki/Fuzzy_extractor) for biometrics. Brute force attacks.

#### Lo-teks

The cyberpunk Amish. "Pants are technology!" Privacy-focused ludditism. Human brain face-detection. Limpy-Frankenstein to avoid person-tracking. [Ice T memes](https://news.avclub.com/ice-t-has-some-startling-information-for-you-in-these-f-1798283728).

> Snatch back your brain, zombie. And hold it!
>
> - J Bone

{% responsive_image path: assets/imgs/misc/dazzle_camoflage.jpg caption: "Naval &quot;dazzle&quot; camoflage" alt: "USS West Mahomet w/ &quot;dazzle&quot; camoflage" attr_text: "Naval History and Heritage Command CC-0" attr_url: "https://www.history.navy.mil/our-collections/photography/numerical-list-of-images/nara-series/19-n/19-N-00001/19-N-1733.html" %}

#### Non-graphical computing

VR silliness. [Command-line life](https://chrispeterson.info/category/blog/). Christopher tried a MUD/MOO. Abandoning cheeky abstractions. AI solving yet another problem. Morgan Freeman Espeak modules.
