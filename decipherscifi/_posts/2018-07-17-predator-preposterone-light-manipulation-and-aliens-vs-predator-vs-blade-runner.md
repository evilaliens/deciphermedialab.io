---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-07-17 07:45:14+00:00
description: 'Real life invisibility. Evolving the ability to see in infrared like Predator. What determines the color of blood? Macro mandibles. Self-destruction tech.'
layout: podcast_post
permalink: /decipherscifi/predator-preposterone-light-manipulation-and-aliens-vs-predator-vs-blade-runner
redirect_from:
  - /decipherscifi/151
slug: predator-preposterone-light-manipulation-and-aliens-vs-predator-vs-blade-runner
title: 'Predator : preposterone, light manipulation, and Aliens vs Predator vs Blade'
tags:
  - aliens
  - evolution
  - invisibility
  - man meat
  - science
  - science fiction
  - technology
  - vision

# Episode particulars
image:
  path: assets/imgs/media/movies/predator_1987.jpg
  alt: Arnold looking very manly with a grenade launcher
links:
  - text: If It Bleeds We Can Kill It - The Making of Predator
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=i_pBi0m-QfM'
  - text: If It Bleeds We Can Kill It - Predator the Musical by legolambs
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=qlicWUDf5MM'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/predator/id363331575?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Predator-Arnold-Schwarzenegger/dp/B003GX5CWY'
  title: Predator
  type: movie
  year: 1987
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/predator_1987.jpg
  number: 151
  guid: 'https://decipherscifi.com/?p=7134'
  media:
    audio:
      audio/mpeg:
        content_length: 37219160
        duration: '44:17'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Predator_--_--_Decipher_SciFi.mp3'
---
#### Testosterone

Preposterous amounts of testosterone. [_Preposterone_](https://youtu.be/t-3qncy5Qfk?t=66). Ahhnold and Carl Weathers. The muscles from Brussels in _spaaaaace_.

#### Invisibility

Picking a chunk of the electromagnetic spectrum to be invisible within.

* Spatial light manipulation
* Temporal light manipulation
* Spectral light manipulation

#### Predator vision

Seeing in infrared! Interactions between optical camouflage and optical receivers. Why doesn't all the life on Earth have heat vision? Heat-tracking evolution. Pit viper heat sensors. Human heat-tracking. Biology absorbing infrared. Seeing in greyscale. Wet clay camouflage and effectiveness over time. Mythbusting. Infrared-blocking materials.

#### Predator physiology

Aliens vs Predator for top monster. Ears...? Bleeding. The rainbow of different blood colors available on Earth. Green blood. Luminescent blood. Figuring out why only arthropods seems to have mandibles, and lamenting the lack of them in macro predators.

#### Shared Universe

Appreciating that Blade Runner is pretty much officially the same world as Predator now.

#### Self-destructing technology

Techno-scuttling. Destroying advanced technology to keep it out of enemy hands. Super-expanding polymers. Amazon delivery drone automatic "dispersal."
