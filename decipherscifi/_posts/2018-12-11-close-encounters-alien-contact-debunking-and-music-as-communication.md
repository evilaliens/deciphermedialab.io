---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-12-11 07:45:31+00:00
description: "Alien first contact, anal probes, and 'levels' of contact. The Devil's Tower geology. Translation errors. Debunking. SETI. Music as a mode of communication."
layout: podcast_post
permalink: /decipherscifi/close-encounters-alien-contact-debunking-and-music-as-communication
redirect_from:
  - /decipherscifi/171
slug: close-encounters-alien-contact-debunking-and-music-as-communication
title: 'Close Encounters: alien contact, debunking, and music communication'
tags:
  - aliens
  - first contact
  - geology
  - mathematics
  - music
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/close_encounters_1977.jpg
  alt: 'Classic UFO over an empty road - Close Encounters movie backdrop'
links:
  - text: 'Captain Disillusion'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/user/CaptainDisillusion'
  - text: 'Escherian Stairwell Deconstruction by Captain Disillusion'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=eLAwG7CjF_k'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/close-encounters-of-the-third-kind/id557265666?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Close-Encounters-Third-Kind-Directors/dp/B000PNCETC'
  title: Close Encounters of the Third Kind
  type: movie
  year: 1977
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/
  number: 171
  guid: 'https://decipherscifi.com/?p=8473'
  media:
    audio:
      audio/mpeg:
        content_length: 31547635
        duration: '37:32'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Close_Encounters_--_--_Decipher_SciFi.mp3'
---
#### Contact

"Levels" of alien contact. Converting anal probes and sex bases to contact "levels."

1. First kind: visual contact
2. Second kind: evidence of contact
3. Third kind: in-person contact

#### The Devil's Tower

How to pronounce _butte_. Convention vs personal preference. The awesome nature of structures like The Devil's Tower, jutting up all inaccessible as they are. Translation errors for fun and profit. Thinking back to [Schiaparelli's canals]({% link decipherscifi/_posts/2016-09-27-robinson-crusoe-on-mars-episode-57.md %}). Geological origins: igneous intrusions and volcanic butte plugs.

#### "Sunburn"

Ultraviolet effects on body tissues. What is a sunburn? Arc welding and ultraviolet exposure: "arc eye."

#### Debunking things

Shying away from debunking. Appreciating folks who cross boundaries and make deeply interesting informative generally-useful critical thinking-training material, like [Captain Disillusion](https://www.youtube.com/user/CaptainDisillusion). Credulity and our personal predispositions.

#### SETI

Appreciating a scientific, rigorous approach to finding intelligent life in the cosmos.

#### Communicating with aliens

Randomness in space signals. Encoded information, decipherable to latitude and longitude. Past sightings of alien information encoding on Decipher SciFi: Arrival ([part 1]({% link decipherscifi/_posts/2016-11-15-arrival-feat-jolene-adrian-episode-64.md %}), [part 2]({% link decipherscifi/_posts/2016-11-29-arrival-part-2-feat-nick-farmer-linguist-episode-66.md %})), [Contact]({% link decipherscifi/_posts/2016-01-26-contact-we-love-carl-sagan-episode-22.md %}). Math! alien tonal communication.

#### Music! The Tones!

Music as math. Considering the tiny slice of reality in which we operate. Considering the universality in physics of the mathematical relationships between vibrations. [Caelum Rale]({% link _people/caelum_rale.md %})'s mathematical musical perspective. Making sense of the noise of the universe and demonstrating intelligence across vast distances.
