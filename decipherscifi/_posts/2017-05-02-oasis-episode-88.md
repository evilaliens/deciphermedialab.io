---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - tv
date: 2017-05-02 10:03:36+00:00
description: 'A review of the Amazon original scifi pilot. Space travel, space settlement, space dementia, space spirituality, space botany, space! And magnets.'
layout: podcast_post
permalink: /decipherscifi/oasis-episode-88
redirect_from:
  - /decipherscifi/88
slug: oasis-episode-88
title: 'Oasis (pilot): ponics, the vastness of space, and breathable atmospheres'
tags:
  - aeroponics
  - hydroponics
  - magnets
  - religion
  - science
  - science fiction
  - space
  - space dementia
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/tv/oasis_2017.jpg
  alt: Rob Stark, but in the future, doing an eye test. Oasis tv pilot backdrop.
links:
media:
  links:
    - text: Amazon
      url: 'https://www.amazon.com/Oasis/dp/B06X96YZTP/ref=sr_1_1?ie=UTF8&qid=1493708397&sr=8-1&keywords=oasis'
  title: Oasis
  type: tv
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/oasis_2017.jpg
  number: 88
  guid: 'http://www.decipherscifi.com/?p=1515'
  media:
    audio:
      audio/mpeg:
        content_length: 29396685
        duration: '34:59'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Oasis_--_--_decipherSciFi.mp3'
---
#### Amazon Pilot Model

Amazon's practice of putting out a pile of different pilots and letting the cream rise via actual user data.

#### London 2032

Environmental collapse. Precipitous economic drop, at least locally. Population collapse maybe?

#### Space is big

The Oasis settlement is on "the edge of the galaxy." That's really far! Nearest edge to Earth still thousands of lightyears. So, magic FTL. Stasis and muscle-wasting. FTL comms may be couriered by the ships.

#### Preacher man

The role of a man of God on a space station where resources are scarce and engineers are in high demand.

#### The Oasis Settlement

Not as advertised! We're still in the early stages of building [Elysium]({% link decipherscifi/_posts/2015-12-22-elysium-movie-podcast-episode-17.md %}) here; it's still being settled. Gravity and weather systems and water. Tectonic activity and proximity to other bodies. Breathable atmospheres and how they get there.

#### One-way trips

Expense and complexity of designing for return trips in space travel. The nature of the magic FTL seems to still need normal rocket launches to get up there. Processing passenger vessels for materials.

#### *Ponics

Hydroponics. Aeroponics. Lower energy and water requirements, fine control of variables. Haley Joel is our [Mark Watney]({% link decipherscifi/_posts/2015-11-17-the-martian-podcast-episode-12.md %}) botanist.

{% responsive_image path: assets/imgs/misc/haley_joel_poop_potatoes.jpg alt: "Haley Joel Osment poop potatoes meme" %}

#### Space Dementia

Ala [Armageddon]({% link decipherscifi/_posts/2017-03-28-armageddon-feat-daniel-barker-science-communicator-episode-83.md %})? Mundane explanations, Occam's Razor, and _magnets_. Or supernatural. We'll see!

#### Magnets. How do they work?

Please see the [Veritasium](https://www.youtube.com/watch?v=1TKSfAkWWN0) and [Minute Physics](https://www.youtube.com/watch?v=hFAOXdXZ5TM) videos above. This has little (nothing) to do with Oasis, really, but it's a cool thing we wanted to share. :)

#### Predictions

What could be going on on this world and what we expect to actually happen. Solaris!
