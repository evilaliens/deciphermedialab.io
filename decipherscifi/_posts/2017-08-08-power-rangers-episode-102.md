---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-08-08 07:45:43+00:00
description: 'The Science of Power Rangers! Dinosaur extinction events, life seeding, the Eltarian conlang, collecting hobo teeth to make a gold monster, mantis shrimp'
layout: podcast_post
permalink: /decipherscifi/power-rangers-episode-102
redirect_from:
  - /decipherscifi/102
slug: power-rangers-episode-102
title: 'Power Rangers: a gold monster, Elatarian conlang, and zord suggestions'
tags:
  - constructed languages
  - dinsoaurs
  - evolution
  - extinction events
  - gold
  - language
  - life seeding
  - linguistics
  - robots
  - science
  - science fiction
  - superpowers
  - teenagers

# Episode particulars
image:
  path: assets/imgs/media/movies/power_rangers_2017.jpg
  alt: 2017 Power Ranger people standing and looking cool on like, a rock
links:
  - text: Is Rita Repulsa’s Staff More Destructive than Her Monsters? by Because Science w/ Kyle Hill
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=o0f8tM9UrxA'
  - text: POWER/RANGERS UNAUTHORIZED [BOOTLEG UNIVERSE] by Adi Shankar
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=vw5vcUPyL90'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/sabans-power-rangers/id1213572965?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Sabans-Power-Rangers-Dacre-Montgomery/dp/B06XRVMJCB'
  title: Power Rangers
  type: movie
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/power_rangers_2017.jpg
  number: 102
  guid: 'https://decipherscifi.com/?p=2921'
  media:
    audio:
      audio/mpeg:
        content_length: 30875648
        duration: '36:44'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Power_Rangers_--_--_decipherSciFi.mp3'
---
#### Dinosaur extinction event

Dinosaurs spotted in the Cretaceous period. The K-Pg (Cretaceous-Paleogene) aka K-T (Cretaceous-Tertiary) extinction event (the one with the comet). Hypothetical competition with the vulcanism explanation. Morgan Freeman, Brian Cranston, and Christopher's favorite move, the "Zap Brannigan"

#### Get off my lawn

Damn teenagers being irresponsible, endangering others and themselves and whatnot. But then they get superpowers so I guess it's cool.

#### Life seeding and crystals and whatnot

Origin of life on earth: crystals. Pro tip: it's really hard searching online for science things about crystals because all the results are woo woo nonsense. The [Prometheus problem]({% link decipherscifi/_posts/2017-05-16-prometheus-episode-90.md %}). The sameness of all life (that we see). Bipedalism seems to be a generally useful adaptation, I guess. Yay us.

#### Alien Language

[Christine Shreyer](https://twitter.com/c_schreyer) created the Eltarian conlang for the movie. [Here](https://www.voices.com/podcasts/soundstories/podcast/sound-stories-020-inside-the-process-of-constructing-languages/) is an interview where she discusses her work on both Man of Steel and Power Rangers. Erlang is a programming language, in case you get confused. Sources of ancientness that she used in the construction. Language change over time and how to avoid it.

#### Gold

10,000 nukes. Calculating the gold tonnage needed to create a Goldar. Recovering splattered riches. Places to find Goldar-sized gold deposits.

#### Megazord

Combat strategy. Our best Zord ever idea: _MANTIS SHRIMP_.

{% youtube "https://www.youtube.com/watch?v=F5FEj9U-CJM" %}

#### Controlling a Mega Zord

It's basically [Octo Dad](https://www.youtube.com/watch?v=oxKQrEGOUsY), Or [QWOP](http://www.foddy.net/Athletics.html).
