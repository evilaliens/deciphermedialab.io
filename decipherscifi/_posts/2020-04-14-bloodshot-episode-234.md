---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2020-04-14T02:45:58-04:00
description: 'Memento, but with nanites! The 1990s comics bubble. Cinematic universes. Exploding flour. Exciting prostheses! Bad superhero combat tactics. Etc.'
layout: podcast_post
redirect_from:
  - /decipherscifi/234
permalink: /decipherscifi/bloodshot-overdoing-nanotech-comics-bubbles-and-flour-bombs
slug: bloodshot-overdoing-nanotech-comics-bubbles-and-flour-bombs
title: 'Bloodshot: overdoing nanotech, comics bubbles, and flour bombs'
tags:
  - comics
  - film
  - nanotechnology
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/bloodshot_2020.jpg
  alt: Vin Diesel being all bloodshot and badass
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/bloodshot/id1499927976?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Bloodshot-Vin-Diesel/dp/B085P1N4VN'
  title: Bloodshot
  type: movie
  year: 2020
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/bloodshot_2020.jpg
  number: 234
  guid: 'https://decipherscifi.com/?p=10971'
  media:
    audio:
      audio/mpeg:
        content_length: 25274733
        duration: '30:02'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/bloodshot.final.mp3'

---
What if Memento, but with nanites?

#### Movies post pandemic

Everything is either postponed or straight-to-streaming now. What do we expect the film industry to do after this debacle?

#### Comics!

[The 1990s comics bubble](https://screenrant.com/the-comicspeculator-bubble-explained/). Chromium gimmick covers (Bloodshot was the first?).

#### Nano!

The difference between the magic of "nanotechnology" in 1993 and now. Being spoiled by Hollywood nanotech superpowers. Being impressed by self-mending t-shirts. Brute force superhero combat - no tactics whatsoever!

#### Flour

Just a heads-up, flour burns when spear in the air: [Exploding Flour @ YouTube](https://www.youtube.com/watch?v=DU3GSbIzwm8?t=168).
