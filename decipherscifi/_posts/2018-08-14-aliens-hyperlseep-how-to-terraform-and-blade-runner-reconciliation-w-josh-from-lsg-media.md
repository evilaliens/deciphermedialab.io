---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-08-14 04:22:57+00:00
description: "James Cameron rocks at sequels! Hypersleep vs cryosleep, Blade Runner, fecal plugs. Also milky robots, and the nature of Aliens' robots vs Blade Runner."
layout: podcast_post
permalink: /decipherscifi/aliens-hyperlseep-how-to-terraform-and-blade-runner-reconciliation-w-josh-from-lsg-media
redirect_from:
  - /decipherscifi/155
slug: aliens-hyperlseep-how-to-terraform-and-blade-runner-reconciliation-w-josh-from-lsg-media
title: 'Aliens: hyperlseep, how to terraform, and Blade Runner reconciliation w/ Josh Effengee'
tags:
  - aliens
  - blade runner
  - geiger
  - man meat
  - moons
  - planets
  - predator
  - science
  - science fiction
  - space combat
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/movies/aliens_1986.jpg
  alt: 'A Geiger Alien mugging for the camera, Aliens movie backdrop'
links:
  - text: Jurassic World, Jurassic Values by Mike Hill
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=CofZ7xjGyI8'
  - text: "Josh's escape room in Massachusetts"
    urls:
      - text: Mass Escape
        url: 'https://www.massescaperoom.com/'
  - text: "Josh's shows on LSG Media"
    urls:
      - text: 'Science Fiction Film Podcast'
        url: 'https://www.libertystreetgeek.net/all-science-fiction-film-podcasts/'
      - text: 'The X-Files Podcast'
        url: 'https://www.libertystreetgeek.net/all-x-files-podcasts/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/aliens/id270212246?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Aliens-Sigourney-Weaver/dp/B001609SQA'
  title: Aliens
  type: movie
  year: 1986
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: josh_effengee
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/aliens_1986.jpg
  number: 155
  guid: 'https://decipherscifi.com/?p=7376'
  media:
    audio:
      audio/mpeg:
        content_length: 41988449
        duration: '49:58'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Aliens_--_Josh_Effengee_--_Decipher_SciFi.mp3'
---
#### Love

Love! For James Cameron and Aliens. And the great utilitarian design of the spaceship interiors. Great world-building.

#### Hypersleep

Cryosleep? Ice sublimation and mummified Sigourney Weaver. Hibernation. How to shove pine cones up your butt, chug some Nyquil, and turn into a bear. Cryogenic specimen preservations: sperm! DIY fecal plugs for hibernation.

#### Private space industry

The rise of private space industry. Going dystopian.

#### Planetary operations

Planet? Moon? The delineation between planets and moons and looking forward to the future when we get confused again. Demoting Pluto to planetoid. Terraforming Mars. [Sad news on Martian CO2 supplies for atmosphere creation](https://www.universetoday.com/139725/uh-oh-mars-doesnt-have-enough-carbon-dioxide-to-be-terraformed/). Crater settlements and whether or not you need a roof. Atmospheric density limits for human comfort and thriving.

#### Xenomorphs

But are they really called Xenomorphs? [The throwaway line in Aliens that spawned decades of confusion](https://arstechnica.com/the-multiverse/2014/08/the-throwaway-line-in-aliens-that-spawned-decades-of-confusion/).

Send the robots

Always send the robots. _Unless_ you are an evil corporation with shady goals. Milk robots...?

#### Blade Runner connection

[Blade Runner and Aliens are the same universe](http://www.denofgeek.com/us/movies/alien/265927/are-alien-blade-runner-set-in-the-same-universe). The separate lines of technological development that yielded replicants and Aliens' "synthetics." Blade Runner 2049's anime prequel and how that informs the universe crossover.

{% youtube "https://www.youtube.com/watch?v=rrZk9sSgRyQ" %}
