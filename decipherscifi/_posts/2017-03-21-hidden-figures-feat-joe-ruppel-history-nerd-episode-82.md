---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-03-21 06:43:21+00:00
description: 'NACA and NASA history, early space flight, computers human and otherwise, the incredible genius women featured in the film, the future of space competition'
layout: podcast_post
permalink: /decipherscifi/hidden-figures-feat-joe-ruppel-history-nerd-episode-82
redirect_from:
  - /decipherscifi/82
slug: hidden-figures-feat-joe-ruppel-history-nerd-episode-82
title: 'Hidden Figures: the real women and some NACA/NASA history w/ Joe Ruppel'
tags:
  - science fiction
  - science
  - computing
  - nasa
  - space
  - rockets
  - cold war
  - the right stuff

# Episode particulars
image:
  path: assets/imgs/media/movies/hidden_figures_2016.jpg
  alt: Woman mathing on the blackboard
links:
  - text: Hidden Figures (the book) by Margot Lee Shetterly
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/hidden-figures/id1046759784?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Hidden-Figures-American-Untold-Mathematicians-ebook/dp/B0166JFFD0'
  - text: From the Earth to the Moon
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Earth-Moon-Various/dp/B01GGXDS9O'
  - text: Apollo 13
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/apollo-13/id299468139?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Apollo-13-Tom-Hanks/dp/B001JI72AI'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/hidden-figures/id1186154683?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Hidden-Figures-Blu-ray-Taraji-Henson/dp/B01LTI1RHQ/ref=tmm_blu_swatch_0?_encoding=UTF8&qid=1490058387&sr=8-1'
  title: Hidden Figures
  type: movie
  year: 2016
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/hidden_figures_2016.jpg
  number: 82
  guid: 'http://www.decipherscifi.com/?p=1441'
  media:
    audio:
      audio/mpeg:
        content_length: 43499520
        duration: '51:46'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Hidden_Figures_--_Joe_Ruppel_--_decipherSciFi.mp3'

---
#### On voices

My[ guest appearance](https://www.patreon.com/posts/voices-from-l5-8141348) talking about this film on the podcast Voices From L5.

#### The women!

The movie mostly follows [three](https://en.wikipedia.org/wiki/Katherine_Johnson) [incredible](https://en.wikipedia.org/wiki/Dorothy_Vaughan) [women](https://en.wikipedia.org/wiki/Mary_Jackson_(engineer)) in particular.

#### NACA/NASA

This got left out of the movie, but the history of [NACA](https://en.wikipedia.org/wiki/National_Advisory_Committee_for_Aeronautics) (National Advisory Committee for Aeronautics), then [NASA](https://en.wikipedia.org/wiki/NASA) (National Aeronautics and Space Administration), is an important part of the story.

#### Space Race

Existential threats. Intercontinental ballistic missiles and nuclear submarines obviating much of the interest in basing nuclear weapons in space. Dangerous beeps and boops.

#### Humans in Space

[Yuri Gagarin](https://en.wikipedia.org/wiki/Yuri_Gagarin) was the first man in space. Alan Shepard. National and global heroes.

#### Space stuff is dangerous

Flight testing is really unsafe. [Redstone rockets](https://en.wikipedia.org/wiki/Redstone_(rocket_family)).

#### Space Competition

National competition in the day, feels to us more healthy and global-cooperative these days. For now. Until it gets cheaper and there are resources to fight over. :(

#### Computers, human and otherwise

Human computers. Adding machines. Early programmable computers. (Justified) trust issues with these new machines. IBM 7090. Fortran.

#### John Glenn

Joe's favorite superhero. A totally amazing list of accomplishments even before the Mercury program. And then the only person to ride in both the Mercury and Shuttle programs.

#### Hats off

Appreciation for the people who pulled all of this stuff off, with special consideration for the women featured in the film.
