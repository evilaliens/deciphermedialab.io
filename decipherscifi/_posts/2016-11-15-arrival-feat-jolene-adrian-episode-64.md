---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-11-15 03:05:32+00:00
description: "Two guests join us to talk about Ted Chiang's Arrival. Superdimensional aliens, antigravity tech, alien math and physics. Linguistic relativity, Sapir-Whorf"
layout: podcast_post
permalink: /decipherscifi/arrival-feat-jolene-adrian-episode-64
redirect_from:
  - /decipherscifi/64
slug: arrival-feat-jolene-adrian-episode-64
title: 'Arrival Part 1: superdimensionality and alien physics w/ Jolene & Adrian' 
tags:
  - aliens
  - language
  - linguistics
  - math
  - physics
  - science
  - science fiction
  - ted chiang

# Episode particulars
image:
  path: assets/imgs/media/movies/arrival_2016_pt1.jpg
  alt: Human in a hazmat suit with a whiteboard Arrival movie backdrop
links:
  - text: Everything by Ted Chiang
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Ted-Chiang/e/B001HCZ6OA'
  - text: Ted Chiang on Decipher SciFi
    urls:
      - text: Decipher SciFi
        url: 'http://www.decipherscifi.com/25'
  - text: Darmok
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/darmok/id467935861?i=467939005&mt=4&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Darmok/dp/B005HEK3JW'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/arrival/id1166395905?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Arrival-Amy-Adams/dp/B01MDTS4VZ'
  title: Arrival
  type: movie
  year: 2016
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: adrian_falcone
    roles:
      - guest_cohost
  - data_name: jolene_creighton
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/arrival_2016_pt1.jpg
  number: 64
  guid: 'http://www.decipherscifi.com/?p=1130'
  media:
    audio:
      audio/mpeg:
        content_length: 40183914
        duration: '55:49'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Arrival_Jolene_Creighton_Adrian_Falcone_decipherSciFi.mp3'
---
#### Alien Ship Technology

Ship materials construction. Anti-gravity technologies. Tesseracts. Shadows looking into other dimensions.

#### Alien Math and Physics

{% responsive_image path: assets/imgs/misc/jef_raskin_alien_math.png alt: "Jef Raskin alien math puzzle thought experiment" caption: "Jef Raskin's alien math" %}

Superdimensionality. Imagining alien understanding of maths and physics. Different perspectives forcing different approaches. The physics of alien arrivals.

#### Linguistic Relativity

The strong version of the  Sapir-Whorf hypothesis is not backed by good evidence. Superdimensional alien languages. Gaining new perspective into the reality of the universe.

#### Preparation

Military secret-keeping. General Whitaker's Morpheus operandi: "I cannot tell you what the matrix is. I have to show you."
