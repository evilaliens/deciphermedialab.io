---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-08-30 07:45:16+00:00
description: 'Scott Wayne Indiana joins us to address the original The Day the Earth Stood Still. Stoic robots, violence, Klaatu language, atomic power, more'
layout: podcast_post
permalink: /decipherscifi/the-day-the-earth-stood-still-feat-scott-wayne-indiana-tech-artist-episode-52
redirect_from:
  - /decipherscifi/53
slug: the-day-the-earth-stood-still-feat-scott-wayne-indiana-tech-artist-episode-52
title: 'The Day the Earth Stood Still: Gort, nonviolence, and aliens w/ Scott W Indiana'
tags:
  - science fiction
  - science
  - aliens
  - invasion
  - language
  - ufo
  - spacecraft
  - linguistics
  - atomic power

# Episode particulars
image:
  path: assets/imgs/media/movies/the_day_the_earth_stood_still_1951.jpg
  alt: Illustrated humanoid alien with his really large bodyguard robot The Day The Earth Stood Still move backdrop
links:
  - text: Lotus Dimension
    urls:
      - text: Lotus Dimension
        url: 'http://lotusdimension.com/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/day-earth-stood-still-1951/id277018260?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Day-Earth-Stood-Still/dp/B000UL5YW8'
  title: The Day the Earth Stood Still
  type: movie
  year: 1951
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: scott_wayne_indiana
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_day_the_earth_stood_still_1951.jpg
  number: 53
  guid: 'http://www.decipherscifi.com/?p=984'
  media:
    audio:
      audio/mpeg:
        content_length: 36437427
        duration: '01:00:43'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Day_the_Earth_Stood_Still_Scott_Wayne_Indiana_decipherSciFi.mp3'
---
#### Origins

Short story "Farewell to the Master" by Harry Bates. Cold War paranoia. 50s.

#### Aliens

Humanoid and otherwise. Sex with aliens? Alien life from Mars, Venus, or maybe even moons of Jupiter.

#### Period Things

_Everybody_ smokes. Letting your kid go around town with strange men. Independent women of the 50s. More smoking.

#### Language

Emray Klaatu naruat macro proval brarato lukto denso implikit yavo tarri axell plakatio baringa degas.

#### Gort

Gort's _real_ concerns. Who is the real master? Open source space police operating systems.

#### Atomic Power

The atomic energy scene in the 50s. Great power and great responsibility.

#### Violence

Whoever carries the biggest stick seems to be right.
