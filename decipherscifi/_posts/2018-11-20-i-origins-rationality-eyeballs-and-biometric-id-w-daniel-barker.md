---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-11-20 07:45:16+00:00
description: 'Eyeball evolution. Comparative eyeball anatomy. Octopod superiority. Saccades, chronostasis, and various optical illusions. Rationality and spiritualism.'
layout: podcast_post
permalink: /decipherscifi/i-origins-rationality-eyeballs-and-biometric-id-w-daniel-barker
redirect_from:
  - /decipherscifi/168
slug: i-origins-rationality-eyeballs-and-biometric-id-w-daniel-barker
title: 'I Origins: rationality, eyeballs, and biometric ID w/ Daniel Barker'
tags:
  - bio identification
  - biology
  - creationism
  - evolution
  - optical illusions
  - science
  - science fiction
  - sensory perception

# Episode particulars
image:
  path: assets/imgs/media/movies/i_origins_2014.jpg
  alt: 'Two scientists and a bunny rabbit - I Oirigins movi e backdrop'
links:
  - text: 'Richard Dawkins demonstrates the evolution of the eye'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=2X1iwLqM2t0'
  - text: "Daniel James Barker's Podcast"
    urls:
      - text: Uncertainty Principle the Podcast
        url: 'https://uncertaintyprinciplethepodcast.com/'
  - text: 'Welcome, Marty - a special short episode'
    urls:
      - text: 'Uncertainty Principle The Podcast'
        url: 'https://soundcloud.com/uncertaintyprinciple/welcome-marty-a-special-short-episode'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/i-origins/id898297625?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/I-Origins-Michael-Pitt/dp/B00OVVKFFO'
  title: I Origins
  type: movie
  year: 2014
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: daniel_barker
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/i_origins_2014.jpg
  number: 168
  guid: 'https://decipherscifi.com/?p=8117'
  media:
    audio:
      audio/mpeg:
        content_length: 39680037
        duration: '47:13'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/I_Origins_--_Daniel_Barker_--_Decipher_SciFi.mp3'
---
#### Eyeball anatomy

All the important parts, wherever they are! Who really cares anyway we're only here for two in particular: the iris and the retina. The retinal blind spot.

#### Creationism

And intelligent design. Are wrong. Reaching conclusions and working backwards for fun and profit. Young Earth creationism. Dan's religious upbringing. Irreducible complexity. Faulty reasoning.

#### The evolution of the modern vertebrate eyeball

Fish and their goofy eyeball arrangements. The crazy Charlie eyeball progress chart. Light sensitive flat patches, pits, enclosures, open pinhole eyeballs, lenses, and flexing your sphincters. Dan's [episode on Three-D Vision](https://soundcloud.com/uncertaintyprinciple/three-d). Pepe Silvia eyeball evolution.

#### Comparative eyeball studies

Human eyes are pretty sweet! But there other ones that do other things (at their own local summits on Mount Improbable) and are also really good at stuff! Goats, sharks, snakes (sorta), bees. Removing your lenses for fun and profit. Monet's lens removal and ultraviolet perception.

#### Eyeball shortcomings

Common "colorblindness" is a misnomer, but "color discernment syndrome" doesn't flow as well. Retinal wiring and the "blind spot." [Saccades](https://en.wikipedia.org/wiki/Saccades). "[Chronostasis](https://en.wikipedia.org/wiki/Chronostasis)" or "the stopped clock illusion" and the tight coupling of our eyes and our brains.

#### Bio-identification

Selecting scanning sites for statistical uniqueness and natural resistance to hacking. The insufficiency of fingerprints. Iris scans. Retinal scans. Bio-identification security. Ease of use.

#### Rationality

Rationalism/spiritualism/etc. The rational scientific worldview and self-correction. The absolution of new knowledge.
