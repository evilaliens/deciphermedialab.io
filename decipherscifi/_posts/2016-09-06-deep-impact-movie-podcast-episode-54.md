---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-09-06 07:45:43+00:00
description: 'With Deep Impact on the show we talk citizen astronomy, extinction level events, variously sized cosmic impactors, avoiding impacts, the end of the world'
layout: podcast_post
permalink: /decipherscifi/deep-impact-movie-podcast-episode-54
redirect_from:
  - /decipherscifi/54
slug: deep-impact-movie-podcast-episode-54
title: 'Deep Impact: citizen scientists, extinction events, and avoidance'
tags:
  - science fiction
  - science
  - citizen astronomy
  - astronomy
  - exctinction events
  - meteors
  - kt extinction event
  - space impactors

# Episode particulars
image:
  path: assets/imgs/media/movies/deep_impact_1998.jpg
  alt: Impactor striking Earth from space Deep Impact movie backdrop
links:
  - text: The Formation of the Moon
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Fwl_JBQtH9o'
  - text: Asteroids in Resonance with Jupiter
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=yt1qPCiOq-8'
  - text: Death from the Skies by Phil Plait
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/death-from-the-skies!/id357986849?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Death-Skies-Science-Behind-World-ebook/dp/B001FA0LY4'
  - text: Solar System Collision Calculator
    urls:
      - text: Astronomy Workshop
        url: 'http://janus.astro.umd.edu/astro/impact/'
media:
  links:
    - text: iTunes
      url: 'https://www.amazon.com/Deep-Impact-Robert-Duvall/dp/B005DN7W4G/ref=sr_1_1?ie=UTF8&qid=1473127643&sr=8-1&keywords=deep+impact'
    - text: Amazon
      url: 'https://www.amazon.com/Deep-Impact-Robert-Duvall/dp/B005DN7W4G'
  title: Deep Impact
  type: movie
  year: 1998
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/deep_impact_1998.jpg
  number: 54
  guid: 'http://www.decipherscifi.com/?p=1000'
  media:
    audio:
      audio/mpeg:
        content_length: 27609824
        duration: '46:00'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Deep_Impact_decipherSciFi.mp3'
---

{% responsive_image path: assets/imgs/selfies/deep_impact.jpg alt: "Decipher Media hosts looking in the wrong direction with a telescope" %}

#### Citizen Astronomy

People. Computers. Computer vision. [KIC 8462852](https://en.wikipedia.org/wiki/KIC_8462852).

#### E.L.E.

Extinction level events. Dinosaur impact. Impactor scales.

#### Impactor Types

Meteors, meteorites, meteors, comets, asteroids.

#### Avoiding Impacts

Gentle (nuclear bombs), and less gentle (gravity tug) methods of redirecting impactors.

#### Earth Impacts

Tunguska, dinosaur impact. The difference between a small number of large impacts and a large number of small ones.

#### Humanity's Fate

Floods, atmospheric disruption, a world aflame from the heat of a million impactors.
