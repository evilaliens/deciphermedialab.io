---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-03-29 08:40:08+00:00
description: 'Our history expert is back to discuss with us the context that produced the original War of the Worlds book, alien attack methods, harvesting humans, more.'
layout: podcast_post
permalink: /decipherscifi/war-of-the-worlds-joe-ruppel-movie-podcast-episode-31
redirect_from:
  - /decipherscifi/31
slug: war-of-the-worlds-joe-ruppel-movie-podcast-episode-31
title: 'War of the Worlds: aliens, biological warfare, and Orson Welles radio w/ Joe Ruppel'
tags:
  - science fiction
  - science
  - history
  - hg wells
  - dystopia
  - apocalypse
  - genocide
  - family
  - alien invasion

# Episode particulars
image:
  path: assets/imgs/media/movies/war_of_the_worlds_2005.jpg
  alt: War of the Worlds Tom Cruise movie backdrop
links:
  - text: War of the Worlds by H.G. Wells
    urls:
      - text: Project Gutenberg (ebook)
        url: 'https://www.gutenberg.org/ebooks/36'
      - text: Project Gutenberg (audiobook)
        url: 'https://librivox.org/war-of-the-worlds-solo-by-h-g-wells/'
  - text: War of the Worlds (1953 film)
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/the-war-of-the-worlds-1953/id210440245?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'http://www.amazon.com/War-Worlds-Gene-Barry/dp/B000JGD26E'
  - text: War of the Worlds Orson Welles original radio broadcast
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Xs0K4ApWl4g'
  - text: Could it happen again? And again?
    urls:
      - text: Radiolab
        url: 'http://www.radiolab.org/story/91624-could-it-happen-again-and-again/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/war-of-the-worlds-2005/id929618785?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/War-Worlds-Tom-Cruise/dp/B01B5FYNM4'
  title: War of the Worlds
  type: movie
  year: 2005
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 31
  guid: 'http://www.decipherscifi.com/?p=689'
  media:
    audio:
      audio/mpeg:
        content_length: 25216264
        duration: '42:02'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/War_of_the_Worlds_Joe_Ruppel_decipherSciFi.mp3'
---
#### War of the Worlds - the book, the movie

Original book. Orson Welled radio broadcast hysteria not as reported. Radiolab report on repeated adaptations. The new developments at the time of writing that informed the book.

#### Alien Attack

Methods, research, planning. Trying to explain the buried  death-bots.

#### But Why

Are we food? Do they want our resources? Making the most sense of the options. Are liquified people good for fertilizer?

#### Germ Attack

They might have done a little more research.
