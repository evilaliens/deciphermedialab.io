---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-10-11 07:45:58+00:00
description: 'We address zombies for the first time with 28 Days Later. Infectious disease, survival strategy, virus vs parasite, parkour vs shambling zombies'
layout: podcast_post
permalink: /decipherscifi/28-days-later-feat-jolene-creighton-of-futurism-episode-59
redirect_from:
  - /decipherscifi/59
slug: 28-days-later-feat-jolene-creighton-of-futurism-episode-59
title: '28 Days Later: zombie attacks, survival skills, and a quarrel about plagues w/ Jolene Creighton'
tags:
  - science fiction
  - science
  - zombies
  - pathogens
  - disease
  - viruses
  - parasites
  - infection
  - taxonomy

# Episode particulars
image:
  path: assets/imgs/media/movies/28_days_later_2002.jpg
  alt: Red London silhouette 28 Days Later backdrop
links:
  - text: Shaun of the Dead
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/shaun-of-the-dead/id279610304?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'https://www.amazon.com/Shaun-Dead-Simon-Pegg/dp/B0018OIK0E'
  - text: Dead Island
    urls:
      - text: Steam
        url: 'http://store.steampowered.com/app/383150/'
  - text: Brookhaven Experiment for HTC Vive
    url:
      - text: Steam
        url: 'http://store.steampowered.com/app/440630/agecheck'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/28-days-later/id569220589?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/28-Days-Later-Cillian-Murphy/dp/B00C4QLIJK'
  title: 28 Days Later
  type: movie
  year: 2002
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: jolene_creighton
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/28_days_later_2002.jpg
  number: 59
  guid: 'http://www.decipherscifi.com/?p=1047'
  media:
    audio:
      audio/mpeg:
        content_length: 36802156
        duration: '43:48'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/28_Days_Later_Jolene_Creighton_decipherSciFi.mp3'
---

{% responsive_image path: assets/imgs/selfies/28_days_later.jpg alt: "Decipher SciFi hosts being attacked by zombie Jolene Cleighton" %}

#### Lab Safety

Security in researching highly contagious zombie viruses. Animal liberation.

#### Biological Infection

Viruses, parasites, R0, monkeys. Infection rates. Bloodborne illness.

#### Zombies

Zombie taxonomy. Do these count as zombies? A disgreement. Fainting goats.

{% youtube "https://www.youtube.com/watch?v=uT-UGTQd6zQ" %}

#### Preparedness

[CDC recommendations](http://www.cdc.gov/phpr/zombies.htm). Weapon choice. Get on a boat.

#### Hospital Escape

Cillian Murphy's wiener. [Playing God](http://www.radiolab.org/story/playing-god/).
