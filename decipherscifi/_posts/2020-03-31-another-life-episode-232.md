---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - tv
  - pilot series
date: 2020-03-31T02:46:43-04:00
description: 'Young and brash human drama... in spaaaaace. Moebius ships. What alien ship designs tell us about a story. The difficulty of stumbling across rogue planets. Squeezing O² out of rocks. Etc!'
layout: podcast_post
permalink: /decipherscifi/another-life-visible-rogue-planets-visible-dark-matter-and-other-smart-stuff
redirect_from:
  - /decipherscifi/232
slug: another-life-visible-rogue-planets-visible-dark-matter-and-other-smart-stuff/
title: 'Another Life: visible rogue planets, visible dark matter, and other smart stuff'
tags:
  - aliens
  - ftl
  - science
  - science fiction
  - space
  - space travel

# Episode particulars
image:
  path: assets/imgs/media/tv/another_life_2019.jpg
  alt: "Starbuck from Battlestar floating and being like 'whoah, space'"
links:
  - text: 'Battlestar Galactica'
    urls:
      - text: iTunes
        url: 'https://itunes.apple.com/gb/tv-season/bsg-the-complete-collection/id946709572?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/33/dp/B000UU2YKE'
  - text: 'Battlestar Galactica: curse words linguistics, formal code verification, and robo Jesus'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/199'
media:
  links:
    - text: Netflix
      url: 'https://www.netflix.com/title/80236236'
  title: Another Life
  type: tv
  year: 2019
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/another_life_2019.jpg
  number: 232
  guid: 'https://decipherscifi.com/?p=10962'
  media:
    audio:
      audio/mpeg:
        content_length: 25495132
        duration: '30:14'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/another_life.final.mp3'

---
# First contact

Aliens. Alien intelligence. Alien vessel shapes and what it says about the show you are about to watch. Moebius ships?

# CETI

Crystalline FTL space antennae. The realities of lightspeed communication and why [The SETI Institute](https://en.wikipedia.org/wiki/SETI_Institute) *doesn't* do "[Communication with extraterrestrial intelligence](https://en.wikipedia.org/wiki/Communication_with_extraterrestrial_intelligence)."  Unexplained FTL comms.

# Fumbling about space

When your crew is this good, you really should just send a ship without the humans on it instead. MapQuest in spaaaaaace being confounded by gravitational lensing. Dark matter... blocks your view sometimes? Rogue planets and how very very very dark it is in between star systems. [Robinson Crusoe oxygen rocks]({$ link decipherscifi/_posts/2016-09-27-robinson-crusoe-on-mars-episode-57.md %}) (or crystals).




