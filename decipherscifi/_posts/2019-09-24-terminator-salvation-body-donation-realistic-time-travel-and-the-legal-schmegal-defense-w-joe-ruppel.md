---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-09-24 07:45:30+00:00
description: 'Continuing Terminator series coverage! Body donation and lethal injection. Realistic time travel, terminator skin-jobs, the results of AI takeoff. Etc.'
layout: podcast_post
permalink: /decipherscifi/terminator-salvation-body-donation-realistic-time-travel-and-the-legal-schmegal-defense-w-joe-ruppel
redirect_from:
  - /decipherscifi/211
slug: terminator-salvation-body-donation-realistic-time-travel-and-the-legal-schmegal-defense-w-joe-ruppel
title: 'Terminator Salvation: body donation, realistic time travel, and the legal shmegal defense w/ Joe Ruppel'
tags:
  - cyborg
  - future
  - man versus machine
  - medicine
  - science
  - science fiction
  - skynet
  - terminator
  - time travel

# Episode particulars
image:
  path: assets/imgs/media/movies/terminator_salvation_2009.jpg
  alt: 'john Connor and another guy in the future where robots are trying to exterminate humanity'
links:
  - text: 'Terminator Salvation the Machinima Series'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=-aTmkMpUm8I&list=PL_AYqxzHii9izIjWUY0kP9vzQOYV18Xqy'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/terminator-salvation-directors-cut/id338372479?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Terminator-4-Salvation-Christian-Bale/dp/B002RRN952'
  title: Terminator Salvation
  type: movie
  year: 2009
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/terminator_salvation_2009.jpg
  number: 211
  guid: 'https://decipherscifi.com/?p=10796'
  media:
    audio:
      audio/mpeg:
        content_length: 42439810
        duration: '50:25'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/terminator_salvation.mp3'

---
Is this a sequel? A prequel? Our progress in covering the entire Terminator series. The possibility of the future war trilogy, dashed.

#### Body donation

Donating your body to science. Body donation vs organ donation. Size and BMI standards. Medical education via cadaver. "Body brokers." Why do blast testing on bodies? Are we learning to cure "acute dynamitism?"

#### Lethal injection

How to make a body unsuitable for whole-body donation. Donating to Terminator-science. Executions as disincentive. The role of the medieval executioner. How the guillotine was an early example of the robots taking our jerbs. Execution showmanship. The Ultimate Warrior was _nuts_.

{% youtube "https://www.youtube.com/watch?v=Q46l70mNmCw" %}

#### Time travel

The only sorta realistic time travel in the whole Terminator series: pausing your exiistence and turning back on in the future! The nature of self, and the "legal schmegal" defense.

#### Terminators

The most human Terminator yet? Robocop vs Wolverine? Head transplant or body transplant? Terminator production volume and just-in-time delivery.  The first T800. A T600 review. Austrian bodybuilders always make good Terminators.

#### Skynet

Technological anxieties over time through the Terminator series.... but not really in this one? Or are just jumping straight through to concern with AI takeoff? The Skynet-iest movie in the Terminator series, so far. Personifying Skynet. Explaining the human prisoner project with the design of the T800.

#### Madness

Amazing alternate endings that Chris is sad didn't happen! Skinning John Connor, for fun and profit.
