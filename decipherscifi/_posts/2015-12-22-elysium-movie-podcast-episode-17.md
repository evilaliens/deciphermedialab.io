---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2015-12-22 04:45:53+00:00
description: 'With Elysium on the podcast we talk lots about space settlement with our guest, language and class, exoskeletons, magical healing chambers, neural tech'
layout: podcast_post
permalink: /decipherscifi/elysium-movie-podcast-episode-17
redirect_from:
  - /decipherscifi/17
slug: elysium-movie-podcast-episode-17
title: "Elysium: power suits, wealth gaps, and an O'Neill cylinder"
tags:
  - brain storage
  - class warfare
  - cyborg
  - encryption
  - exoskeleton
  - implants
  - language
  - science
  - science fiction
  - sharlto copley
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/movies/elysium_2013.jpg
  alt: Elysium Matt Damon movie backdrop
links:
  - text: Voices From L5
    url: 'http:/voicesfroml5.space'
  - text: "The High Frontier - by Gerard O'Neill"
    urls:
      - text: Amazon
        url: 'http://www.amazon.com/High-Frontier-Human-Colonies-Space-ebook/dp/B00CB3SIAI'
  - text: District 9
    urls:
      - text: Amazon
        url: 'http://www.amazon.com/District-9-Sharlto-Copley/dp/B0030GJ50C'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/elysium/id686619568?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Elysium-Matt-Damon/dp/B00G3HSF7O'
  title: Elysium
  type: movie
  year: 2013
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: liam_ginty
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 17
  guid: 'http://www.decipherscifi.com/?p=389'
  media:
    audio:
      audio/mpeg:
        content_length: 29702019
        duration: '41:14'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Elysium_decipherSciFi.mp3' 
---
#### Language

Spanish, French, Transatlantic Accent. Class identifier. Norman invasion. Linguistic bigotry.

#### Rich Folks

Facial branding. Fancy Pip Boys. Differences in rich and poor tech.

#### Magical Healing Chamber

How does it work? Neurological limitations. Altering personality.

#### Neural Tech

Brain storage implants. Encryption. Kill switches.

#### Exo Suits

REALLY cool installation montage. Order of installation operations. Current tech. [Warhammer Space Marines](https://en.wikipedia.org/wiki/Space_Marine_(Warhammer_40,000)).

{% youtube "https://www.youtube.com/watch?v=ViL4bAUGuGY" %}

#### Space Settlement

[Voices from L5](https://www.patreon.com/VoicesFromL5/posts)! Space planes. Torus settlements. Artificial gravity. Division of rich and poor. Space exploration

{% responsive_image_block %}
  path: 'assets/imgs/misc/stanford_torus_interior.jpg'
  alt: 'Stanford Torus'
  caption: 'Stanford Torus'
  attr_text: 'Donald Davis CC-0'
  attr_url: 'http://www.donaldedavis.com/PARTS/allyours.html'
{% endresponsive_image_block %}
