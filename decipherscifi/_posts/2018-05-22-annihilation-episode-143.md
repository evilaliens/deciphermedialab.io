---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-05-22 07:45:52+00:00
description: 'Annihilation science! Deciphering the shimmer, and the nature of the shimmerer if there is such a thing. Transformational and destructive information processing. Runaway Von Neumann machines. Mutation and evolution. Horizontal gene transfer. Learning how to live with agentive intestines. What happened at the end. More!'
layout: podcast_post
permalink: /decipherscifi/annihilation-episode-143
redirect_from:
  - /decipherscifi/143
slug: annihilation-episode-143
title: 'Annihilation : Episode 143'
tags:
  - science fiction
  - science

# Episode particulars
image:
  path: assets/imgs/media/movies/annihilation_2018.jpg
  alt: Science team walking into the Shimmer, Annihilation movie backdrop
links:
  - text: 'The Girl With All the Gifts: parasitic fungi, speed of smell, and the end of humanity'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/96'
  - text: 'Ex Machina: ai software, wetware, and the turing test'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/3'
  - text: The Deadliest Being on Planet Earth – The Bacteriophage by Kurzgesagt
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=YI3tsmFsrOg'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/annihilation/id1341054673?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Annihilation-Natalie-Portman/dp/B079YYHM9Z'
  title: Annihilation
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/annihilation_2018.jpg
  number: 143
  guid: 'https://decipherscifi.com/?p=6497'
  media:
    audio:
      audio/mpeg:
        content_length: 36993443
        duration: '44:01'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Annihilation_--_--_Decipher_SciFi.mp3'
---
#### Alex Garland

❤️. [Ex Machina]({% link decipherscifi/_posts/2015-09-22-ex-machina-movie-podcast-episode-003.md %}).

#### Abominable mutants

Shark-gators. Flower-deer. Man-bear-mole-pig. Dr Manhattan-style animal transformations. Big blue tree phalluses.

#### Horizontal gene transfer

Micro vs macro.

#### Continuous mutation

_Plants_.

#### The shimmer

Exponential understanding. Deciphering the puzzle of Earth life. Clams with human faces. Desctructive/transformative information processing. [They're Made Out of Meat](https://www.youtube.com/watch?v=7tScAyNaRdQ)

#### Micro-life in the shimmer

Boa constrictors in your gut. Tapeworm run amok. Gut bacterial colonies and fungus. Cordyceps zombie fungus a la [The Girl With All the Gifts]({% link decipherscifi/_posts/2017-06-27-the-girl-with-all-the-gifts-episode-96.md %}).

#### The Shimmerrer

Interfacing with _alien_ life. Different levels of understanding. Von Neumann machines.

#### The ending

Deciphering the ending without putting the major spoilage here in this page.
