---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - tv
date: 2017-01-31 07:45:43+00:00 
description: 'Liam Ginty is back for our second episode on Black Mirror Season 3. Happy endings, crippling fear of death, creepy watson sex dreams, bee in the ear, etc' 
layout: podcast_post
permalink: /decipherscifi/black-mirror-season-3-part-2-feat-liam-ginty-episode-75
redirect_from:
  - /decipherscifi/75
slug: black-mirror-season-3-part-2-feat-liam-ginty-episode-75 
title: 'Black Mirror Season 3, Part 2: happy endings, brainwashing, and beeeeeeeeeees w/ Liam Ginty' 
tags:
  - augmented reality
  - digital immortality
  - drones
  - dystopia
  - future
  - immortality
  - love
  - robots
  - science
  - science fiction
  - social media
  - vr

# Episode particulars
image:
  path: assets/imgs/media/tv/black_mirror_2011_season_3_part_2.jpg
  alt: Black Mirror season 3 collage featuring episodes San Junipero, Men against Fire, and Hated in the Nation
links:
media:
  links:
    - text: Netflix
      url: 'http://www.netflix.com/title/70264888'
  title: Black Mirror
  type: tv
  year: 2011
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: liam_ginty
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/black_mirror_2011_season_3_part_2.jpg
  number:  75
  guid: 'http://www.decipherscifi.com/?p=1281'
  media:
    audio:
      audio/mpeg:
        content_length: 50931712
        duration: '01:10:43'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Black_Mirror_Season_3_Part_2_--_Liam_Ginty_--_decipherSciFi.mp3'
---
## Episodes 4-6

San Junipero, Men Against Fire, Hated in the Nation

#### San Junipero

Happy endings. Consciousness uploading. Children of the 80s, idealistic recollections and the English lens. Continuity of self. Crippling fear of death.

{% youtube "https://www.youtube.com/watch?v=nQHBAdShgYI" %}

#### Men Against Fire

How we get people to do things for us and how technology can make that even more terrible. [Hacking visual input](https://en.wikipedia.org/wiki/BLIT_(short_story)). Creepy Watson sex dreams.

{% youtube "https://www.youtube.com/watch?v=13YlEPwOfmk" %}

#### Hated in the Nation

Covered in beeeeeees. Drone swarms. Social media hate.

{% youtube "https://www.youtube.com/watch?v=ezTayb76x9U" %}

{% youtube "https://www.youtube.com/watch?v=Xs-tl6GBOBo" %}
