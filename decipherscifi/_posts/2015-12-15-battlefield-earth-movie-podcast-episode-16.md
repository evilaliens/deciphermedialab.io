---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: '2015-12-15 05:45:28+00:00'
description: 'With Battlefield Earth on the podcast we cover Scientology, pulp sci-fi, the Fermi paradox, the great filter, nature taking over after humans, and more!'
layout: podcast_post
permalink: /decipherscifi/battlefield-earth-movie-podcast-episode-16
redirect_from:
  - /decipherscifi/16
slug: battlefield-earth-movie-podcast-episode-16
title: 'Battlefield Earth: L Ron Hubbard, fermi paradox, and intergalactic war w/ Daniel Mann'
tags:
  - science fiction
  - science
  - aliens
  - alien colonization
  - pulp
  - war
  - scientology
  - drake equation
  - fermi paradox

# Episode particulars
image:
  path: assets/imgs/media/movies/battlefield_earth_2000.jpg
  alt: Battlefield Earth movie backdrop
links:
  - text: Battlefield Earth - by L Ron Hubbard
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/battlefield-earth/id367853373?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Battlefield-Earth-Saga-Stories-Golden-ebook/dp/B009MA212Y'
  - text: Life After People
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/life-after-people/id257241074?i=272491628&at=1001l7hP&mt=4'
      - text: Amazon
        url: 'http://www.amazon.com/Life-After-People-History-Channel/dp/B0012IV3PU'
  - text: Looking beyond the Cover and baggage of Battlefield Earth - by Jason Heller
    urls:
      - text: AV Club
        url: 'http://www.avclub.com/article/looking-beyond-the-coverand-the-baggageof-ibattlef-83934'
  - text: The Fermi Paradox
    urls:
      - text: Wait But Why
        url: 'http://waitbutwhy.com/2014/05/fermi-paradox.html'
media:
  title: Battlefield Earth
  type: movie
  year: 2000
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: daniel_mann
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 16
  guid: 'http://www.decipherscifi.com/?p=375'
  media:
    audio:
      audio/mpeg:
        content_length: 34603436
        duration: '48:03'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Battlefield_Earth_decipherSciFi.mp3'
---
#### 50s Pulp Stories

Chewed to bits by giant turtles! [The Pulpiest Pulp](http://www.menspulpmags.com/2009/10/chewed-to-bits-by-giant-turtles-mans.html).

The pulp school L. Ron Hubbard came from. The book should not stand out much against this background. Except it came out in the 80s!

#### The Film

_DUTCH TILTS_. The book Hubbard most wanted made into a movie. Travolta stood behind it as a love letter to Scientology. Reportedly, interference from Scientology higher-ups destroyed the whole thing, and then they threw John Travolta under the bus.

{% youtube "https://www.youtube.com/watch?v=VHDk8YvgNlw" %}

#### Earth After 1000 Years

Nature would take over. Paper would waste away. People would... start to act like chimps? Chernobyl. The Koream DMZ.

#### The Fermi Paradox

Read about the paradox on [Wikipedia](https://en.wikipedia.org/wiki/Fermi_paradox). The Psychlos as the great filter. [_They're Made Out of Meat_](http://www.thetruthpodcast.com/story/2015/10/14/theyre-made-out-of-meat) on The Truth Podcast.

#### Judging Alien Intelligence

Psychlos underestimating humans. Tool use and language.

#### Alien Design

In the book, they were feline! Douchey Klingons. Breathable atmosphere. High and low gravity. Cats are jerks.

{% youtube "https://www.youtube.com/watch?v=ncBcJ8RFteg" %}

#### Intergalactic Civilization

Why gold? Why slaves? Corporate space exploitation.
