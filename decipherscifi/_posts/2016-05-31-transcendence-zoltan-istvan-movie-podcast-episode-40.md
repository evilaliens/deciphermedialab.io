---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-05-31 07:00:20+00:00
description: 'Zoltan Istvan is on the show with us talking about Transcendence, the singularity, transhumanism, atheism, techno-utopianism, AI, living forever, more'
layout: podcast_post
permalink: /decipherscifi/transcendence-zoltan-istvan-movie-podcast-episode-40
redirect_from:
  - /decipherscifi/40
slug: transcendence-zoltan-istvan-movie-podcast-episode-40
title: 'Transcendence: Transhumanism and AI takeoff scenarios w/ Zoltan Istvan'
tags:
  - science fiction
  - science
  - utopia
  - transhumanism
  - resurrection
  - nanotechnology
  - artificial intelligence
  - medicine
  - computing
  - machine learning
  - futurism

# Episode particulars
image:
  path: assets/imgs/media/movies/transcendence_2014.jpg
  alt: Johnny Depp digitally disintegrating movie backdrop
links:
  - text: Zoltan Istvan
    urls:
      - text: ZoltanIstvan.com
        url: 'http://www.zoltanistvan.com/'
  - text: 'Superintelligence: Paths, Dangers, Strategies by Nick Bostrom'
    urls:
      - text: Amazon
      - url: 'https://www.amazon.com/Superintelligence-Dangers-Strategies-Nick-Bostrom-ebook/dp/B00LOOCGB2'
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/superintelligence/id899568056?mt=11&at=1001l7hP'
  - text: The Singulatiry is Near by Ray Kurtzweil
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/the-singularity-is-near/id361932532?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Singularity-Near-Humans-Transcend-Biology-ebook/dp/B000QCSA7C'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/transcendence-2014/id872652035?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Transcendence-Johnny-Depp/dp/B00K6RYVGQ'
  title: Transcendence
  type: movie
  year: 2014
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: zoltan_istvan
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/transcendence_2014.jpg
  number: 40
  guid: 'http://www.decipherscifi.com/?p=795'
  media:
    audio:
      audio/mpeg:
        content_length: 31525022
        duration: '52:32'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Transcendence_Zoltan_Istvan_decipherSciFi.mp3'
---
#### Zoltan!

Running for President of the USA as an atheist and a transhumanist.

#### The Singularity

Kurzweil. Vernor Vinge. The post-work economy. Luddites and progress.

#### Artificial Intelligence

Uploading the human mind. [Will people try to convert the first real artificial super intelligence](http://gizmodo.com/when-superintelligent-ai-arrives-will-religions-try-t-1682837922)?

#### Transhumanism

Improving humanity through technology. Medicine. Life extension and immortality. The Transhumanist Wager. The power of love.
