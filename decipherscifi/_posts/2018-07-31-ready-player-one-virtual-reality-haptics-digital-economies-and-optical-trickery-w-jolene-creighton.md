---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-07-31 07:45:08+00:00
description: 'Fandom and identity. Ad-hoc urban planning and The Stacks vs Kowloon Walled City. Virtual moistness simulations. Optical VR trickery for irl movement. Etc.'
layout: podcast_post
permalink: /decipherscifi/ready-player-one-virtual-reality-haptics-digital-economies-and-optical-trickery-w-jolene-creighton
redirect_from:
  - /decipherscifi/153
slug: ready-player-one-virtual-reality-haptics-digital-economies-and-optical-trickery-w-jolene-creighton
title: 'Ready Player One: virtual reality haptics, digital economies, and optical trickery'
tags:
  - cities
  - computing
  - fandom
  - science
  - science fiction
  - virtual reality

# Episode particulars
image:
  path: assets/imgs/media/movies/ready_player_one_2018.jpg
  alt: 'Kid in VR in a messy trailer, Ready Player One movie backdrop'
links:
  - text: 'Ready Player One (the book) by Ernest Cline'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/ready-player-one/id422540053?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Ready-Player-One-Ernest-Cline-ebook/dp/B004J4WKUQ'
  - text: 'Saccade-driven Redirected Walking for VR'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=ySbhJEF9fXU'
  - text: 'A Real Life Haptic Glove (Ready Player One Technology Today) by Smarter Every Day'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=OK2y4Z5IkZ0'
  - text: 'The Infinadeck Omnidirectional Treadmill by Smarter Every Day'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=fvu5FxKuqdQ'
  - text: 'Kowloon Walled City interactive feature'
    urls:
      - text: WSJ
        url: 'http://projects.wsj.com/kwc/'
  - text: 'The Brookhaven Experiment'
    urls:
      - text: Steam
        url: 'https://store.steampowered.com/app/440630/agecheck'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/ready-player-one/id1354248992?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Ready-Player-One-Tye-Sheridan/dp/B07BDRR6S1'
  title: Ready Player One
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: jolene_creighton
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/ready_player_one_2018.jpg
  number: 153
  guid: 'https://decipherscifi.com/?p=7225'
  media:
    audio:
      audio/mpeg:
        content_length: 44004276
        duration: '52:22'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Ready_Player_One_--_Jolene_Creighton_--_Decipher_SciFi.mp3'
---
#### Fandom

Identity according to interests. Fiction as personal and social mirror. Cultures and subcultures and super-niches.

#### The "Stacks"

Ad-hoc city large-scale construction. [Kowloon Walled City](https://en.wikipedia.org/wiki/Kowloon_Walled_City).

#### Virtual Reality immersion technology

VR treadmills vs slippy-feet ([Virtuix Omni](https://www.youtube.com/watch?v=1SlZvuhABGk)). Looking silly. Scaling and translation from real-world input to virtual worlds. Manual and body haptics. Simulating _moistness_. Nard-stimulation for good and ill.

#### VR spatial awareness

Directional trickery in virtual reality. Tricking people into experiencing smaller real-world spaces as much larger in virtual reality. [Saccade-driven Redirected Walking for VR](https://www.youtube.com/watch?v=ySbhJEF9fXU).

#### Resolution of virtual worlds

Pixel densities and optical foci. Continuity of experience and detection of the difference between virtual and simulated worlds. Finding the clever "hacks" that will enable us to more deeply immerse ourselves in VR experiences.

#### Sharding

Virtual world scale and sharding vs mon-worlds.Virtu

#### Value systems

_Real_ virtual economies and responsibility.
