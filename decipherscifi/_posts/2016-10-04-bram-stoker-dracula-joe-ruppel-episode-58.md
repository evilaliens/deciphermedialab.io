---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-10-04 07:45:17+00:00
description: 'Our first special horror episode of the month! Joe Ruppel is back to talk Dracula: in history, in the book, in the movie. Sexiness. Cooking and eating blood'
layout: podcast_post
permalink: /decipherscifi/bram-stoker-dracula-joe-ruppel-episode-58
redirect_from:
  - /decipherscifi/58
slug: bram-stoker-dracula-joe-ruppel-episode-58
title: "Dracula: Vlad the Impaler, Elizabethan sexuality, and Kenau's awesome accent w/ Joe Ruppel"
tags:
  - blood
  - history
  - horror
  - keanu
  - religion
  - science
  - science fiction
  - sex
  - supernaturalism
  - vampires

# Episode particulars
image:
  path: assets/imgs/media/movies/dracula_1992.jpg
  alt: Scary monster head sculpture Bram Stoker's Dracula movie backdrop
links:
  - text: Bram Stoker's Dracula by Bram Stoker
    urls:
      - text: Project Gutenberg
        url: 'https://www.gutenberg.org/ebooks/345'
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/dracula/id395541616?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Dracula-Enriched-Classics-Bram-Stoker-ebook/dp/B000FC0NP0'
      - text: Librivox
        url: 'https://librivox.org/dracula-by-bram-stoker-2/'
  - text: Dracula (1931)
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/dracula-1931/id322443654?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'https://www.amazon.com/Dracula-Tod-Browning/dp/B002MG2RG0'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/bram-stokers-dracula/id270006501?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Bram-Stokers-Dracula-Gary-Oldman/dp/B00170GXQ2'
  title: Bram Stoker's Dracula
  type: movie
  year: 1992
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: joe_ruppel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/dracula_1992.jpg
  number: 58
  guid: 'http://www.decipherscifi.com/?p=1038'
  media:
    audio:
      audio/mpeg:
        content_length: 58097860
        duration: '01:00:31'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Bram_Stoker_Dracula_Joe_Ruppel_decipherSciFi.mp3'
---

{% responsive_image path: assets/imgs/selfies/bram_stokers_dracula.jpg alt: "Decipher SciFi co-hosts standing in a gloomy castle with their shadows fighting as in the movie Bram Stoker's Dracula" %}

#### Keanu

We love Keanu. But that accent! Oy vey. And standing across from Gary Oldman, no less.

#### Vlad

Vlad history lesson. Spikes in the butt. Motivations.

#### Elizabethan Sexuality

Promiscuity, puritanism. Syphilis. Bloooood.

#### Vampire Rules

How to vampire.

#### Eating/drinking Blood

Maasi blood drinking. Blood preparations in various world cuisines. Blood fetishes?

#### Science vs Supernaturalism

In the period and place.
