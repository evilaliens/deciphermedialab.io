---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-06-28 07:25:39+00:00
description: 'With Jupiter Ascending on the show, we talk Jupiter, the Fermi paradox, space feudalism, gene editing, Channing Tatum with his mouth open, more'
layout: podcast_post
permalink: /decipherscifi/jupiter-ascending-episode-44
redirect_from:
  - /decipherscifi/44
slug: jupiter-ascending-episode-44
title: 'Jupiter Ascending: gene splicing, industrialized panspermia, and going over the top'
tags:
  - science fiction
  - science
  - jupiter
  - space
  - nihilism
  - genetic modification
  - animals
  - planet seeding
  - prometheus

# Episode particulars
image:
  path: assets/imgs/media/movies/jupiter_ascending_2015.jpg
  alt: Channing Tatum and Mila Kunis standing in front of Jupiter (and Earth somehow?) movie backdrop
links:
  - text: "A Quick Note on Jupiter Ascending's Politics"
    urls:
      - text: This Cage is Worms
        url: 'https://thiscageisworms.com/2015/06/25/a-quick-note-on-jupiter-ascendings-politics/'
  - text: "Antibodies Part 1: CRISPR by Radiolab"
    urls:
      - text: Radiolab
        url: 'http://www.radiolab.org/story/antibodies-part-1-crispr/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/jupiter-ascending/id960831851?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Jupiter-Ascending-Channing-Tatum/dp/B00T9DLXYQ'
  title: Jupiter Ascending
  type: movie
  year: 2015
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/jupiter_ascending_2015.jpg
  number: 44
  guid: 'http://www.decipherscifi.com/?p=868'
  media:
    audio:
      audio/mpeg:
        content_length: 25009079
        duration: '41:40'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Jupiter_Ascending_decipherSciFi.mp3'
---
#### Jupiter

Baby naming. Looking through a telescope with the kids and co-bff Joe. Astrology. 😒

#### Fermi Padadox

_Jupiter Jones_: Because a dream is the only way any of this make sense.
_Caine Wise_: Compared to what? The idea that you're the only intelligent species, on the only inhabitable planet, in a universe so full of planets that you don't even have a number to describe how many there are.

#### Civilization Development

"Tersies." Seeding planets. The "prometheus problem." Coexistence with dinosaurs??

#### Harvest

Analogies to our treatment of animals. It can still be "affecting."

#### Genetic Modification

Cyberpunk. CRISPR. Animal trait splicing. Channing Tatum with his mouth open.

#### Space Feudalism

Lots -isms and -igarchies and whatnot. An economy sort-of based on time reminds us of Andrew Niccol's _In Time_.

#### Nihilism

What would you do if you owned whole planets?
