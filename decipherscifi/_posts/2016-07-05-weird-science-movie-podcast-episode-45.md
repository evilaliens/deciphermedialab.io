---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-07-05 07:40:39+00:00
description: 'With Weird Science we realize we are nerds and geeks, find discomfort in teenage fantasies, talk about Frankenstein, science magic, objectification, more'
layout: podcast_post
permalink: /decipherscifi/weird-science-movie-podcast-episode-45
redirect_from:
  - /decipherscifi/45
slug: weird-science-movie-podcast-episode-45
title: 'Weird Science: science, magic, and moral development in adolescence' 
tags:
  - science fiction
  - science
  - hacking
  - nerds
  - geeks
  - teenagers
  - 80s
  - fantasy

# Episode particulars
image:
  path: assets/imgs/media/movies/weird_science_1985.jpg
  alt: Two dudes being surprised by an attractive conjured woman Weird Science movie backdrop
links:
  - text: Frankenstein w/ Joe Ruppel
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/frankenstein-joe-ruppel-movie-podcast-episode-29/'
  - text: Made of the Future/Weird Science
    urls:
      - text: Mars Will Send no More
        url: 'https://marswillsendnomore.wordpress.com/2011/06/07/origins-of-omac-made-of-the-future-ec-comics/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/weird-science/id280496203?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Weird-Science-Anthony-Michael-Hall/dp/B009CGGA9I'
  title: Weird Science
  type: movie
  year: 1985
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/weird_science_1985.jpg
  number: 45
  guid: 'http://www.decipherscifi.com/?p=877'
  media:
    audio:
      audio/mpeg:
        content_length: 18419165
        duration: '30:41'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Weird_Science_decipherSciFi.mp3'
---
#### John Hughes

And the Brat Pack. Uncle Buck and Breakfast Club.

#### Smagic

Science so unscientific, it is magic: smagic. Callbacks to pygmalion and Frankenstein.

#### Sexism/Objectification

Boy howdy. Literally using a barbie to create a living sex object. Consent. Creating sex bots. The 1950s [comic](https://marswillsendnomore.wordpress.com/2011/06/07/origins-of-omac-made-of-the-future-ec-comics/) on which the movie was based.

#### Geekery

What are they? what are we? Geeks? Nerds? Both?

#### Growing Up

Adolescence _sucks_. Being a sexually frustrated teenage boy. Fantasies and ethical concerns.
