---
# General post frontmatter
categories:
  - episode
  - state of the union
date: 2018-11-08 07:45:59+00:00
description: "In which we look at your survey responses and reveal where we’re headed in the coming year"
layout: podcast_post
permalink: /decipherscifi/decipher-scifi-state-of-the-union-2018
redirect_from:
  - /decipherscifi/sotu2018
slug: decipher-scifi-state-of-the-union-2018
title: Decipher SciFi State of the Union 2018
tags:
  - meta
  - miscellany

# Episode particulars
image:
  path: assets/imgs/media/misc/state_of_the_union_2018.jpg
  alt: President Dwayne Elizondo Mountain Dew Herbert Camacho presiding over out meeting
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/state_of_the_union_2018.jpg
  guid: 'https://decipherscifi.com/?p=8023'
  media:
    audio:
      audio/mpeg:
        content_length: 24184303
        duration: '28:46'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/State_of_the_Union_2018_Decipher_Media.mp3'
---
In which we look at your survey responses and reveal where we're headed in the coming year
 	
* Listening habits
* RPG experience
* Nerd gatherings and meeting listeners irl
* Creator support
* [LSG Media](https://www.libertystreetgeek.net/) appreciation party
* Guest co-hosts
* Suggestion box
* Future expansion recommendations (Decipher History!?)
