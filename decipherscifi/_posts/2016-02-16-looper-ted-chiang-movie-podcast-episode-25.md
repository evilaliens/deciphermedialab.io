---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-02-16 07:42:35+00:00
description: 'Guest co-host Ted Chiang helps us figure out what was going on in Looper. Time travel, revenge, redemption, motherhood and lots of diagrams with straws.'
layout: podcast_post
permalink: /decipherscifi/looper-ted-chiang-movie-podcast-episode-25
redirect_from:
  - /decipherscifi/25
slug: looper-ted-chiang-movie-podcast-episode-25
title: 'Looper: Ted Chiang on causal loops, mama bears, and time travel'
tags:
  - science fiction
  - science
  - time travel
  - paradoxes
  - time machine
  - future
  - noir
  - dystopia

# Episode particulars
image:
  path: assets/imgs/media/movies/looper_2012.jpg
  alt: Looper Bruce Willis movie backdrop
links:
  - text: Stories of Your Life and Others by Ted Chiang
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/stories-your-life-others/id1061453391?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Stories-Your-Life-Others-Chiang-ebook/dp/B0048EKOP0'
  - text: There is No Loop in Looper by Prof Hames Van Cleve
    urls:
      - text: A Time Travel Website
        url: 'http://timetravelphilosophy.net/topics/no-loop-in-looper/'
  - text: Director Rian Johnson Talks Plot Holes and Burning Questions
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=wA2Y6WUqaY8'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/looper/id575490887?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Looper-Bruce-Willis/dp/B00A9X9TIW'
  title: Looper
  type: movie
  year: 2012
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: liam_ginty
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 25
  guid: 'http://www.decipherscifi.com/?p=521'
  media:
    audio:
      audio/mpeg:
        content_length: 24227760
        duration: '40:22'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Looper_Ted_Chiang_decipherSciFi.mp3'
---
#### Ted Chiang, Guest Co-Host

Thanks to Ted for coming on and helping us figure out what's going on in Looper. Rather than an interview, Ted is a fully-fledged guest co-host with all the power that entails. :)

#### Plot Outline

This gets a little convoluted, because the movie is not incredibly worried about logical consistency.

#### Looper Isn't Actually About Loops

Despite appearances and the language in the film about loops and looping, we decide that either there are no paradoxical loops or the movie just doesn't make a lot of sense in this regard. Or maybe both things. Probably both.

#### Diagramming Timelnes

We straighten out how many Bruces/JGLs there have been and how they all lived out their lives. It might be more than you think! Ted has to reign us in so we don't spend the whole show making diagrams with straws. :)

{% responsive_image_block %}
  path: 'assets/imgs/misc/looper-whiteboard.jpg'
  alt: 'Our Looper timelines whiteboard diagram'
  caption: 'no straws, but we spent some time with a whiteboard. too much time. here is a sampling'
  attr_text: 'CC-BY-NC'
{% endresponsive_image_block %}

#### Motherhood

Actually it turns out this movie is about motherhood (an uncommon topic for science fiction) and the importance of a mother in keeping boys from growing up "lost." We needed a professional writer on the show to get us to shut up about time travel long enough to figure out the point of the movie. Thanks, Ted.

#### Free Will

Comparing and contrasting the appearance/reality of free will in this film and other examples (Twelve Monkies, Back to the Future).

#### Rewriting Looper

Ted tries to rewrite the movie but realizes how important all of the logically difficult plot devices are to the story and characters.

> _The first half of the film, where criminal organizations are sending people back in time to be killed - that made no sense at all! And that really bugged me.
>
> But I quite liked the second half of the film, which was about the importance of motherhood and the possibility of breaking the cycle of violence.
>
> I wanted there to be a way to fix the first half of the film, but keep the second half.
>
> So I, in my mind, tried to rewrite the film..._
> -Ted Chiang

And keep an eye out for the upcoming movie "Arrival" based on Ted's story "Story of Your Life" (with Jeremy Renner and Amy Adams) probably some time this year. Should be really good!
