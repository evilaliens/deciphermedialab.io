---
# General post frontmatter
categories:
  - episode
  - bonus
date: 2019-10-01 06:19:55+00:00
layout: podcast_post
permalink: /decipherscifi/on-decipher-history-gangs-of-new-york
redirect_from:
slug: on-decipher-history-gangs-of-new-york
title: 'On Decipher History: Gangs of New York'
tags:
  - promo

# Episode particulars
image:
  path: assets/imgs/media/movies/gangs_of_new_york_2002.decipherhistory_promo.jpg
  alt: Bill the Butcher and the History Brain
links:
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/gangs_of_new_york_2002.decipherhistory_promo.jpg
  number: 
  guid: 'https://decipherscifi.com/?p=10807'
  media:
    audio:
      audio/mpeg:
        content_length: 1796509
        duration: '02:02'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/gangs_of_new_york_dsf_promo.mp3'

---
The taste of another episode of Decipher History! Featuring gangs fights in 19th century New York, etc.

On Decipher History: [Gangs of New York: history of The Five Points, immigration, and archaic criminal lingo]({% link decipherhistory/_posts/2019-10-01-gangs-of-new-york-episode-005.md %})
