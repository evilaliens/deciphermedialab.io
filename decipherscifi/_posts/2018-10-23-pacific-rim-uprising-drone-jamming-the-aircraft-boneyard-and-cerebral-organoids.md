---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-10-23 07:45:46+00:00
description: 'Big robot science. Brains in jars. Relative abundance of "rare earth" elements. Underwater volcanoes. Blood rockets and colonizing the outer solar system.'
layout: podcast_post
permalink: /decipherscifi/pacific-rim-uprising-drone-jamming-the-aircraft-boneyard-and-cerebral-organoids
redirect_from:
  - /decipherscifi/164
slug: pacific-rim-uprising-drone-jamming-the-aircraft-boneyard-and-cerebral-organoids
title: 'Pacific Rim Uprising: drone jamming, the aircraft boneyard, and cerebral organoids'
tags:
  - aliens
  - geology
  - nioengineering
  - robots
  - science
  - science fiction
  - transhumanism

# Episode particulars
image:
  path: assets/imgs/media/movies/pacific_rim_uprising_2018.jpg
  alt: "Robots and robot-riders, but some different ones this time, standing dramatically. Pacific Rim Uprising movie backdrop."
links:
  - text: 'Pacific Rim: square cube law, giant death robot, and Earth terraforming w/ Stephen Granade'
    urls:
      - text: Decipher SciFi
        url: 'https://decipherscifi.com/134'
  - text: Atlas Parkour
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=LikxFZZO2sk'
  - text: 'Rayguns and Robots (Curly Phil)'
    urls:
      - text: Instagram
        url: 'https://www.instagram.com/raygunsandrobots/'
  - text: Batch 25 Comics (Andy P)
    urls:
      - text: Instagram
        url: 'https://www.instagram.com/batch25comics/'
media:
  links:
    - text: iTunes
      url: 'https://www.instagram.com/batch25comics/'
    - text: Amazon
      url: 'https://www.amazon.com/Pacific-Rim-Uprising-John-Boyega/dp/B07BKRHVYK'
  title: Pacific Rim Uprising
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/pacific_rim_uprising_2018.jpg
  number: 164
  guid: 'https://decipherscifi.com/?p=7922'
  media:
    audio:
      audio/mpeg:
        content_length: 34837993
        duration: '41:27'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Pacific_Rim_Uprising_--_--_Decipher_SciFi.mp3'
---
#### Robot police

Keeping the peace when independent operations can make their own giant robots. Staying somewhat prepared for returning existential threats.

#### Jaeger graveyard

Stowing expensive purpose-built machinery. [Aircraft graveyards](https://en.wikipedia.org/wiki/Aircraft_boneyard). The economics of storing and securing out-of-production technology for later use.

#### Robotics

Avoiding trouble with the [square-cube law](https://en.wikipedia.org/wiki/Square%E2%80%93cube_law) by building smaller robots. Boston Dynamics and the inevitable impression of the coming robot apocalypse. [Parkour Atlas](https://www.youtube.com/watch?v=LikxFZZO2sk). Robot kumite.

#### Drones

Securing mobile war machines. EMPs. Resistance to signal-jamming. Broad attack surfaces.

#### Brain in a jar

IRL progress towards a brain in a jar: human brain organoids [implanted on a mouse brain](https://www.the-scientist.com/daily-news/human-brain-organoids-thrive-in-mouse-brains-30242). Sensory apparatus and feeling pain. Getting (ethically) hairy.

#### Rare earth elements

Actually: not so rare! But a pain in the ass to extract. The Japanese rare earth ocean-bottom treasure trove and how it will be [difficult to access](https://earther.gizmodo.com/dont-get-too-excited-over-japans-new-semi-infinite-rare-1825185977). Mining rare earth elements from spaaaaaaaace and how we probably need a new colloquial name for these materials. Underwater volcanoes.

#### Jaeger blood rocket fuel

Magical Jaeger blood rocker fuel. Gravity gun space race. Keeping Earth as a farm for kaiju blood, fueling our expansion into the entire solar system. Throwing rocks at things for fun and profit.
