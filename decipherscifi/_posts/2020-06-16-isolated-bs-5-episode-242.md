---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - conversation
date: 2020-06-16T02:22:44-04:00
description: 'Raspberry Pis for myriad computing projects. Gaming. Our quickly-perished webapp. Robot vacuums are the best thing. Plastic rain. Etc.'
layout: podcast_post
redirect_from:
  - /decipherscifi/242
slug: tasty-pi-robot-vacuums-our-saddest-failure
title: 'Isolated BS: tasty Pi, robot vacuums, and our saddest failure'
tags:
  - computing
  - parenting
  - plastic
  - robots
  - science
  - science fiction
  - youtube

# Episode particulars
image:
  path: assets/imgs/media/misc/isolated_bs_5_memeseeks.jpg
  alt: 'Decipher SciFi hosts Christopher and Colbert as Meeseeks'
links:
  - text: 'Old Glory Robot Insurance by SNL'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=g4Gh_IcK8UM'
  - text: 'Frinkiac - The Simpsons Meme Engine'
    urls:
      - text: Frinkiac.com
        url: 'https://frinkiac.com/'
  - text: 'Master of All Science - The Rick and Morty Meme Engine'
    urls:
      - text: 'MasterOfAllScience.com'
        url: 'https://masterofallscience.com/'
  - text: 'Plastic Rain Is the New Acid Rain'
    urls:
      - text: Wired
        url: 'https://www.wired.com/story/plastic-rain-is-the-new-acid-rain/'
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/isolated_bs_5_memeseeks.jpg
  number: 242
  guid: tasty-pi-robot-vacuums-our-saddest-failure
  media:
    audio:
      audio/mpeg:
        content_length: 31725945
        duration: '37:43'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/isolated_bs_5.final.mp3'

---
# Computing

Not the sweet and sour delicious dessert, but the computing devices. [Kodi](https://www.raspberrypi.org/documentation/usage/kodi/), [Smart Mirrors](https://magicmirror.builders/), [retro arcade machines](https://retropie.org.uk/), and low-power cheap servers in the closet. Apple moving to [ARM](https://www.quora.com/What-is-an-ARM-processor). The next generation of game consoles. SSDs.

{% responsive_image_block %}
  path: 'assets/imgs/misc/raspberry_pi.jpg'
  alt: 'Raspberry Pi 4 model B'
  caption: 'Raspberry Pi4 model B'
  attr_text: 'Michael  Henzler / Wikimedia Commons / CC-BY-SA 4.0'
  attr_url: 'https://commons.wikimedia.org/wiki/File:Raspberry_Pi_4_Model_B_-_Side.jpg'
{% endresponsive_image_block %}


# YouTube

Chapter-markers. Auto-generated subtitles and "free" transcription.

# Memeseeks

The sad story of [The Memeseeks Box]({% link blog/_posts/2017-07-25-the-memeseeks-box.md %}). Learning how to frame what may be your "failures."

{% responsive_image_block %}
  path: 'assets/imgs/misc/us-memeseeks.png'
  alt: 'The original Memeseeks Box logo art'
  caption: 'The original Memeseeks Box logo art'
  attr_text: 'Nicholas Lowe, used with permission'
  attr_url: 'https://www.instagram.com/gentlemanlowe'
{% endresponsive_image_block %}

# Robot vacuums

Both a cleaning tool and a defense against madness in the presence of eldritch horrors. "[Because they're made of metal, and robots are strong](https://www.youtube.com/watch?v=g4Gh_IcK8UM)." Using the Roomba as a parenting tool. [Deebot Ozmo](https://www.ecovacs.com/us/deebot-robotic-vacuum-cleaner/DEEBOT-OZMO-930).

# Plastic Rain

It's [everywhere](https://www.wired.com/story/plastic-rain-is-the-new-acid-rain/). Referring to our [Wall-E episode]({% link decipherscifi/_posts/2018-08-07-wall-e-biodegradable-plastics-waste-management-and-space-cattle.md %}) with heaping portions of appreciation and concern for plastics.

# Big tech

Withholding the sale of facial recognition tech from police. Bezos the nigh-trillionaire being able to afford his own ISS.


<sub><sup>"Cartoon Room House" used in episode image background, by [mixrobastudio/pixabay/CC-0](https://pixabay.com/illustrations/cartoon-room-house-interior-2550689/)</sup></sub>
