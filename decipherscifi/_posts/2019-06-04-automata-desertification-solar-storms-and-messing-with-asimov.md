---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-06-04 07:45:20+00:00
description: "Reformulating Asimov's laws. Ancient automata. Automata vs robots. Determining what makes us human. Civilizational collapse and desertification."
layout: podcast_post
permalink: /decipherscifi/automata-desertification-solar-storms-and-messing-with-asimov
redirect_from:
  - /decipherscifi/196
slug: automata-desertification-solar-storms-and-messing-with-asimov
title: 'Automata: desertification, solar storms, and messing with Asimov'
tags:
  - artificial intelligence
  - asimov
  - automata
  - cyborg
  - desertification
  - laws of robotics
  - science
  - science fiction
  - solar storms

# Episode particulars
image:
  path: assets/imgs/media/movies/automata_2014.jpg
  alt: 'Pensive post-apocalyptic Antonio Banderes and some robots'
links:
  - text: 'Automata by In Our Time'
    urls:
      - text: BBC
        url: 'https://www.bbc.co.uk/programmes/b0bk1c4d'
  - text: 'Ghost in the Machine by Radio Lab'
    urls:
      - text: WNYC
        url: 'https://www.wnycstudios.org/story/218955-ghost-machine'
media:
  links:
    - text: iTunes
      url: 'https://itunes.apple.com/ca/movie/automata/id975391800'
    - text: Amazon
      url: 'https://www.amazon.com/Automata-Antonio-Banderas/dp/B00OCEDVW4'
  title: Automata
  type: movie
  year: 2014
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/automata_2014.jpg
  number: 196
  guid: 'https://decipherscifi.com/?p=10423'
  media:
    audio:
      audio/mpeg:
        content_length: 25744124
        duration: '30:38'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/automata_--_--_decipher_scifi.mp3'

---
#### Robot laws

Asimov's laws as a storytelling device and what you get when you alter the formula. "Humans are the true robots."

#### Automata

The difference between "automata" and "robots." Ancient Greek, Arab, and medieval church automata. Automata and robots making humans realize we need to explain what makes us special. Realizing that, on the whole, we probably aren't really. Cartesian dualism as a stop-gap measure. Moral progress. Appreciating that we stopped seeing other animals as automatons and vivisecting them.

Modern automata

NASA getting into the automata game for difficult environments. The [Automaton Rover for Extreme Environments](https://en.wikipedia.org/wiki/Automaton_Rover_for_Extreme_Environments). Mixing electronic and (mostly) mechanical components for robustness against horrible, deadly, no good, very bad atmospheres. Sending data back home from limited electronic or even completely mechanical systems.

#### Solar storms

Various phenomena caused by disturbances in the sun. [The Carrington Event](https://en.wikipedia.org/wiki/Solar_storm_of_1859). Imagining the losses in the trillions from a Carrington-strength event in the modern day. Civilizational collapse with worldwide disturbance/destruction of telecommunications infrastructure.

World population

Realizing that several billion humans on Earth is actually like a _real real lot_ of people. Very many. So many that even a 97% reduction leaves millions. Oy.

#### Desertification

Defining deserts by rainfall. Defining desertification by a land's inability to hold water.
