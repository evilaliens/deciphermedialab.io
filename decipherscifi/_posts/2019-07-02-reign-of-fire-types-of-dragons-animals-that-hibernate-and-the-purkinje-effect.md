---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-07-02 07:45:13+00:00
description: 'Crazy American yee-haw. Shifting specral sensitivities at twilight. Setting bars for Dragon VFX. Practical fire. Napalm. Hibernation and other dormancies. Etc.'
layout: podcast_post
permalink: /decipherscifi/reign-of-fire-types-of-dragons-animals-that-hibernate-and-the-purkinje-effect
redirect_from:
  - /decipherscifi/200
slug: reign-of-fire-types-of-dragons-animals-that-hibernate-and-the-purkinje-effect
title: 'Reign of Fire: types of dragons, animals that hibernate, and the Purkinje effect'
tags:
  - dragons
  - hibernation
  - medieval weaponry
  - murrica
  - optical illusions
  - science
  - science fiction
  - visual processing
  - weapons

# Episode particulars
image:
  path: assets/imgs/media/movies/reign_of_fire_2002.jpg
  alt: 'Huge dragona straight up razing London. Also, helicopters.'
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/reign-of-fire/id350098681?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Reign-Fire-Matthew-McConaughey/dp/B004LGL3KA/ref=sr_1_2?keywords=reign+of+fire&qid=1581454621&sr=8-2'
  title: Reign of Fire
  type: movie
  year: 2002
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/reign_of_fire_2002.jpg
  number: 200
  guid: 'https://decipherscifi.com/?p=10455'
  media:
    audio:
      audio/mpeg:
        content_length: 25530563
        duration: '30:22'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/reign_of_fire_--_--_decipher_scifi.mp3'

---
#### 'Murrica

Jacked Christian Bale, Scottish Gerard Butler, and brash American hero Matthew McConaughey. Fighting a dragon _with an axe_. Appreciating the brash American caricature.

#### Dragons

Dragon VFX. Drawing the line from here to [Game of Thrones]({% link decipherscifi/_posts/2019-05-14-game-of-thrones-medieval-weaponry-variable-seasons-and-final-season-anxiety.md %}). Animals that use a spray attack (or defense). Skunks, ants, horned lizard, snakes, even bombardier beetles and their boiling butt juices. Dragon classification (vs wyverns, etc).

#### Fire etc

Actual napalm is a hell of a thing. Remember the Anarchist's Cookbook and how innacurate and unsafe everything was. Practical fire FX in the movie and how well it holds up. _Hundreds_ of gallons of propane per dragon blast. Burninating all the food on a planet.

#### "Hibernation"

Hibernation vs other states. Dormancy Torpor. Estivation. Endotherm and ectotherm dormancy. Classical and modern ideas of what counts as "hibernation." _EXTREEEEEEME_ dormancies. Water bears/tardigrades.

#### Eyesight

Vision difficulties at twilight. The [Purkinje effect](https://en.wikipedia.org/wiki/Purkinje_effect). Shifting spectral sensitivities based on brightness. Progression through photopic, mesopic, and scotopic vision. Appreciating the idiosyncratic nature of our vision and understanding why emulating what we see is so difficult with a camera lens.

> Twilight, my only weakness. How did you know?
> 
> -A Dragon

#### Castles

Buying up castles in the Romanian countryside. Castles come in different sizes but no matter how you measure that's a really big dragon.
