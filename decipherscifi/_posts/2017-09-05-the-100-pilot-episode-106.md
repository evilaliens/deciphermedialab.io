---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - tv
date: 2017-09-05 07:45:43+00:00
description: 'The 100 and the value of frontloading worldbuilding. Impromptu space stations. Resource mining in space. Nuclear war. Radiation cookies. Bioluminescence.'
layout: podcast_post
permalink: /decipherscifi/the-100-pilot-episode-106
redirect_from:
  - /decipherscifi/106
slug: the-100-pilot-episode-106
title: 'The 100 (pilot): space station maintenance, post-nuclear Earth, and grumpy old men'
tags:
  - apocalypse
  - bioluminescence
  - nuclear annihilation
  - rules
  - science
  - science fiction
  - space mining
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/tv/the_100_2014.jpg
  alt: Pretty, young people stading around on a planet I guess, and some more of them are falling
links:
  - text: How Damaging is Radiation? by Veritasium
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=wQmnztyXwVA'
  - text: Cruel Bombs by Vsauce
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=SHZAaGidUbg'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/tv-season/pilot/id824282666?i=843769594&mt=4&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/gp/video/detail/B00IUX4VQ8'
    - text: 'Netflix'
    - url: 'https://www.netflix.com/title/70283264'
  title: The 100
  type: tv
  year: 2014
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/the_100_2014.jpg
  number: 106
  guid: 'https://decipherscifi.com/?p=3177'
  media:
    audio:
      audio/mpeg:
        content_length: 34746368
        duration: '41:21'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_100_--_--_decipherSciFi.mp3'
---
#### The particular value of pilot episodes

We did this [once before]({% link decipherscifi/_posts/2017-05-02-oasis-episode-88.md %}) and had a really good time. Covering pilot episodes specifically and exclusively to get at a high-concentration of scifi ideas and worldbuilding. And now we're at it again with The 100!

#### Free-floating constructed space settlements

And their value versus planetary settlements or settlements otherwise on existing bodies in the solar system. Time to develop space settlements and what a dozen individual, national settlements suggest about the state of industry in space before the apocalypse in the show. [Inflatables](https://arstechnica.com/science/2017/05/the-future-nasa-uses-3d-printing-to-test-its-new-expandable-habitat/) in space settlement.

#### Space resources

Both before and after earth enters nuclear winter. Water, air, hydrocarbons and where to find them.

#### Rules and enforcement under extreme circumstances

Freedom vs keeping everyone from dying. Reproduction policies. Simulations for generation ships - maintaining sufficient genetic diversity through selection and control of reproduction.

#### Aging equipment in spaaaaaaace

We hit on this once before in our [Seveneves]({% link decipherscifi/_posts/2016-08-09-seveneves-feat-daniel-mann-software-pro-episode-50.md %}) episode. Using landing vessels that have been sitting in orbit for a century. Don't keep your equipment next to your bananas.

#### Nuclear apocalypse

The [7:10 rule](https://emilms.fema.gov/IS3/FEMA_IS/is03/REM0504050.htm). Human civilization vs survival of the human animal. Nuclear fallout and nuclear winter. Types of radiation and [radiation cookie puzzles](https://www.reddit.com/r/askscience/comments/1hbd86/you_have_three_cookies_one_emits_alpha_radiation/).

#### Post-Nuclear Earth

Ancient Roman artillery (ps we said "scorpio" but meant "ballista," whoops). Bioluminescence in a forest.

{% responsive_image path: assets/imgs/misc/roman_ballista.jpg alt: "Roman Ballista being loaded" credit_author: " 	Pearson Scott Foresman" credit_url: "https://commons.wikimedia.org/wiki/File:Ballista_(PSF)_vector.svg" %}
