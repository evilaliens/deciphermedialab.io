---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2016-04-19 07:44:21+00:00
description: 'With Fifth Element on the podcast we talk broken window fallacy, death cults, space wizards, FTL comms, chamber design, space orcs and space turtles, more.'
layout: podcast_post
permalink: /decipherscifi/fifth-element-movie-podcast-episode-34
redirect_from:
  - /decipherscifi/34
slug: fifth-element-movie-podcast-episode-34
title: 'The Fifth Element: space turtles, magic stones, and LeeLoo Dallas multipass'
tags:
  - science fiction
  - science
  - space
  - space opera
  - spy
  - dystopia

# Episode particulars
image:
  path: assets/imgs/media/movies/the_fifth_element_1997.jpg
  alt: The Fifth Element movie backdrop
links:
  - text: The Divine Language
    url: 'http://www.divinelanguage.com/'
  - text: Affordances and Constraints in the Fifth Element
    urls:
      - text: Vimeo
        url: 'https://vimeo.com/67334858'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-fifth-element/id263331863?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Fifth-Element-Bruce-Willis/dp/B000S6BKSC'
  title: The Fifth Element
  type: movie
  year: 1997
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_fifth_element_1997.jpg
  number: 34
  guid: 'http://www.decipherscifi.com/?p=725'
  media:
    audio:
      audio/mpeg:
        content_length: 24159750
        duration: '40:15'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Fifth_Element_decipherSciFi.mp3'
---
#### The Film

90s, Leeloo's thermal tape, awesome hair.

#### Alien Species

Space turtles, space orcs, space squids, space humans. Archeology and ancient mythology. Chamber design.

#### Malevolent Evil

The possible nature of the evil space-mass. Zorg's motivations. Broken window fallacy.

#### 200 Billion Citizens

Overpopulation. Governance over interstellar distances. Real-time FTL space comms. Space wizards. Space borders.

#### Building Leeloo

3d printing skeletons. Genetic design and genetic data storage.

#### The Stones

Sufficiently advanced technology indistinguishable from magic. Good and bad places to smuggle 20 pounds of stone. "Space opera." Impossible series of vocal notes. Saved by love, or something.
