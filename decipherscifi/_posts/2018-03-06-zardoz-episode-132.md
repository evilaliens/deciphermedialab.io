---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-03-06 07:45:22+00:00
description: "Zardoz! Man oh man, this movie is weird. The 70s! Sean Connery's fabulous wardrobe. Meat-only diets, evolutionary humanity, and uh relationships with gorillas. Cognition into metacognition, and theory of mind in other animals. Transhumanism and artificial intelligence. Boring immortals getting bored. More!"
layout: podcast_post
permalink: /decipherscifi/zardoz-episode-132
redirect_from:
  - /decipherscifi/132
slug: zardoz-episode-132
title: 'Zardoz: eating only meat, awkward sexual experiments, and dealing with immortality'
tags:
  - carnivores
  - evolution
  - human evolution
  - humans
  - immortality
  - metacognition
  - science
  - science fiction
  - sexual reproduction
  - transhumanism

# Episode particulars
image:
  path: assets/imgs/media/movies/zardoz_1974.jpg
  alt: Shaun Connery in an amazing red bandolier/codpiece get-up, brandishing a pistol
links:
  - text: Facts about Human Evolution by SciShow
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=ROwKq3kxPEA'
  - text: 'Rick & Morty - Raising Gazorpazorp'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/raising-gazorpazorp/id741096885?i=838311624&mt=4&at=1001l7hP'
      - text: YouTube
        url: 'https://www.youtube.com/results?search_query=raising+gazorpazorp'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/zardoz/id314918279?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Zardoz-Sean-Connery/dp/B0029M5XMC'
  title: Zardoz
  type: movie
  year: 1974
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/zardoz_1974.jpg
  number: 132
  guid: 'https://decipherscifi.com/?p=5307'
  media:
    audio:
      audio/mpeg:
        content_length: 32762489
        duration: '38:59'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Zardoz_--_--_Decipher_SciFi.mp3'
---
#### Weeeeeeeeeird stuff

Pulling a James Cameron. Sean Connery is down for anything. Weeeeeird stuff. A list of some of our favorite things

* Zardoz' mustache
* Zardoz's speeches both and and out of stone head
* Sean Connery's everything
* Psychic attacks

#### Bullets in the air

One of the worse ways to celebrate. Coins off of a skyscraper. Mass and volume and drag. Bullet lethality reductions with different trajectories.

#### Eating _only_ meat

The benefits and drawbacks. The inuit diet. Surprisingly-high carbohydrate intake when all your meat is raw. Micronutrients via organ meats. Food preservation through controlled rot. Using fats and proteins to energy.

#### Evolutionary humanity

Other varieties of humanity that coexisted with homo sapiens. The study of evolution. Fossil evidence and developments in mitochondrial DNA analysis. The multiregional hypothesis -> the out of Africa hypothesis.

#### Human evolutionary promiscuity

Neanderthals, Denisovans, and uh.... gorillas? The evolutionary relationship between gorilla and human pubic lice.

#### Metacognition

Don't forget other animals are smart too! Humans are still way better though.

#### The boner test

Awkward science. Would you pass?

#### Transhumanism in Zardoz

The nature of power the "Eternals" have. Evolutionary timescales vs the application of technology. AI control.

#### Dealing with immortality

Boredom is for boring people. The value of freedom of will when you can't die.
