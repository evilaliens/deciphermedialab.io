---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-03-07 07:40:49+00:00
description: 'Chris Noessel helps us process this scifi teen romance. Settling Mars. Robotic base-building. The effects of microgravity. Real-time interplanetary comms.'
layout: podcast_post
permalink: /decipherscifi/the-space-between-us-feat-chris-noessel-ui-expert-episode-80
redirect_from:
  - /decipherscifi/80
slug: the-space-between-us-feat-chris-noessel-ui-expert-episode-80
title: 'The Space Between Us: lightspeed commuunications and relative gravity  w/ Chris Noessel'
tags:
  - science fiction
  - science
  - mars
  - space settlement
  - space
  - weightlessness

# Episode particulars
image:
  path: assets/imgs/media/movies/the_space_between_us_2017.jpg
  alt: Asa Butterfield standing in a spacesuit in a field on Earth, Space Between Us movie backdrop
links:
  - text: Make it So by Chris Noessel
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/make-it-so/id564005891?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Make-So-Interaction-Lessons-Science-ebook/dp/B009EGPJCU'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-space-between-us/id1180853199?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Space-Between-Us-Gary-Oldman/dp/B01N4BO92P'
  title: The Space Between Us
  type: movie
  year: 2017
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: chris_noessel
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_space_between_us_2017.jpg
  number: 
  guid: 'http://www.decipherscifi.com/?p=1377'
  media:
    audio:
      audio/mpeg:
        content_length: 43204608
        duration: '51:25'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Space_Between_Us_--_Chris_Noessel_--_decipherSciFi.mp3'
---
#### How we wound up covering this teen romance

Chris was in town and at the last minute, we called together a Decipher SciFi meetup in NYC. Thanks for coming out, folks!

#### Settling Mars

Long and short term threats to humanity. Anthropocene.  Courage and adventure and inspiration.

{% youtube "https://www.youtube.com/watch?v=wwY4Q4q6Vdg" %}

#### Astronaut Quarantine

Frank Borman and [space vomit and space diarrhea](http://newatlas.com/apoll-8-45th-anniversary/29991/). Pre-flight and post-flight quarantine protocols.

#### Mars mission design

Rocket to orbit, dock with the larger ship/station, then on to Mars. Not entirely dissimilar to irl concepts. Finding energy on other bodies in space with lesser gravity wells. Public and private space industry partnership.

{% youtube "https://www.youtube.com/watch?v=H7Uyfqi_TE8" %}

#### Timelines and propulsion

Around nine months to mars (maybe). Ion drives? Mundane solid rocket? Apparent weightlessness?

{% responsive_image path: assets/imgs/misc/spacex_proposed_mars_transport.jpg alt: "SpaceX proposed Martian transport" %}

#### Robots

[Remote Agent](https://ti.arc.nasa.gov/tech/asr/planning-and-scheduling/remote-agent/history/) architecture. Robots prefabricating a Mars base before the humans arrive.

#### Weightlessness

Weightlessness as an environmental "stressor." Insemination in space. Fetal and childhood physical development.

#### Video Chat

Light speed communication speed limitations. Using [AI to simulate realtime conversation](https://scifiinterfaces.wordpress.com/2017/03/06/real-time-interplanetary-chat/). Translucent screens! ? A terrible design for the user, but a good design for storytelling in film. Also of some utility to teachers, who can then see what students are looking at. Parallax.

#### The hardship of existing on earth

What would a native-born Martian human experience coming to Earth? Higher gravity. Heart enlargement. Blood pressure. Thin bones, low muscle mass.
