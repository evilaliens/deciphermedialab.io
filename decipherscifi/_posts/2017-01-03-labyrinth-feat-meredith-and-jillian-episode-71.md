---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2017-01-03 08:02:36+00:00 
description: 'Two friends join us to cap off fantasy month - Labyrinth! Dabid Bowie, the rules of dreamworlds, science of eternal stench, dreaming, reverse minotaur, more' 
layout: podcast_post
permalink: /decipherscifi/labyrinth-feat-meredith-and-jillian-episode-71
redirect_from:
  - /decipherscifi/71
slug: labyrinth-feat-meredith-and-jillian-episode-71 
title: 'Labyrinth: Jim Henson, adolescence, and solving mazes w/ Jillian & Meredith' 
tags:
  - science fiction
  - science
  - fantasy
  - balls
  - smell
  - adolescence
  - dreams

# Episode particulars
image:
  path: assets/imgs/media/movies/labyrinth_1986.jpg
  alt: David Bowie holding his balls over all of the characters from Labyrinth, movie backdrop
links:
  - text: The Dark Crystal
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/the-dark-crystal/id469333419?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Dark-Crystal-Stephen-Garlick/dp/B005U9QJY2'
  - text: Brian Fraud's Art Books
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Good-Faeries-Bad-Brian-Froud/dp/0684847817'
  - text: The Goblins of Labyrinth by Brian Froud
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Goblins-Labyrinth-Brian-Froud/dp/0810970554'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/labyrinth/id553393377?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'http://www.amazon.com/Labyrinth-David-Bowie/dp/B005U9REFA'
  title: 
  type: movie
  year: 
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: jillian_meredith
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/labyrinth_1986.jpg
  number: 71
  guid: 'http://www.decipherscifi.com/?p=1242'
  media:
    audio:
      audio/mpeg:
        content_length: 22917018
        duration: '31:49'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Labyrinth_--_Meredith_and_Jillian_--_decipherSciFi.mp3'
---
#### Labyrinth production

The Dark Crystal -> Labyrinth -> Mirrormask : a spiritual trilogy. Jim Henson. [Beverly Crusher's choreography](http://mentalfloss.com/article/51882/gates-mcfadden-dr-crusher-choreographed-labyrinth).

{% responsive_image path: assets/imgs/misc/gates_mcfadden_choreographer.jpg alt: "Dr Crusher as choreographer for Labyrinth" %}


#### Fantasy Rules and Dream Worlds

Our difficulty with fantasy _again_, and further difficulty specific to dreamland stories. Nothing makes sense, cause and effect cannot be counted on, and essentially nothing is on the line. Yet Labyrinth is still delightful (because nostalgia!).

#### Relating to adolescents

Identifying with adults now more, since we are adults.

#### Dreams

Dreaming. Animal models. Rats and humans and dreaming during non-REM sleep.

#### The Greek Myth of the Labyrinth

Minotaur, reverse-minotaur. Plus, a succinct and accurate retelling of the story.

{% responsive_image path: assets/imgs/misc/reverse_minotaur.png alt: "Reverse minotaur: DAVE illustration" caption: "#science" %}

#### Maze-solving and cognition

Humans are okay. Rats are okay. Dogs, not so much. "Ecocentric cognitive mapping." FMRI and navigation in virtual reality.

#### The bog of eternal stench

Irreparable putrescence. Not something that is _on_ you, but something that infuses your very flesh to continue to be stinky. Or, magic.
