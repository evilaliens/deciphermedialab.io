---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - conversation
date: 2020-09-08T02:23:24-04:00
description:  'Josh , all over great podcasts recently. Teledildonics and Free Software 🤓🍆. Nuclear batteries and the Silurian hypothesis. The benefits of simulations, digital or otherwise. Tabletop RPGs and another future collaboration?'
layout: podcast_post
redirect_from:
  - /decipherscifi/251
slug: isolated-nuclear-bananas-unreal-simulations-buttplug-archaeology
title: 'Isolated BS: nuclear bananas, unreal simulations, and buttplug archaeology w/ Josh'
tags:
  - battery technology
  - free software
  - science
  - science fiction
  - silurian hypothesis
  - teledildonics
  - ttrpgs
  - video games

# Episode particulars
image:
  path: assets/imgs/media/misc/isolated_bs_10_nuclear_bananas.jpg
  alt: Cartoon characters Jar Brain and History Brain in a pile of bananas
links:
  - text: 'Saving Private Ryan w/ Josh'
    urls:
      - text: LSG Media
        url: 'https://libertystreetgeek.net/podcast-saving-private-ryan-1998/'
  - text: 'The X-Files Podcast /w Josh'
    urls:
      - text:  LSG Media
        url: 'https://libertystreetgeek.net/subscribe-xfiles/#'
  - text: 'Buttplug: an open-source standards and software project for controlling intimate hardware'
    urls:
      - text: Buttplug.io
        url: 'https://buttplug.io/'
  - text: 'Strapped Into A Sinking Helicopter (with U.S. Marines) by Smarter Every Day'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=-53kaP6dZeI'
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: josh_effengee
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/isolated_bs_10_nuclear_bananas.jpg
  number: 251
  guid: isolated-nuclear-bananas-unreal-simulations-buttplug-archaeology
  media:
    audio:
      audio/mpeg:
        content_length: 42823201
        duration: '50:56'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/bs10.final.mp3'

---
<sub><sup>
["Banana" image](http://clipart.nicubunu.ro/?gallery=fruits) used in featured image with our brains by nicubunu CC-0 
</sup></sub>

# Josh, recently

Made a strong showing on the recent [Saving Private Ryan](https://libertystreetgeek.net/podcast-saving-private-ryan-1998/) episode over at LSG Media. Real human stuff in between the dick and fart jokes.

# Teledildonics

Tales of coupled internet-connected sex toys are [way up](https://www.rollingstone.com/culture/culture-news/teledildonics-remote-sex-toy-sales-covid19-coronavirus-pandemic-975140/)! Hooray for free and open source teledildonics at [buttplug.io](https://buttplug.io).

# Nuclear things

Making long-lived diamond nuclear batteries from spent nuclear fuel. The [banana equivalent dose](https://en.wikipedia.org/wiki/Banana_equivalent_dose) of radiation. The trouble with battery-tech press releases. Eating enough grapes to give you superpowers. Buttplug archaeology. Tongue twisters. Forever-vibrating mummies and the [Silurian hypothesis](https://en.wikipedia.org/wiki/Silurian_hypothesis).
# Simulations

The major leap in tech in the new [MS Flight Simulator](https://en.wikipedia.org/wiki/Microsoft_Flight_Simulator). Taking the fun out of games by making them rigorous simulations. The value of process simulations when the real thing is expensive or dangerous. Going all Ender's Game on actual military simulations with [Close Combat](https://en.wikipedia.org/wiki/Close_Combat_(video_game)). Buying military might with lots of money.

# Tabletop RPGs

Missing the humans-at-a-table element during the pandemic. Appreciating how much fun we had with [Liam](%{ _people/liam_ginty.md %}) in our Mirror RPG playthrough (parts [I]({% link decipherscifi/_posts/2018-09-11-decipher-rpg-mirror-test-1.md %}), [II]({% link decipherscifi/_posts/2018-09-12-decipher-rpg-mirror-pilot-2.md %}), and [III]({% link decipherscifi/_posts/2018-09-19-decipher-rpg-mirror-pilot-3-the-finale.md %})). And catching Josh 100% on the hook to GM for the podcast again in the future! Hooray!
