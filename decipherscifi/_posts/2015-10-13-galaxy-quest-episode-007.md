---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2015-10-13 03:45:09+00:00'
description: "With Galaxy Quest we talk science fiction, Star Trek, cast analogs, fandom, Sam Rockwell, self-aware Star Trek trope comedy, aliens, the hero's journey, more"
layout: podcast_post
permalink: /decipherscifi/galaxy-quest-episode-007
redirect_from:
  - /decipherscifi/7
slug: galaxy-quest-episode-007
title: 'Galaxy Quest: the best Star Trek movie.'
tags:
  - science fiction
  - star trek
  - fandom
  - aliens
  - satire
  - alien morphology
  - linguistics

# Episode particulars
image:
  path: assets/imgs/media/movies/galaxy_quest_1999.jpg
  alt: 'Galaxy Quest backdrop'
links:
  - text: Artemis Spaceship Bridge Simulator
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/app/artemis-spaceship-bridge-simulator/id578372500?mt=8&at=1001l7hP'
      - text: Android Play Store
        url: 'https://play.google.com/store/apps/details?id=incandescent.game.artemis&hl=en'
  - text: 'Star Trek: The Next Generation'
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/star-trek-next-generation/id466420218?at=1001l7hP&mt=4'
      - text: Amazon
        url: 'https://www.amazon.com/The-Naked-Now/dp/B005HEQ0DA'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/galaxy-quest/id286147467?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Galaxy-Quest-Tim-Allen/dp/B005721W9O'
  title: Galaxy Quest
  type: movie
  year: 1999
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 7
  guid: 'http://www.decipherscifi.com/?p=257'
  media:
    audio:
      audio/mpeg:
        content_length: 26103465
        duration: '36:15'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Galaxy_Quest_decipherSciFi.mp3'
---
#### Star Trek

Christopher is a big fan. Has a revelation regarding this on the show.

#### Star Trek Fandom

Christopher might be a trekkie. The movie is a loving tribute. Star Trek cast reactions to Galaxy Quest.

#### Aliens in the movie

Tend to be humanoid. Luc Besson's The Fifth Element. Abundance, working together. Literalism in language like small human children.

#### Silly Star Trek Tropes

The movie uses them for great meta comedy. Transporter. Omega 13 device. Most important crew members go down to explore new worlds.

#### Artemis is Really Really Awesome

Here is a good example of the frivolity you could expect ([skip to3:38](https://www.youtube.com/watch?v=V9Q2X32hZNk?t=217) for gameplay):

{% youtube "https://www.youtube.com/watch?v=V9Q2X32hZNk" %}
