---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-02-14 07:45:52+00:00 
description: 'Tumbling Spaghetti-Monster aliens. Biological Von Neumann probes. Tom Cruise. Causal loops v time loops. Many-worlds interpretation. Precious bodily fluids.' 
layout: podcast_post
permalink: /decipherscifi/edge-of-tomorrow-live-die-repeat-episode-77
redirect_from:
  - /decipherscifi/77
slug: edge-of-tomorrow-live-die-repeat-episode-77 
title: 'Edge of Tomorrow (Live. Die. Repeat): alien tentacle morphology and biological Von Neumann probes' 
tags:
  - aliens
  - exoskeletons
  - groundhog day
  - immortality
  - insects
  - mechs
  - power armor
  - science
  - science fiction
  - time loops
  - time travel
  - video games
  - von neumann probes

# Episode particulars
image:
  path: assets/imgs/media/movies/edge_of_tomorrow_2014.jpg
  alt: Tom Cruise and Emily Blunt standing badass in power armor before a scene of destruction
links:
  - text: One-Minute Time Machine
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=vBkBS4O3yvY'
  - text: Groundhog Day
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/groundhog-day/id270870214?mt=6&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Groundhog-Day-Bill-Murray/dp/B00B99QTM4'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/live-die-repeat-edge-tomorrow/id878682651?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'http://www.amazon.com/Live-Die-Repeat-Edge-Tomorrow/dp/B00O8NPCA8'
  title: Edge of Tomorrow
  type: movie
  year: 2014
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/edge_of_tomorrow_2014.jpg
  number: 77
  guid: 'http://www.decipherscifi.com/?p=1341'
  media:
    audio:
      audio/mpeg:
        content_length: 41230866
        duration: '49:05'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Edge_of_Tomorrow_--_--_decipherSciFi.mp3'
---

{% responsive_image path: assets/imgs/selfies/edge_of_tomorrow.jpg alt: "Decipher SciFi co-hosts as Tom Cruise and Emily Blunt in Edge of Tomorrow" %}

#### Names and Names and Names

The light novel (All You Need is Kill) -> the movie (Edge of Tomorrow) -> the marketing (Live. Die. Repeat.)

#### Tom Cruise in a Video Game

Save points. Back in my day. Old-school save system where you have to go back to the beginning every time.

#### Biological Von Neumann Probes

The possibility of the cosmos awash in aggressive noodly alien murder machines. What are the odds they'd wind up at our doorstep?

#### Mimic Morphology

[Spaghetti Monster](https://en.wikipedia.org/wiki/Flying_Spaghetti_Monster) tumbleweeds! Bioluminescence. Alienness. Hybrid creatures. Hierarchy.

#### Starcraft

Zerg rush! [Starcraft was almost Warhammer](http://kotaku.com/5929161/how-warcraft-was-almost-a-warhammer-game-and-how-that-saved-wow).

#### Exoskeletons!

Reminds us of [Elysium]({% link decipherscifi/_posts/2015-12-22-elysium-movie-podcast-episode-17.md %})! Exosuit interfaces. Neural interface versus gestural. Safety mechanisms.

#### Time Loop Superpowers

What are thresholds of alpha blood exposure to contract timeloopism? Best method: bathing? in the butt? in the mouth? All of the above? Will the effect ever "run out?" Blood-bathing, Sookie Stackhouse style.

#### Time Loops!

Control of time loops. Like a garage door opener? TV remote? Is it time travel? [Many-worlds interpretation](https://en.wikipedia.org/wiki/Many-worlds_interpretation). Branching timelines! Terminator meatballs.

#### Who Wants to Live Forever, Sorta?

Chris does. Whatever it takes!
