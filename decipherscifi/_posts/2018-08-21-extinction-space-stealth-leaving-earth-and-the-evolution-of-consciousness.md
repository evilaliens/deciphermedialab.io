---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-08-21 07:45:11+00:00
description: 'Possible origins of consciousness, and possible avenues in the future. Evolution. Stealth in space, sneaking up on Earth. Space invasion and space genocide.'
layout: podcast_post
permalink: /decipherscifi/extinction-space-stealth-leaving-earth-and-the-evolution-of-consciousness
redirect_from:
  - /decipherscifi/156
slug: extinction-space-stealth-leaving-earth-and-the-evolution-of-consciousness
title: 'Extinction: space stealth, leaving Earth, and the evolution of consciousness'
tags:
  - androids
  - planetary warfare
  - robots
  - science
  - science fiction
  - space
  - space settlement
  - stealth

# Episode particulars
image:
  path: assets/imgs/media/movies/extinction_2018.jpg
  alt: A family hiding around a corner from an alien invader
links:
  - text: 'What is Consciousness? by Vsauce'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=qjfaoe847qQ'
  - text: 'Real Quick'
    urls:
      - text: LSG Media
        url: 'https://www.libertystreetgeek.net/subscribe-real-quick/'
media:
  links:
    - text: Netflix
      url: 'https://www.netflix.com/title/80236421'
  title: Extinction
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/extinction_2018.jpg
  number: 156
  guid: 'https://decipherscifi.com/?p=7465'
  media:
    audio:
      audio/mpeg:
        content_length: 28657416
        duration: '34:06'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Extinction_--_--_Decipher_SciFi.mp3'
---
#### Spoiler alerts

Special conditions requiring primary and secondary warnings.

#### Robots

How to tell if you are a robot? [New Study Finds Best Way To Determine If You Are Android Still Cutting Open Forearm To Reveal Circuitry Within](https://www.theonion.com/new-study-finds-best-way-to-determine-if-you-are-androi-1819580012). Self-delusion and _mass_ self-delusion.

#### Earthvacuation

Earth is really well-tuned for our needs (or is it the other way around?), and humans should try to maintain this as a base of operations if at all possible.

#### Surviving outside of Earth

Key resources on Mars and elsewhere in the solar system (outside of Earth). Iron for _bullets_. Rocket fuel.

#### Stealth in space

[There is no stealth in space](https://youtu.be/xvs_f5MwT04). Trying to hide from Earth (on Mars). Difficulties in hiding propulsion, light-blocking, and infrared emission. Hiding "in" the sun.

#### Attacking Earth

Shock and awe vs the impossibility of stealth. Why a nuclear EMP attack might not be a great idea. Throw a rock at it.

#### Evolving consciousness

Evolved consciousness in Earth life. The possibility of consciousness emerging in "artificial" life. Attention Schema Theory. Central processing of separate signal enhancement.
