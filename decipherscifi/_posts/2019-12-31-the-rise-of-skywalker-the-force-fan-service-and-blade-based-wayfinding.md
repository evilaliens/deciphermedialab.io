---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-12-31 06:00:48+00:00
description: 'Star Wars feelings. Franchise productions. Disney. Tech stagnation. Hiding planets and major centers of production.'
layout: podcast_post
permalink: /decipherscifi/the-rise-of-skywalker-the-force-fan-service-and-blade-based-wayfinding
redirect_from:
  - /decipherscifi/222
slug: the-rise-of-skywalker-the-force-fan-service-and-blade-based-wayfinding
title: 'The Rise of Skywalker: the force, fan service, and blade-based wayfinding'
tags:
  - artificial intelligence
  - digital rights management
  - planets
  - science
  - science fiction
  - space
  - star wars
  - technology

# Episode particulars
image:
  path: assets/imgs/media/movies/star_wars_the_rise_of_skywalker.jpg
  alt: 'Rey and Kylo having a colorful lightsaber face-off'
links:
  - text: 'The Saga - Star Wars with Tommy Wiseau - The full story'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=1M-_PX5WFBc'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/star-wars-the-rise-of-skywalker/id1490359713?mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Star-Wars-Skywalker-Carrie-Fisher/dp/B082VZL4T6'
  title: 'Star Wars: The Rise of Skywalker'
  type: movie
  year: 2019
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/star_wars_the_rise_of_skywalker.jpg
  number: 222
  guid: 'https://decipherscifi.com/?p=10895'
  media:
    audio:
      audio/mpeg:
        content_length: 38171515
        duration: '45:20'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/rise_of_skywalker.output.mp3'

---
#### Whoops, a movie review

So the movie didn't put in any cool science things and the only thing left to do is review it and talk about... feelings? Different viewing experiences between opening night and subsequent viewings.

#### Franchises

Fan service. Coherent extended story arcs. Reconsidering the prequel triloges in light of the new one. Indie IP ownership with one person in charge vs Disney. Realizing what a truly satisfying job disney did with Marvel. What made Star Wars different?

#### Oy vey

Bending over backwards to justify fantasy events, and how movies can deserve this treatment. Bringing in The Emperor at the end. Bringing back Leia with scraps of cut scenes and lots of over-the-shoulder conversation shots.

#### Exagol

How to hide a planet. How to hide a planet engaging in major empire-level engineering and construction projects. Hiding your Sith shipyard.

#### Tech

Technological stagnation. Miniaturization. Bad ways to encode spatial information. Knife-based wayfinding.

#### People

C3PO software limitations and an argument in favor of [Free Software](https://en.wikipedia.org/wiki/Free_software). Probably maybe lobotomizing your long-time companion without deliveration. The sith ecclesiastical cult language. Pointless characters. The writer [explains](https://www.hollywoodreporter.com/heat-vision/star-wars-writer-sets-record-straight-perceived-last-jedi-jabs-1265168) how Rose got mostly cut from the film.

#### The Force

Force diads. Beavis and Butthead [candy sale force-healing math](https://www.youtube.com/watch?v=7EzKOfqgOeM&feature=emb_title). Adam Driver doing all the acting.
