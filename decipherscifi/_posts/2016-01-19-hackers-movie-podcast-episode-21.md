---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: '2016-01-19 05:55:33+00:00'
description: 'With Hackers we talk 90s love, fashion, Kevin Mitnick, irl hacking, hacker taxonomy, doxxing, curiosity and legality, and criminal apprehension style.'
layout: podcast_post
permalink: /decipherscifi/hackers-movie-podcast-episode-21
redirect_from:
  - /decipherscifi/21
slug: hackers-movie-podcast-episode-21
title: 'Hackers: social engineering, Kevin Mitnick, and hacking before the internet w/ Brian Epstein'
tags:
  - science fiction
  - science
  - computing
  - hacking
  - computer virus
  - social engineering
  - 90s
  - kevin mitnick
  - doxxing

# Episode particulars
image:
  path: assets/imgs/media/movies/hackers_1995.jpg
  alt: Hackers Movie backdrop
links:
  - text: Ghost in the Wires - by Kevin Mitnick
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/ghost-in-the-wires/id395522097?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'http://www.amazon.com/Ghost-Wires-Adventures-Worlds-Wanted-ebook/dp/B0047Y0F0K'
  - text: Freedom Downtime
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/track-down/id729455769?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'http://www.amazon.com/Trackdown-Skeet-Ulrich/dp/B0002L57YQ'
  - text: Mr Robot
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/tv-season/mr.-robot-season-1/id993272008?at=1001l7hP&mt=4'
      - text: Amazon
        url: 'http://www.amazon.com/eps1-0_hellofriend-mov/dp/B00YBX8QEO'
  - text: Security Now
    url: 'https://twit.tv/shows/security-now'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/hackers/id333441649?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Hackers-Jonny-Lee-Miller/dp/B009950GAQ'
  title: Hackers
  type: movie
  year: 1995
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: brian_epstein
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 21
  guid: 'http://www.decipherscifi.com/?p=471'
  media:
    audio:
      audio/mpeg:
        content_length: 27464161
        duration: '38:08'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Hackers_decipherSciFi.mp3'
---
#### 90s

We're from there! We love the fashion. And the music!

{% youtube "https://www.youtube.com/watch?v=W5fM6WpC_nE" %}

#### Hacker Taxonomy

Hackers, crackers, script kiddies, etc.

#### Kevin Mitnick

Huge in the new at the time. Media coverage. The model for Dade's legal situation.

#### Hacking

Hacking, social engineering, dumpster diving.

#### Doxxing

Ruining Agent Gill. Reddit.
