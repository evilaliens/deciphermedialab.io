---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2016-01-05 06:45:40+00:00'
description: 'With Sunshine on the podcast we tackle tough decisions and pragmatism in space. How will the sun die? Space vacuum exposure. How much dust can one man make?'
layout: podcast_post
permalink: /decipherscifi/sunshine-movie-podcast-episode-19
redirect_from:
  - /decipherscifi/19
slug: sunshine-movie-podcast-episode-19
title: 'Sunshine: the lifecycle of a star, solar sails, and Captain America in space'
tags:
  - alex garland
  - apocalypse
  - astronaut
  - crew selection
  - faith
  - pragmatism
  - saving the world
  - science
  - science fiction
  - space
  - spaceship
  - star
  - stellar lifecycle
  - sun

# Episode particulars
image:
  path: assets/imgs/media/movies/sunshine_2007.jpg
  alt: Sunshine Danny Boyle Alex Garland movie backdrop
links:
  - text: An alleged intro to the Sunshine script by Alex Garland
    url: 'http://twilightvisions.com/sunshine/introduction.htm'
  - text: Solaris
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/solaris/id271628234?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'http://www.amazon.com/Solaris-George-Clooney/dp/B000SW2ER2'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/sunshine/id272508664?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Sunshine-Cillian-Murphy/dp/B000ZHS3E8'
  title: Sunshine
  type: movie
  year: 2007
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 19
  guid: 'http://www.decipherscifi.com/?p=412'
  media:
    audio: 
      audio/mpeg:
        content_length: 38690441
        duration: '53:43'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Sunshine_decipherSciFi.mp3'
---
#### Dying Sun

Sun science. Stages and timeline of sun death.

#### Spaceship

Solar radiation. Reflector shields. Ion drive. Magical gravity. Oxygen garden. Sun room.

#### The Crew

LOVE pragmatic Chris Evans. Our favorite person. Cillian is always great. Earth room holo tech. The different captains and their madness. Jumping through space! **PS**: we've covered this before, in [episode 1]({% link decipherscifi/_posts/2015-09-15-titan-ae-movie-podcast-episode-001.md %}).

#### Third Act

Thoughts on the third act juxtaposed with the first two.

#### Tough Decisions

Why return trip? Rendezvous a good idea? Thinning the herd. Pragmatic Chris Evans again.

#### Bomb Scene

We looooooooooove the score around this whole thing.

#### Post-Credits Secret Scene

{% youtube "https://www.youtube.com/watch?v=BHTxzn4YL6o" %}
