---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-07-19 07:25:15+00:00
description: 'Miles Greb joins us to look at Godzilla (Gojira) and historical context, atomic weapons, censhorship, science, regret, metaphors, analogies, more'
layout: podcast_post
permalink: /decipherscifi/godzilla-miles-greb-movie-podcast-episode-47
redirect_from:
  - /decipherscifi/47
slug: godzilla-miles-greb-movie-podcast-episode-47
title: 'Godzilla: postwar Japan, science, and regrets w/ Miles Greb'
tags:
  - science fiction
  - science
  - dinosaurs
  - radioactivity
  - shinto
  - japan
  - nuclear bombs

# Episode particulars
image:
  path: assets/imgs/media/movies/godzilla_1954.jpg
  alt: 'Godzilla smashing a city in Japan black & white movie backdrop'
links:
  - text: 'Godzilla: Half Century War by James Stokoe'
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Godzilla-Half-Century-War-ebook/dp/B00CTYEM9U'
  - text: King of the Monsters
    urls:
      - text: History of Japan Podcast
        url: 'http://historyofjapan.libsyn.com/episode-114-the-king-of-the-monsters'
  - text: Double Blasted
    urls:
      - text: Radiolab
        url: 'http://www.radiolab.org/story/223276-double-blasted/'
  - text: After the Gold Rush
    urls:
      - text: After the Gold Rush
        url: 'http://www.afterthegoldrush.space/'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/gojira/id836300968?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Gojira-English-Subtitled-Akira-Takarada/dp/B003MUU9TM'
  title: Godzilla
  type: movie
  year: 1954
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: miles_greb
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/godzilla_1954.jpg
  number: 47
  guid: 'http://www.decipherscifi.com/?p=903'
  media:
    audio:
      audio/mpeg:
        content_length: 26143065
        duration: '43:33'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Godzilla_Miles_Greb_decipherSciFi.mp3'
---
#### This is real life

Well, partially. Lcuky Dragon #5. Hydrogren bomb testing. Real life and movie deaths. Press censorship.

#### Giant movie monsters

Godzilla and his ilk. The Lost World. The Beast from 20,000 Fathoms.

#### Oxygen destroyer

¯\\_(ツ)_/¯

#### Science and regret

Oppenheimer. Eyepatch guy. Suicide to save the world. Analogies.

#### Goodbyes

Let them fight.
