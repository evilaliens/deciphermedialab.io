---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - tv
date: 2017-06-06 07:45:24+00:00
description: 'Fraser Cain joins us to talk space science and The Expanse - falling into the sun, Martian gravity, Venus sucks sucks sucks, space settlement, solar energy'
layout: podcast_post
permalink: /decipherscifi/the-expanse-season-2-feat-fraser-cain-episode-93
redirect_from:
  - /decipherscifi/93
slug: the-expanse-season-2-feat-fraser-cain-episode-93
title: 'The Expanse Season 2: falling into the sun, martian gravity, and human g-force limits w/ Fraser Cain'
tags:
  - g forces
  - gravity
  - mars
  - science
  - science fiction
  - solar
  - space
  - space settlement
  - venus

# Episode particulars
image:
  path: assets/imgs/media/tv/the_expanse_2015_season_2.jpg
  alt: Martian Marines cresting a hill on Mars, The Expanse backdrop
links:
  - text: "Fraser's things"
    urls:
      - text: Universe Today
        url: 'https://www.universetoday.com'
      - text: Astronomy Cast
        url: 'http://www.astronomycast.com'
      - text: Fraser Cain @ YouTube
        url: 'https://www.youtube.com/user/universetoday'
  - text:  'The Expanse: Nick Farmer on conlanging and the intersection of language and scifi'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/46'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/tv-season/the-expanse-season-2/id1177088082?mt=4&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Calibans-War/dp/B01NGWQPX3'
  title: The Expanse
  type: movie
  year: 2011
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: fraser_cain
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/the_expanse_2015_season_2.jpg
  number: 93
  guid: 'http://www.decipherscifi.com/?p=1565'
  media:
    audio:
      audio/mpeg:
        content_length: 49464947
        duration: '58:52'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Expanse_Season_2_--_Fraser_Cain_--_decipherSciFi.mp3'
---
#### Falling into the sun

Why not dump all of our nuclear waste there? Shedding orbital velocity. Eros the magic space potato.

#### Martian Gravity

Our lack of understanding irl of whether humans can conceive and gestate and grow in low or microgravity. Centrifugal artificial gravity.

#### Epstein Drive

Pulling Gs. Human limits.

{% youtube "https://www.youtube.com/watch?v=lK8U8RZyzsM" %}

#### Exciting times

Finally we're in a place that we're taking space settlement for granted. Developments in space travel like the Falcon Heavy et al. Recently, Stratolaunch:

{% youtube "https://www.youtube.com/watch?v=6ez2MDjRbzs" %}

#### Settlement issues

Fragility of interconnected systems at small scale. Radiation. Living inside a big rock.

#### Venus

Venus _sucks_. Mostly. On the surface, at least. Soviet Venus missions. Turning Venus into a spaceship.

{% youtube "https://www.youtube.com/watch?v=T1c-P1yEEnM" %}

#### Solar Energy

Harvesting and limiting solar exposure in different parts of the solar system. Relative solar intensity.
