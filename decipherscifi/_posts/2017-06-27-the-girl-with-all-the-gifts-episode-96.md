---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2017-06-27 07:45:25+00:00
description: "Fungus zombies! All about fungus and related dad jokes. Parasites on which the film's antagonist was based. How smell works. Human evolution, more"
layout: podcast_post
permalink: /decipherscifi/the-girl-with-all-the-gifts-episode-96
redirect_from:
  - /decipherscifi/96
slug: the-girl-with-all-the-gifts-episode-96
title: 'The Girl With All the Gifts: parasitic fungi, speed of smell, and the end of humanity'
tags:
  - cordyceps
  - evolution
  - fungus
  - mushroom
  - parasites
  - science
  - science fiction
  - smell
  - zombies

# Episode particulars
image:
  path: assets/imgs/media/movies/the_girl_with_all_the_gifts_2016.jpg
  alt: Young girl with a Hannibal Lecter mask
links:
  - text: The Girl With All the Gifts by M.R. Carey
    urls:
      - text: iTunes
        url: 'https://books.apple.com/us/book/the-girl-with-all-the-gifts/id645571072?mt=11&app=itunes&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Girl-All-Gifts-M-Carey-ebook/dp/B00CO7FLFG'
  - text: Why Do We Have Two Nostrils? by VSauce
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=eiAx2kqmUpQ'
  - text: The Last of Us
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Last-Us-Remastered-PlayStation-4/dp/B00JK00S0S'
  - text: Zombie Parasites from Nat Geo Live
    urls:
      - text: YouTube
        url: 'Zombie Parasites from Nat Geo Live'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-girl-with-all-the-gifts/id1199019294?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Girl-All-Gifts-Gemma-Arterton/dp/B06VXZJ4CJ'
  title: The Girl With All the Gifts
  type: movie
  year: 2016
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_girl_with_all_the_gifts_2016.jpg
  number: 96
  guid: 'http://www.decipherscifi.com/?p=1607'
  media:
    audio:
      audio/mpeg:
        content_length: 33816576
        duration: '40:14'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Girl_With_all_the_Gifts_--_--_decipherSciFi.mp3'
---
#### Dad jokes

A fun guy lol ?

#### New sorts of zombies

Fungus zombies! Similarities to _The Last of Us_. Surprise that we're not completely tired of zombies. Pandemic vs epidemic.

#### Fungus

Mycelium, hyphae, fruiting bodies. Spores! "All mushrooms are edible. Some only once."

#### Fungus Marionettes

Fungi playing Surgeon Simulator with human bodies.

{% youtube "https://www.youtube.com/watch?v=Y2F3ZWEEbF4" %}

#### Sense of smell

Hunting by smell. Covering scents. Smelling slow and fast. The power of Axe Body spray. The power of chemical warfare. How "smells" work.

#### IRL

Ophiocordyceps unilateralis! The real fungus on which the zombie fungus is based. The rundown of its lifecycle. Biting the leaf. Chemical instructions controlling behaviour.

#### Cordyceps moving to humans

The difference in arthtropod and human circulatory and nervous systems. How to best spread fungal infections between humans.

#### The end of homo sapiens

Parasites forcing their hosts to act in their interests. Mushroom towers. The rise of homo boletus. Coming around on anthropocentrism.

#### Goodbyes

Do you want a cat?
