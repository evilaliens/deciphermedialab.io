---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - tv
date: 2018-02-06 06:17:02+00:00
description: 'Altered Carbon, science, and transhumanism. Zoltan Istvan guides us through major areas of futurism in relation to the show, and we tackle radical life extension, immortality, identity, race, gender, the cyberpunk wealth gap. And nipples.'
layout: podcast_post
permalink: /decipherscifi/altered-carbon-feat-zoltan-istvan-transhumanist-episode-128
redirect_from:
  - /decipherscifi/128
slug: altered-carbon-feat-zoltan-istvan-transhumanist-episode-128
title: 'Altered Carbon: Zoltan Istvan on transplant immortality, identity, and consciousness uploading'
tags:
  - biological immortality
  - body-swapping
  - immortality
  - science
  - science fiction
  - transhumanism
  - virtual reality

# Episode particulars
image:
  path: assets/imgs/media/tv/altered_carbon_2018.jpg
  alt: Cyberpunk city Altered Carbon backdrop
links:
  - text: Zoltan
    urls:
      - text: ZoltanIstvan.com
        url: 'http://www.zoltanistvan.com/'
  - text: Altered Carbon (the novel) by Richard K. Morgan
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/altered-carbon/id419980024?mt=11&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Altered-Carbon-Takeshi-Kovacs-Novels-ebook/dp/B000FBFMZ2'
media:
  links:
    - text: Netflix
      url: 'Netflix'
  title: Altered Carbon
  type: tv
  year: 
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: zoltan_istvan
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/tv/altered_carbon_2018.jpg
  number: 128
  guid: 'https://decipherscifi.com/?p=5107'
  media:
    audio:
      audio/mpeg:
        content_length: 35381248
        duration: '42:06'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Altered_Carbon_--_Zoltan_Istvan_--_decipherSciFi.mp3'
---
#### Immortality by transplant

Analogy with [developments in head transplantation](https://www.popsci.com/first-head-transplant-human-surgery#page-3). Consciousness uploading. Preservation of identity. Conflict of human immortality with religion.

#### Transhumanism and Identity

The meaning of race and gender in a world where anyone can be any variety of human (and more). Merging with AI and technology in general.

#### Sleeving

The boundaries of the "self" and the "mind" and the meat. Hard to draw! The influence of the greater body system over the "self" part and boners. So many boners. But not from nipple stuff, no sir. ?
