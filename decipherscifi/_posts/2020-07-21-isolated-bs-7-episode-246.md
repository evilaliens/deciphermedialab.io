---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - conversation
date: 2020-07-21T02:03:46-04:00
description: 'Hacking vs social engineering. Free software and the impossibility of competing with social networks. Unicode and Instacart and user experience failure. The story of why our RPG show never came out. And spotted dick!'
layout: podcast_post
redirect_from:
  - /decipherscifi/246
slug: isolated-bs-social-engineering-fun-latin-proper-beaver-representation
title: 'Isolated BS: social engineering, spotted dick, and proper beaver representation'
tags:
  - free software
  - hacking
  - kickstarter
  - role-playing games
  - science
  - science fiction
  - tattoos
  - ted chiang
  - writing systems

# Episode particulars
image:
  path: assets/imgs/media/misc/isolated_bs_7_rpgs.jpg
  alt: ''
links:
  - text: 'Monster Care Squad: A Ghibli-inspired tabletop adventure of healing Monsters and solving local problems in the gentle world of Ald-Amura'
    urls:
      - text: Kickstarter
        url: 'https://www.kickstarter.com/projects/sandypuggames/monster-care-squad'
  - text: 'Hackers: social engineering, Kevin Mitnick, and hacking before the internet w/ Brian Epstein'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/21'
  - text: 'Splatmoji - Quickly look up and input emoji and/or emoticons/kaomoji on your GNU/Linux desktop via pop-up menu.'
    urls:
      - text: GitHub.com
        url: 'https://github.com/cspeterson/splatmoji'
  - text: 'Decipher RPG - Mirror Pilot #1'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/rpg1'
  - text: 'Decipher RPG - Mirror Pilot #2'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/rpg2'
  - text: 'Decipher RPG - Mirror Pilot #3 (the finale!)'
    urls:
      - text: Decipher SciFi
        url: 'https://deciphermedia.tv/decipherscifi/rpg3'
  - text: 'The hilarious, extremely convincing proposal to make a beaver emoji.'
    urls:
      - text: Slate
        url: 'https://slate.com/technology/2019/05/beaver-emoji-proposal-is-hilarious-and-extremely-correct.html'
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/isolated_bs_7_rpgs.jpg
  number: 246
  guid: isolated-bs-social-engineering-fun-latin-proper-beaver-representation
  media:
    audio:
      audio/mpeg:
        content_length: 44076509
        duration: '52:25'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/bs_7.final.mp3'

---
# Hacking

The reality of most hacking: [social engineering](https://en.wikipedia.org/wiki/Social_engineering_(security)). [Hackers]({% link decipherscifi/_posts/2016-01-19-hackers-movie-podcast-episode-21.md %}) vs [Sneakers](https://www.themoviedb.org/movie/2322-sneakers). The 90s as the *best* period for hacking in fiction, back when people understood even less how computers worked. A review of Hacking in media.

# Free Software

Free as in speech, free as in beer, etc. Freedom-respecting social network alternatives and the dream of critical mass. [Mastodon](https://joinmastodon.org/). [How the USGS uses Twitter data to track earthquakes](https://blog.twitter.com/en_us/a/2015/usgs-twitter-data-earthquake-detection.html). The impossibility of competing with YouTube. Appreciating Free software and open source and collaboration.

# Unicode things hooray

Ranting about a user experience failure with Instacart. Text encoding, Unicode, emoji, and non-Latin writing systems. Emoji standardization. Lesbian emoji activism and the [new beaver emoji](https://emojipedia.org/beaver/)!

# Tattoos

Stephen Wolfram and [Arrival]({% link decipherscifi/_posts/2016-11-29-arrival-part-2-feat-nick-farmer-linguist-episode-66.md%}) Heptapod logogram creation. Ravens. Figuring out how to spell "Bomberman" in Heptatpod.

# RPGs

Dungeons and Dragons is fun! The positives and negatives of play-by-post. A review of our (Chris') failure with Decipher RPG. A desire to try again! [Liam Ginty]({% link _people/liam_ginty.md %}).

[Monster Care Squad: A Ghibli-inspired tabletop adventure of healing Monsters and solving local problems in the gentle world of Ald-Amura](https://www.kickstarter.com/projects/sandypuggames/monster-care-squad)

# Percent!

"Percent" is literally just latin for "per hundred" and Christopher was this many years old when he realized this! And then we learn about [per mille](https://en.wikipedia.org/wiki/Per_mille) and [per myriad](https://en.wikipedia.org/wiki/Basis_point#Permyriad) and have to promise not to start using them.

# Spotted dick

A rewarding pun-tastic etymological romp. Also it was really tasty!

# Attribution

* "[Six dice of various colours](https://commons.wikimedia.org/wiki/File:Dice_(typical_role_playing_game_dice).jpg)" used in episode image - by Diacritica CC-BY-SA-3.0
* Decipher RPG Mirror illustration commissioned for Decipher Media by Amber Lee Jones
