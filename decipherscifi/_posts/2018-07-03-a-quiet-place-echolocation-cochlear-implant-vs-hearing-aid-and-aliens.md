---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-07-03 07:45:36+00:00
description: 'The science of sound-based navigation. The awesomeness of bats. Echolocation. Grain entrapment. Cochlear implants vs hearing aids. Alien evolution. Sound.'
layout: podcast_post
permalink: /decipherscifi/a-quiet-place-echolocation-cochlear-implant-vs-hearing-aid-and-aliens
redirect_from:
  - /decipherscifi/148
slug: a-quiet-place-echolocation-cochlear-implant-vs-hearing-aid-and-aliens
title: 'A Quiet Place: echolocation, cochlear implant vs hearing aid, and aliens'
tags:
  - aliens
  - cochlear implants
  - echolocation
  - hearing
  - science
  - science fiction
  - sound

# Episode particulars
image:
  path: assets/imgs/media/movies/a_quiet_place_2018.jpg
  alt: Emily Blunt looking distressed in a bathtub
links:
  - text: 'BBC Radio 4 - In Our Time, Echolocation'
    urls:
      - text: BBC
        url: 'https://www.bbc.co.uk/programmes/b0b6hrl3'
  - text: Shooting Down a Lost Drone and why Dogs Tilt their Heads - Smarter Every Day
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=Oai7HUqncAA'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/a-quiet-place/id1356850151?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Quiet-Place-Emily-Blunt/dp/B07BZ2YYPF'
  title: A Quiet Place
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/a_quiet_place_2018.jpg
  number: 148
  guid: 'https://decipherscifi.com/?p=6779'
  media:
    audio:
      audio/mpeg:
        content_length: 34170130
        duration: '40:40'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/A_Quiet_Place_--_--_Decipher_SciFi.mp3'
---
#### Surviving without sound

Deafness. How loud is a fart? Cochlear implants. Alien defense engineering.

#### Cochlear implants

How cochlear implants differ from hearing aids. How a cochlear implant works. The difficulties of hearing loss.

#### Grain Entrapment

_Ughhhhhhhhh_. Compression and suffocation.

#### Echolocation

The strangeness of terrestrial echolocation. Frequency adaptations for different types of prey. Signal rate at different distances.

#### The creatures

Planetary orbit situations with extended periods of surface darkness. Extremophile armor development. Echolocation evolution. Space cat behaviour. [Dinner Time](https://www.youtube.com/watch?v=4v4OeAY-Z0A) ambulation.

#### Killing aliens

Feedback loops. Amplitude vs frequency and damaging the hearing apparatus.
