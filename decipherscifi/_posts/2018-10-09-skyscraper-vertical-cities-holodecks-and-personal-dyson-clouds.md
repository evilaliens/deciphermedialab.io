---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2018-10-09 07:45:49+00:00
description: 'Big buildings! Engineering! The future of skyscrapers. Varieties of sand. IRL holodeck technologies. Elevators. Arcologies. And wooden skyscrapers, finally!'
layout: podcast_post
permalink: /decipherscifi/skyscraper-vertical-cities-holodecks-and-personal-dyson-clouds
redirect_from:
  - /decipherscifi/162
slug: skyscraper-vertical-cities-holodecks-and-personal-dyson-clouds
title: 'Skyscraper: vertical cities, holodecks, and personal Dyson clouds'
tags:
  - science fiction
  - science

# Episode particulars
image:
  path: assets/imgs/media/movies/skyscraper_2018.jpg
  alt: 'Skyscraper movie backdrop, but with trajectory lines'
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/skyscraper/id1405620210?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Skyscraper-Dwayne-Rock-Johnson/dp/B07FD5T12N'
  title: Skyscraper
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/skyscraper_2018.jpg
  number: 162
  guid: 'https://decipherscifi.com/?p=7854'
  media:
    audio:
      audio/mpeg:
        content_length: 34424354
        duration: '40:58'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Skyscraper__--_--_Decipher_SciFi.mp3'
---
#### Skyscrapers

History. The industrialization at scale of steel. The state of things. Future heights. [Arcologies](https://en.wikipedia.org/wiki/Arcology).

Vertical cities

[Hong Kong's real estate problem](https://www.youtube.com/watch?v=hLrFyjGZ9NU). Urban verticality and resource efficiency. Living in coffin pods and sharing utilities.

#### Holodeck

IRL holodeck technology. Projecting images for multiple . Your own personal Dyson display cloud. Frickin lasers directly in your eyes. CastAR.

#### Skyscraper materials technology

Sand as a rubbish base for huge buildings. [Building deep foundations in the desert](https://www.youtube.com/watch?v=QDGjVhS9das). [Wooden skyscrapers](https://www.youtube.com/watch?v=Xi_PD5aZT7Q)!? Sand in concrete and sand shortages. Varieties of sand. Engineered sand.

#### Fire

Fire suppression! Overheated air and air pressure for aircraft.

#### Elevators

Elevator cable lengths and limits. Maglev elevators. High-speed elevators.

{% youtube "https://www.youtube.com/watch?v=L1BDM1oBRJ8" %}

*PORK CHOP SANDWICHES*
