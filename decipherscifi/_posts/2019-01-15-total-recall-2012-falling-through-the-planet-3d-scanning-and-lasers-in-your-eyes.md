---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-01-15 08:35:18+00:00
description: 'The (im)possibility of making a hole through Earth. Ultrafast travel with or without vacuum. Room-mapping by Roomba or photogrammetry. The Coriolis effect.'
layout: podcast_post
permalink: /decipherscifi/total-recall-2012-falling-through-the-planet-3d-scanning-and-lasers-in-your-eyes
redirect_from:
  - /decipherscifi/176
slug: total-recall-2012-falling-through-the-planet-3d-scanning-and-lasers-in-your-eyes
title: 'Total Recall 2012: falling through the planet, 3d scanning, and lasers in your eyes'
tags:
  - 3d mapping
  - dystopia
  - gravity
  - meomry
  - philip k dick
  - physics
  - science
  - science fiction

# Episode particulars
image:
  path: assets/imgs/media/movies/total_recall_2012.jpg
  alt: Colin Farrel disintegrating into bits
links:
  - text: FACE SWAP by David Gidali
    urls:
      - text: Vimeo
        url: 'https://vimeo.com/306304445'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/total-recall-directors-cut-2012/id559737629?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Total-Recall-Unrated-Colin-Farrell/dp/B00A0R02HO'
  title: Total Recall
  type: movie
  year: 2012
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/total_recall_2012.jpg
  number: 176
  guid: 'https://decipherscifi.com/?p=9767'
  media:
    audio:
      audio/mpeg:
        content_length: 30790341
        duration: '36:38'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Total_Recall_2012_--_--_Decipher_SciFi.mp3'
---
#### Philip K Dick

Giving'em the old Philip K Dick joint. Cyberpunk. Adaptation fidelity.

#### Even though it looks like it's the future...

Luminescent tattoos! Bioluminescence, electroluminescence, phosphorescence, and maybe some other -escences too! A world with _three boobs_. Well, maybe more than that in the whole world. Hand phones. Ubiquitous smart glass. Neck-mounted face-replacing direct-eye laser projectors. Memory manipulation and deletion.

#### "The Fall"

Digging a hole _through_ the Earth. Trying to find hard things to do when building a space elevator is too _easy_. Adding unnecessary rockets to your earth-faller to reach a seventeen minute trip time. Digging deep in the Earth and heat issues. Finding impenetrable hot rock stew even without piercing the crust. Atmospheric pressure in the center of the Earth. Making it work by vacuum tube. The one easy straight line through the Earth that avoids the Coriolis effect, and the actually useful curved lines between land areas.

#### 3d space-scanning

Camera rocket grenades that explode into camera bullets. Like a camera, but more messy. IR scanning. Two-dimensional [room-mapping by Roomba](https://www.theverge.com/circuitbreaker/2018/12/26/18156600/doomba-roomba-cleaning-maps-doom-levels-rich-whitehouse). Photogrammetry. Playing VR hentai sex games in your room, _in your room_.
