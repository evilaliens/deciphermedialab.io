---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2015-09-15 05:03:03+00:00'
description: 'With Moon we talk science fiction and Sam Rockwell, solitary confinement, functional robot design, immoral mega corporation, helium3, exploitative business'
layout: podcast_post
permalink: /decipherscifi/moon_movie_podcast_episode_003
redirect_from:
  - /decipherscifi/3
slug: moon_movie_podcast_episode_003
title: 'Moon: helium 3 lunar mining and emoji robots'
tags:
  - science fiction
  - isolation
  - robots
  - user interface design
  - space
  - space mining
  - helium-3
  - fusion

# Episode particulars
image:
  path: assets/imgs/media/movies/moon_2009.jpg
  alt: 'Moon 2009 backdrop'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/moon/id331842140?mt=6'
    - text: Amazon
      url: 'https://www.amazon.com/Moon-Sam-Rockwell/dp/B00380URIO/'
  title: Moon
  type: movie
  year: 2009
links:
  - text: Michel Siffre
    url: 'https://en.wikipedia.org/wiki/Michel_Siffre'
  - text: The Island
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/movie/the-island/id291859166?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'https://www.amazon.com/gp/video/detail/B00HALPWOG/'
  - text: '2001: A Space Odyssey'
    urls:
      - text: iTunes 
        url: 'https://geo.itunes.apple.com/us/movie/2001-a-space-odyssey/id285993250?at=1001l7hP&mt=6'
      - text: Amazon
        url: 'http://www.amazon.com/2001-Odyssey-Blu-ray-Keir-Dullea/dp/B000Q66J1M/'
people:
  - data_name: lee_colbert
    roles:
      - host
  - data_name: christopher_peterson
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 3
  guid: 'http://www.decipherscifi.com/?p=203'
  media:
    audio:
      audio/mpeg:
        content_length: 20473164
        duration: '34:06'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Moon_2009.mp3' 
---
#### Movies with strict limitations in cast and location


Love these! Moon is a good one, seriously only one human actor present. Other pleasing examples include _Buried_ with Ryan Reynolds, _The Man From Earth_, _12 Angry Men_ is the greatest.


#### Isolation


Humans need other humans. Our own introversion. Solitary confinement is torture. Having the light at the end of the tunnel.


#### Gerty


Love the robot design. Practical to the extreme. He has a cupholder! The name means nothing. LOVE the smiley interface. Hal 9000 references.


#### Space Business


Multinational mega corporation with government ties. Bottom line is all that matters. The economics of evil and helium-3 mining. Exploitative labor practices. Helium 3 science.

[spoiler title='Spoiler Notes' style='default' collapse_link='true']
Cloning

How would you feel to be a clone? To be an original who is cloned? A clone of you, even in adult form with implanted memories, is not "you" anymore once the copy is made. Could you work together with your clone?[/spoiler]
