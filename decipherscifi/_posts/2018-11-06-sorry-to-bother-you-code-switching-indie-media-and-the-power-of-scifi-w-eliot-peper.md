---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-11-06 07:45:24+00:00
description: 'The power of science fiction storytelling. HG Wells and scifi conscience. Mass media vs indie community. Eliot Peper. Milk stuff.'
layout: podcast_post
permalink: /decipherscifi/sorry-to-bother-you-code-switching-indie-media-and-the-power-of-scifi-w-eliot-peper
redirect_from:
  - /decipherscifi/166
slug: sorry-to-bother-you-code-switching-indie-media-and-the-power-of-scifi-w-eliot-peper
title: 'Sorry to Bother You: code switching, indie media, and the power of scifi w/ Eliot Peper'
tags:
  - language
  - linguistics
  - media
  - milk
  - privacy
  - science
  - science fiction
  - social commentary
  - storytelling
  - wealth

# Episode particulars
image:
  path: assets/imgs/media/movies/sorry_to_bother_you_2018.jpg
  alt: 'Cassius and Detroit, protesting. Sorry to Bother you movie backdrop.'
links:
  - text: Bandwidth by Eliot Peper
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Bandwidth-Analog-Novel-Book-1-ebook/dp/B075CLV95J'
  - text: Borderless by Eliot Peper
    urls:
      - text: Amazon
        url: 'https://www.amazon.com/Borderless-Analog-Novel-Book-2-ebook/dp/B07BM7F9SF'
  - text: 'Key & Peele Obama Meet & Greet'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=nopWOC4SRm4'
  - text: 'Key & Peele on Code Switching'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=_YkE7W6qegg'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/sorry-to-bother-you/id1405351816?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Sorry-Bother-You-Boots-Riley/dp/B07FN8STFS'
  title: Sorry to Bother You
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: eliot_peper
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/sorry_to_bother_you_2018.jpg
  number: 166
  guid: 'https://decipherscifi.com/?p=8059'
  media:
    audio:
      audio/mpeg:
        content_length: 36645724
        duration: '43:36'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Sorry_to_Bother_You_--_Eliot_Peper_--_Decipher_SciFi.mp3'
---
#### This movie

It gets a bit weird! And we have a bit of a time summarizing it.

#### Storytelling

Adventure stories. Social allegory. The power of science fiction in particular to _force_ you to reframe your perception by believably breaking your assumptions. HG Wells and Moreau. Science fiction tropes and the subversion thereof. Christopher is bad at human things.

#### Code Switching

Languages, dialect, and speaking to your audience. "Not having to think about it."

Media

Mass media vs indie communities. The lowest common denominator. Kevin Kelly's [1000 True Fans](https://kk.org/thetechnium/1000-true-fans/).

#### Milk stuff

A lament for the concentration on cow's milk. Mare milk. Fermented milk products from cows and horses.
