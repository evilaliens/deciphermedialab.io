---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-10-02 07:45:52+00:00
description: 'The moons of Mars and the ways in which they are rubbish. What is a moon? Space marine tactics. Corporate space settlement. The prometheus problem, again!'
layout: podcast_post
permalink: /decipherscifi/doom-moons-of-mars-space-marines-and-telefragging
redirect_from:
  - /decipherscifi/161
slug: doom-moons-of-mars-space-marines-and-telefragging
title: 'Doom: moons of Mars, space marines, and telefragging w/ Josh Effengee of LSG Media'
tags:
  - doom
  - marines
  - mars
  - moons
  - nostalgia
  - retro gaming
  - science
  - science fiction
  - space
  - space settlement

# Episode particulars
image:
  path: assets/imgs/media/movies/doom_2005.jpg
  alt: 'Space marines The Rock and Carl Urban Doom movie backdrop'
links:
  - text: Science Fiction Film Podcast
    urls:
      - text: LSG Media
        url: 'https://www.libertystreetgeek.net/all-science-fiction-film-podcasts/'
  - text: The X-Files Podcast
    urls:
      - text: LSG Media
        url: 'https://www.libertystreetgeek.net/all-x-files-podcasts/'
  - text: Masters of Doom by David Kushner
    urls:
      - text: iTunes
        url: 'https://www.amazon.com/Masters-Doom-Created-Transformed-Culture-ebook/dp/B000FBFNL0/'
      - text: Amazon
        url: 'https://geo.itunes.apple.com/us/book/masters-of-doom/id420345142?mt=11&at=1001l7hP'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/doom-unrated/id300684230?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Doom-Unrated-Dwayne-Johnson/dp/B001OBSE2A'
  title: Doom
  type: movie
  year: 2005
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: josh_effengee
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/doom_2005.jpg
  number: 161
  guid: 'https://decipherscifi.com/?p=7822'
  media:
    audio:
      audio/mpeg:
        content_length: 41674198
        duration: '49:36'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Doom_--_Josh_Effengee_--_Decipher_SciFi.mp3'
---
#### Doom

Doom! Nostalgia.

#### The Martian moons

Crappy moons that can barely form a potato. Phobos and Deimos. Delineating between moons, planets, and all the other stuff. The likely origin of the Martian moons.

#### Space marines

Corporate space settlement and operations. Space Force. Josh reviews space marines; again. Martian weaponry.

#### Settling mars

Where to build? The cost of scratch settlement construction. Caves and lava tubes. Teleporting resources changes the game.

#### Ancient aliens

The [Prometheus Problem]({% link decipherscifi/_posts/2017-05-16-prometheus-episode-90.md %}), _again_. Doom humans-on-mars hypotheses: aliens developed humans on Mars vs humans originated on Mars. Species resurrection. Gene therapy. Chromosomal addition and subtraction.

#### Martian miscellany

Nano walls! Water on Mars! Telefragging!
