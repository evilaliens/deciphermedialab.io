---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - conversation
date: 2020-08-18T02:05:59-04:00
description: 'Awaiting our cyberpunk robot limbs. Fighting facial recognition with Fawkes. Google correlating food poisoning outbreaks. NES emulation on PSP. The power of limiting your gaming options. Etc.'
layout: podcast_post
redirect_from:
  - /decipherscifi/250
slug: proactive-prostheses-facial-recognition-correlating-diarrhea
title: 'Proactive prostheses, facial recognition, and correlating diarrhea'
tags:
  - big brother
  - emulation
  - facial recognition
  - machine learning
  - science
  - science fiction
  - video games

# Episode particulars
image:
  path: assets/imgs/media/misc/isolated_bs_9_fawkes.jpg
  alt: 'Jar brain hiding behind a Fawkes mask in a crowd of brains'
links:
  - text: "Fawkes Image 'Cloaking' for Personal Privacy"
    urls:
      - text: Chicago.edu
        url: 'http://sandlab.cs.uchicago.edu/fawkes/'
  - text: 'Fawkes: Protecting Personal Privacy against Unauthorized Deep Learning Models (USENIX Security 2020)'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=AWrI0EuYW6A'
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/isolated_bs_9_fawkes.jpg
  number: 250
  guid: proactive-prostheses-facial-recognition-correlating-diarrhea
  media:
    audio:
      audio/mpeg:
        content_length: 20630551
        duration: '24:31'
        explicit: false
        url: 'https://traffic.libsyn.com/secure/decipherscifi/bs9.final.mp3'

---
<sub><sup>Fawkes mask in post image [Multipainkiller Studio CC-BY](https://sketchfab.com/3d-models/guy-fawkes-mask-v1-free-ffdc746cabc647259fc494c151dc3d8f)</sup></sub>

# Prosthetics

Proactive prostheses in our future? Recognizing that we routinely do this with major joints, and with for a full cyberpunk limb-improvement future.

# Big brother etc

Machine learning facial recognition. [Fawkes](http://sandlab.cs.uchicago.edu/fawkes/) machine learning facial image "cloaking." The possible value of poisoned data sets. Placing individual bricks in the wall that is our ability be private and protect our data. Google correlating diarrhea. 

# Emulation

Finding a use for an old [PlayStation Portable](https://en.wikipedia.org/wiki/PlayStation_Portable). [RetroArch](https://www.retroarch.com/). The difficulty of old games and the power of limiting your options.

# Our website

Our [first outside code contribution](https://gitlab.com/deciphermedia/deciphermedia.gitlab.io/-/merge_requests/2)! And just thanks to Elad Avron and Hugh Fisher 😄!
