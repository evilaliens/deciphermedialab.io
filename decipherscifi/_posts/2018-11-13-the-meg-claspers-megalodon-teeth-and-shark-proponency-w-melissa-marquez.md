---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2018-11-13 07:45:53+00:00
description: "Real ancient Megalodons, and how we know they're gone. Deep see adaptations. Shark conservation. Scary things from the ocean. Life in the Marianas trench."
layout: podcast_post
permalink: /decipherscifi/the-meg-claspers-megalodon-teeth-and-shark-proponency-w-melissa-marquez
redirect_from:
  - /decipherscifi/167
slug: the-meg-claspers-megalodon-teeth-and-shark-proponency-w-melissa-marquez
title: 'The Meg: claspers, megalodon teeth, and shark proponency w/ Melissa Marquez'
tags:
  - archaeology
  - biology
  - ocean
  - oceanography
  - science
  - science fiction
  - sharks

# Episode particulars
image:
  path: assets/imgs/media/movies/the_meg_2018.jpg
  alt: "A big shark eating a big dude BUT itself being eaten by a larger shark, it's incredible. The Meg movie backdrop."
links:
  - text: Melissa's Chondrichthyan-promotion organization
    urls:
      - text: The Fins United Initiative
        url: 'https://www.finsunited.co.nz/'
  - text: Melissa's podcast
    urls:
      - text: ConCiencia Azul
        url: 'https://melissacristinamarquez.weebly.com/conciencia-azul-podcast.html'
media:
  links:
    - text: iTunes
      url: ''
    - text: Amazon
      url: ''
  title: 
  type: movie
  year: 
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: melissa_marquez
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_meg_2018.jpg
  number: 167
  guid: 'https://decipherscifi.com/?p=7990'
  media:
    audio:
      audio/mpeg:
        content_length: 36544756
        duration: '43:29'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/The_Meg_--_Melissa_Marquez_--_Decipher_SciFi.mp3'
---
#### Sharks

A sharky public service message/pro-shark propaganda. Sharks get a bad wrap in the media. Eating weird stuff. The importance of sharks in the ocean and their roles in their particular ecosystems.

#### Shark behaviour

Biting the internet. Alternative shark species name suggestions. "Exploratory" biting. [Claspers](https://youtu.be/RT_HBQWxgo0)! The Megalodon's likely "_biiiiiiiiiig_" penis. Shark skin with dermal denticles, aka "teeth paper." Shark intelligence and how you might measure it.

#### The Megalodon

Irl best estimates of megalodon size vs the creatures of the film. Limited fossil data and cartilaginous skeletons. Learning from paleoarchaeological study of ancient shark relatives with more calcified skeletal material. Working backward from the modern shark bodyform. Ancient buzzsaw-mouthed monsters. The importance of teeth.

{% responsive_image path: assets/imgs/misc/shark_helicoprion.jpg alt: "Helicoprion buzzsaw-mouth shark" caption: "Helicoprion" attr_url: "https://commons.wikimedia.org/wiki/File:Helicoprion_as_chimeroid.jpg" attr_text: "ДиБгд CC-BY-SA-4.0" %}

#### The Marianas Trench

Pressure and darkness at depth. Adaptations to pressure and darkness. Odd, squishy creatures. Blob fish as the [gudetama](https://giphy.com/search/gudetama) of the sea.

{% responsive_image path: assets/imgs/misc/blob_fish.jpg alt: "Blob fishes, out of their element" caption: "Blob fishes, out of their element" attr_url: "https://commons.wikimedia.org/wiki/File:Psychrolutes_phrictus.jpg" attr_text: "AFSC CC-BY-NC-ND-2.0" %}

#### Conservation

Shark public image management. Sharks with eyebrows.
