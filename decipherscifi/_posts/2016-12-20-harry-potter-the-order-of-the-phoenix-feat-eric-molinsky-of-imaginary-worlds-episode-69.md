---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2016-12-20 07:45:11+00:00 
description: 'Eric Molinsky comes on to help us decipher more fantasy. Patrick Rothfus, wizard warfare, muggle technology vs magic, wizard Hitler, "true name" magic, more' 
layout: podcast_post
permalink: /decipherscifi/harry-potter-the-order-of-the-phoenix-feat-eric-molinsky-of-imaginary-worlds-episode-69
redirect_from:
  - /decipherscifi/69
slug: harry-potter-the-order-of-the-phoenix-feat-eric-molinsky-of-imaginary-worlds-episode-69 
title: 'Harry Potter & The Order of the Phoenix: wizard and muggle technologies w/ Eric Molinsky'
tags:
  - science fiction
  - science
  - fantasy
  - magic

# Episode particulars
image:
  path: assets/imgs/media/movies/harry_potter_and_the_order_of_the_phoenix_2007.jpg
  alt: 'A bunch of kids encroaching upon the camera with wands Harry Potter Order of the Phoenix movie backdrop'
links:
  - text: Harry Potter
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/harry-potter-sorcerers-stone/id1037193578?mt=11'
      - text: Amazon
      - url: 'https://www.amazon.com/Harry-Potter-Complete-Collection-1-7-ebook/dp/B01B3DIPMW'
  - text: Imaginary Worlds Podcast
    urls:
      - text: Apple Podcasts
        url: 'https://itunes.apple.com/us/podcast/imaginary-worlds-eric-molinsky/id916273527'
      - text: RSS
        url: 'https://rss.art19.com/imaginary-worlds'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/harry-potter-order-phoenix/id273696792?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Harry-Potter-Phoenix-Daniel-Radcliffe/dp/B0012GE91W'
  title: 'Harry Potter & the Order of the Phoenix'
  type: movie
  year: 2007
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: eric_molinsky
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/harry_potter_and_the_order_of_the_phoenix_2007.jpg
  number: 69
  guid: 'http://www.decipherscifi.com/?p=1217'
  media:
    audio:
      audio/mpeg:
        content_length: 27681793
        duration: '38:26'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Harry_Potter_and_the_Order_of_the_Phoenix_Eric_Molinsky_decipherSciFi.mp3'
---
#### Scifi and Fantasy

Our different aptitudes for processing fantasy. Fantasy tropes, internal consistency, Patrick Rothfuss.

#### Wizard and muggle technologies

Telegraphs, internets, owls and agentive magical paper airplanes. Other things too!

#### The power of names

"True name" magic in Harry Potter. IRL examples. Ancient Egypt, Medieval Europe, Jewish mysticism, Aronofsky's Pi. Rothfuss again.

#### Classism and wizard racism

Wizard Hitler!

#### Wizard warfare

Magic bullet machine gun enchanted buckler wizards. Enchanted riot shields with a wand-hole in them. Other innovations.

#### Characters

The fascististic headmistress, Ballatrix, Luna, and other characters we wanted to note.
