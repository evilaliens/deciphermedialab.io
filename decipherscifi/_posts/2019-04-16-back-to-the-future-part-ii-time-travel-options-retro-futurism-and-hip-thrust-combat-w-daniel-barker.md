---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-04-16 07:45:19+00:00
description: 'Leaving girlfriends in alleyways. Time travel modes review. The importance of accurate time *and* space measurement. The speed of causality. Smart clothing. Etc.'
layout: podcast_post
permalink: /decipherscifi/back-to-the-future-part-ii-time-travel-options-retro-futurism-and-hip-thrust-combat-w-daniel-barker
redirect_from:
  - /decipherscifi/189
slug: back-to-the-future-part-ii-time-travel-options-retro-futurism-and-hip-thrust-combat-w-daniel-barker
title: 'Back to the Future Part II: time travel options, retro futurism, and hip thrust combat w/ Daniel Barker'
tags:
  - fashion
  - future
  - optimism
  - relativity
  - science
  - science fiction
  - spacetime
  - time travel

# Episode particulars
image:
  path: assets/imgs/media/movies/back_to_the_future_part_ii_1989.jpg
  alt: "Doc and Marty by their time travel car being like, 'whoah'"
links:
  - text: 'VFXcool: Back to the Future Trilogy by Captain Disillusion Part 1'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=AtPA6nIBs5g'
  - text: 'VFXcool: Back to the Future Trilogy by Captain Disillusion Part 2'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=rhNDsPMaK_A'
  - text: 'Uncertainty Principle'
    urls:
      - text: 'UncertaintyPrincipleThePodcast.com'
        url: 'https://uncertaintyprinciplethepodcast.com/'
  - text: 'Welcome, Marty'
    urls:
      - text: 'Uncertainty Principle @ Soundclod'
        url: 'https://soundcloud.com/uncertaintyprinciple/welcome-marty-a-special-short-episode'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/back-to-the-future-part-ii/id380040080?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Back-Future-Part-Michael-Fox/dp/B00498YYCE'
  title: Back to the Future Part II
  type: movie
  year: 1989
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: daniel_barker
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/back_to_the_future_part_ii_1989.jpg
  number: 189
  guid: 'https://decipherscifi.com/?p=10371'
  media:
    audio:
      audio/mpeg:
        content_length: 44492507
        duration: '52:57'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/back_to_the_future_part_ii_--_daniel_james_barker_--_decipherscifi.mp3'

---
#### Likenesses

The [legal precedent](https://www.hollywoodreporter.com/thr-esq/back-future-ii-a-legal-833705) set by Crispin Glover's lawsuit against the production; folks are more careful these days as a result. "Grandma Tarkin."

#### Rick & Morty

Rick & Morty's [original short](https://www.youtube.com/watch?v=1lpKx3FMNC4) lampooning Doc & Marty.

#### The future

The fun, friendly future where the alleyways are safer than the streets. Worrying about your future children and feeling disgusted by your teenage self. Pelvic thrusting fight technique.

{% responsive_image_block %}
  path: 'assets/imgs/misc/daniel_barker_scenekid.jpg'
  alt: 'Daniel Barker, former scene kid'
  caption: 'Daniel Barker, former scene kid'
{% endresponsive_image_block %}

#### Time travel technique

Buying The almanac. Varieties of time travel: mutable, immutable, and branching. The way BTTF mixes these options at its leisure. The butterfly effect. Coping with the scale of possibilities. Strategic infanticide and Godwin's Law.

#### Spacetime travel

Taking into account frames of reference. Appearing suddenly in near-vacuum if you don't properly calibrate your time machine to also account for space travel. The importance of accurate time measurement e.g. [Doctor Who The Movie]({% link decipherscifi/_posts/2018-12-31-doctor-who-the-y2k-scare-supernumerary-hearts-and-the-utility-of-atomic-clocks.md %}). Destroying "the universe" but only the local area as causality here could not affect far away galaxies due to expansion.  `¯\_(ツ)_/¯`

#### Rational Optimism

Dan's message [welcoming Marty McFly to the future](https://soundcloud.com/uncertaintyprinciple/welcome-marty-a-special-short-episode). Recognizing the overarching improvements in the wellbeing of humanity in the face of the negative newscycle.

#### Future things!

[Bricking your self-lacing shoes](https://arstechnica.com/gadgets/2019/02/my-left-shoe-wont-even-reboot-faulty-app-bricks-nike-smart-sneakers/). Keeping your shoes from joining a botnet. Self-smoking shirts. The periodic nature of futurism where "the future" always looks basically like now but with neato accessories. Hoverboard hoaxes and disappointments.
