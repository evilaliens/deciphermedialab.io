---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: '2015-12-01 04:45:29+00:00'
description: 'With Gattaca on the show we talk designer babies, American eugenics, the new eugenics, genetic discrimination, nature and nurture, and more!'
layout: podcast_post
permalink: /decipherscifi/gattaca-podcast-episode-14
redirect_from:
  - /decipherscifi/14
slug: gattaca-podcast-episode-14
title: 'Gattaca: retrofuturism, eugenics, and the ethics of designer babies'
tags:
  - biotechnology
  - disability
  - dna
  - dysgenics
  - dystopia
  - eugenics
  - genetics
  - identity
  - science
  - science fiction
  - space

# Episode particulars
image:
  path: assets/imgs/media/movies/gattaca_1997.jpg
  alt: Gattaca movie backdrop
links:
  - text: 'Genetic Determinism and Gene Therapy in Gattaca - by David A Kirby'
    url: 'http://www.depauw.edu/sfs/essays/gattaca.htm'
  - text: In Time - Decipher SciFi episode 11
    url: 'http://www.decipherscifi.com/11'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/gattaca/id542688199?at=1001l7hP&mt=6'
    - text: Amazon
      url: 'http://www.amazon.com/Gattaca-Ethan-Hawke/dp/B000I8G5B2'
  title: Gattaca
  type: movie
  year: 1997
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: 
  number: 14
  guid: 'http://www.decipherscifi.com/?p=359'
  media:
    audio:
      audio/mpeg:
        content_length: 31626094
        duration: '43:55'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/Gattaca_decipherSciFi.mp3'
---
#### Why does everyone love this movie?

Underdogs, sticking it to the man, engaging with our sense of justice and living the American Dream! Or something.

#### Genetic Determinism

The world of Gattaca banks on this big-time, but in real it's much more complicated. Nature AND nurture. People are more than the genes they start with.

#### Designer Babies

Current practices with in vitro and in vivo baby testing. Down syndrome.

#### The New Eugenics

Racism. "Positive" and "negative." American eugenics programs. Medical choices are eugenic.

#### Genetic Discrimination

Loooooong list of inconveniences Ethan Hawke has to keep up with to not get caught. Financial arrangements. Colloquial speech in Andrew Niccol's films.
