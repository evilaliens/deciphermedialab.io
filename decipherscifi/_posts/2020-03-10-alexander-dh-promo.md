---
# General post frontmatter
categories:
  - episode
  - bonus
date: 2020-03-10T02:45:05-04:00
layout: podcast_post
permalink: /decipherscifi/on-decipher-history-alexander
slug: on-decipher-history-alexander
title: 'On Decipher History: Alexander'

# Episode particulars
image:
  path: assets/imgs/media/movies/alexander_2004.decipherhistory_promo.jpg
  alt: 'Alexander movie backdrop with Decipher History overlay'
links:
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: ryan_stitt
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/alexander_2004.decipherhistory_promo.jpg
  number: 
  guid: 'https://decipherscifi.com/?p=10947'
  media:
    audio:
      audio/mpeg:
        content_length: 443100
        duration: '31'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/dsf_promo.mp3'

---
The taste of another episode of Decipher History! Featuring Ryan Stitt of the History of Ancient Greece Podcast 🔱

[Alexander: succession, "The Great," and sooo many Alexandrias w/ Ryan Stitt](https://deciphermedia.tv/decipherhistory/7)


