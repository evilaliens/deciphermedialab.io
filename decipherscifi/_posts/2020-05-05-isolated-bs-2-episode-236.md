---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - conversation
date: 2020-05-05T01:05:50-04:00
description: 'MAY-ON-NAISE: the best sauce! Updates on the race between AR and VR. Krakatoa. Volcano energy calculations. Super Mario 64 rom deocmpilation and finding new secrets in a 20-year-old game.'
layout: podcast_post
redirect_from:
  - /decipherscifi/236
slug: 
title: 'Isolated BS part two, the mayonnaisening - volcano edition'
tags:
  - augmented reality
  - computing
  - food
  - programming
  - science
  - science fiction
  - virtual reality
  - volcano

# Episode particulars
image:
  path: assets/imgs/media/misc/isolated_bs_2.jpg
  alt: 'Christopher long-haired selfie in mask mode'
links:
  - text: Splatmoji by Christopher Peterson
    urls:
      - text: GitHub
        url: 'https://github.com/cspeterson/splatmoji'
  - text: 'NVidia RTX Real-Time Noise-Reduction Demo'
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=HA6R9NmZO80'
  - text: Avatarify
    urls:
      - text: GitHub
        url: 'https://github.com/alievk/avatarify'
media:
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/misc/isolated_bs_2.jpg
  number: 236
  guid: 'https://decipherscifi.com/?p=10985'
  media:
    audio:
      audio/mpeg:
        content_length: 37648672
        duration: '44:46'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/isolated_bs2.final.mp3'

---
#### Not just bread anymore..

But also mayonnaise! Definitively "The Best Sauce." [Coolio's Ghettalian Garlic Bread](http://garlicescapes.blogspot.com/2010/11/gangstas-delight.html). Japanese mayonnaise appreciation.

#### Miscellany

[Dune set photos!](https://www.vanityfair.com/hollywood/2020/04/behold-dune-an-exclusive-look-at-timothee-chalamet-zendaya-oscar-isaac). Sting's codpiece. Manly housework. NVidia RTX Real-Time Noise-Reduction Demo: https://www.youtube.com/watch?v=HA6R9NmZO80

#### Gaming and computing

Magic Leap disappointment and waiting for trickle-down from Enterprise. AR vs VR and chaning expectations. Oculus Quest sold-outness. Half-Life Alyx: game changer? [Splatmoji](https://github.com/cspeterson/splatmoji). Programming your way into a Bash-hole.

#### Volcanoes

Volcanoes! Krakatoa exploded while we were all over here trying to figure out face masks. Remebering our [Dracula episode w/ Joe Ruppel]({% link decipherscifi/_posts/2016-10-04-bram-stoker-dracula-joe-ruppel-episode-58.md%}) where Krakatoa brought on the "year without a summer."

#### Rom Decomps

[Super Mario 64 Decompilation](https://github.com/n64decomp/sm64). Decades later, the [smoke fix](https://www.polygon.com/2020/4/6/21210183/super-mario-64-smoke-visual-bug-fire-lava-patch-fix). Figuring out *why* game decompilation is interesting. Appreciating harcore nerds.
