---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-10-22 00:01:51-04:00
description: 'Mel Gibson history movies. The invention of the rocking chair. Smooth-bore and rifled firearms. Militias. The "swamp fox." Gambling away your inheritance. War crimes. Slavery.'
layout: podcast_post
permalink: /decipherhistory/patriot-heroism-slavery-rocking-chairs-episode-6
redirect_from:
  - /decipherhistory/6
slug: patriot-heroism-slavery-rocking-chairs-episode-6
title: 'The Patriot: heroism, invisible slavery, and cutting-edge chair technology'
tags:
  - firearms
  - heroism
  - history
  - mel gibson
  - american revolution
  - technology
  - war crimes

# Episode particulars
image:
  path: assets/imgs/media/movies/the_patriot_2000.jpg
  alt: Mel Gibson as a dramatic colonial-era hero, The Patriot movie backdrop
links:
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-patriot-extended-cut-2000/id606706227?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Patriot-Mel-Gibson/dp/B000O19EW0'
  title: The Patriot
  type: movie
  year: 2000
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_patriot_2000.jpg
  number: 6
  media:
    audio:
      audio/mpeg:
        content_length: 38771846
        duration: '46:04.02'
        explicit: false
        url: 'http://traffic.libsyn.com/decipherscifi/the_patriot.mp3'
---
# People

Roland Emmerich and Mel Gibson and historical rigor (or lack thereof).

# Period technologies

Rocking chairs! Pushing the edge of period chair technology. Actually a relatively new thing, so you could see why Mel might have been having some trouble. Smooth-bore firearms and the eventual transition to Longrifles. Militias vs armies and the frequency of rifled weapons between them. Reload times. [Minié balls](https://en.wikipedia.org/wiki/Mini%C3%A9_ball), loading speed, and rifle accuracy.

# Banistre Tarleton

Maybe the name was too deliciously posh and British? So they changed it to William Tavington. Maybe kind of a jerk, but clearly not the caricature we see in the film. Blowing your inheritence on women and gambling vs buying hundreds of horses. Buying your military commissions whether you deserve it or not.

# Heroism

Should we even try to approach the legitimacy of the "hero?" Humans: always flawed through some lens and operating within their time and experience. 

# Militia

Militias vs armies. The problem in modern armies of humans not *really* wanting to kill other humans.

# War crimes

Ungentlemanly conduct. Continental congress early investigations for propaganda. Zero documented mass-slaughter church-burnings.

# Slavery

Do not be misled: it was happening during this period. Especially in South Carolina. Promises (lies) of post-war freedom. Numbers on the British and Patriot sides.
