---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-06-11 07:45:51+00:00
description: 'Base myths of western civilization. The spartan state. Ancient warfare. The "spikey steamroller" of the phalanx. Taking apart the Spartan mirage. Etc.'
layout: podcast_post
redirect_from:
  - /decipherhistory/2
title: '300: professional soldiers, the Spartan Mirage, and expert poking w/ Josh'
tags:
  - greece
  - historicity
  - history
  - man-meat
  - sparta
  - the ancient world
  - war
  - weapons
  - western civilization

# Episode particulars
image:
  path: assets/imgs/media/movies/300_2007.jpg
  alt: 300 movie backdrop
links:
  - text: Josh's show's on LSG Media
    urls:
      - text: The X-Files Podcast (always)
        url: 'https://www.libertystreetgeek.net/subscribe-xfiles/'
      - text: Science Fiction Film Podcast (sometimes)
        url: 'https://www.libertystreetgeek.net/subscribe-sffp/'
  - text: Hardcore History - King of Kings, by Dan Carlin
    urls:
      - text: Part 1
        url: 'https://www.dancarlin.com/hardcore-history-56-kings-of-kings/'
      - text: Part 2
        url: 'https://www.dancarlin.com/hardcore-history-57-kings-kings-ii/'
      - text: Part 3
        url: 'https://www.dancarlin.com/hardcore-history-58-kings-kings-iii/'
  - text: Gates of Fire by Steven Pressfield
    urls:
      - text: iTunes
        url: 'https://geo.itunes.apple.com/us/book/gates-of-fire/id419969537?mt=11&app=itunes&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Gates-Fire-Novel-Battle-Thermopylae/dp/055338368X/ref=sr_1_1?keywords=gates+of+fire&qid=1560223304&s=gateway&sr=8-1'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/300/id271146652?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/300-Gerard-Butler/dp/B000TYWAUA'
  title: 300
  type: movie
  year: 2007
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: josh_effengee
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/300_2007.jpg
  number: 2
  guid: '002:2019-06-11 07:45:51+00:00'
  media:
    audio:
      audio/mpeg:
        content_length: 58773628
        duration: '01:09:57'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/300_--_josh_effengee_--_decipher_scifi.mp3'
---
#### This film

Zach Snyder's strengths in creating comic book panels on film. Slo-mo blood spatter. Abs and glistening man-meat.


#### Western civilization


Ancient Greece and "western civilization." The birth of "democracy" in nearby Athens. Juxtaposing your historical culture with the "other." Like George Washington crossing the Delaware but with way less man-meat. And 100% less codpieces.


#### Sparta


The dawn on the Spartan state. The nature of the Helot slave class. "Land-bonded" slavery vs chattel. Ancient Greek and Spartan combat methods. Heavy infantry. Training by doing crunches all.day.long. Ancient combat analogues with early UFC.


#### The Persian Invasions


Greece as a poor backwater on the edge of the Persian Empire. The Ionian Revolt, The Battle of Marathon, The Battle of Thermopylae, Battle of Plataea.


#### Professional soldiering


The incredible change that was the development of the professional standing army. Modern soldiers and modern combat compared and contrasted with ancient. Dying gloriously. Army sizes at Thermopylae as recorded closer to the time vs modern estimates.


#### Modern understanding of Sparta


Ancient sources and the lack of writing from the actual place and time. "The Spartan Mirage," and the crafting of the Spartan image. Any training as a unit is better than none.


#### Phalanx


The universality of shield walls and spear hedges, aka the "spiky steamroller." Strategically busting out in individual slow motion. Spear-length evolution. Learning whether "dragoons" are at all related to dragons.
