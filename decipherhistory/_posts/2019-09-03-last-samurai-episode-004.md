---
# General post frontmatter
categories:
  - episode
  - guest co-host
  - movie
date: 2019-09-03 00:45:30+00:00
description: 'Japan emerging from isolation and integrating the west. Saigo Takamori. Seppuku and Bushido code. Bad samurai poetry. And samurai with guns!'
layout: podcast_post
permalink: /decipherhistory/last-samurai-japan-meiji-restoration-takamori-bushido
redirect_from:
  - /decipherhistory/4
slug: last-samurai-japan-meiji-restoration-takamori-bushido
title: 'The Last Samurai: Bushido, European influence, and howizters w/ Isaac Meyer'
tags:
  - empire
  - firearms
  - history
  - honor codes
  - swords
  - weapons
  - western imperialism

# Episode particulars
image:
  path: assets/imgs/media/movies/the_last_samurai_2003.jpg
  alt: Tom Cruise in Samurai armor
links:
  - text: 'The Last Samurai: The Life and Battles of Saigo Takamori by Mark Ravina'
    urls:
      - text: iTunes
        url: 'https://books.apple.com/us/book/the-last-samurai/id431968525?mt=11&app=itunes&at=1001l7hP'
      - text: Amazon
        url: 'https://www.amazon.com/Last-Samurai-Battles-Saigo-Takamori/dp/0471705373'
  - text: Stand Up For Your Rights by History of Japan Podcast
    urls:
      - text: Part 1
        url: 'http://isaacmeyer.net/2019/08/episode-301/'
      - text: Part 2
        url: 'http://isaacmeyer.net/2019/08/episode_302/'
  - text: Bushido Blade
    urls:
      - text: Wikipedia
        url: 'https://en.wikipedia.org/wiki/Bushido_Blade_(video_game)'
  - text: Japanese Milk Bread
    urls:
      - text: NYT Cooking via Google page cache
        url: 'https://webcache.googleusercontent.com/search?q=cache:8QpmpvkFW3oJ:https://cooking.nytimes.com/recipes/1016275-japanese-milk-bread'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/the-last-samurai/id282577271?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/Last-Samurai-Tom-Cruise/dp/B001EBV0OE'
  title: The Last Samurai
  type: movie
  year: 2003
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
  - data_name: isaac_meyer
    roles:
      - guest_cohost
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/the_last_samurai_2003.jpg
  number: 4
  media:
    audio:
      audio/mpeg:
        content_length: 65593331
        duration: '01:17:59.84'
        explicit: false
        url: 'http://traffic.libsyn.com/decipherscifi/the_last_samurai.mp3'
---

[Isaac Meyer's History of Japan Podcast](http://isaacmeyer.net/category/podcasts/history-of-japan-podcast/)!!

## Japan post-isolation

[Tokugawa](https://en.wikipedia.org/wiki/Tokugawa_shogunate) rule. Can historical economics be interesting? The introduction of tightly-controlled Dutch trade in Japan preceeding the Meiji restoration. Western opposition. Fighting over western presence and figuring out *how much* Western influence Japan can tolerate while still being Japanese. The return of the Emperor.

## Europeans in Japan

Recognizing the relative militrary reputations of Britain, France, Germany, and the US at the time. French military advisors in irl Japan during the Meiji restoration. [Jules Brunet](https://en.wikipedia.org/wiki/Jules_Brunet) as the closest real-life analogue to Tom Cruise's Algren. Spheres of influence. 

## Samurai

War basically between and within the Samurai class. Samurai with side jobs. Disbanding the social class with the swords. Samurai civil war armor and its increasing disutility as firearms improve. The point of the elaborate headpieces.

{% responsive_image path: assets/imgs/misc/samurai_with_sword.jpg alt: "Samurai with a Katana c. 1860" caption: "Samurai with a Katana c. 1860" attr_text: "Felice Beato, public domain" attr_url: "https://commons.wikimedia.org/wiki/File:Samurai_with_sword.jpg" %}

# Saigō Takamori

The irl analogue for Ken Watanabe's character. Trying to fight Korea. Accidentally creating revolutionaries. Takamori's last stand after his forces *ran out of bullets*. "The Last Samurai" and the ease of Japanese punning.

{% responsive_image path: assets/imgs/misc/saigo_takamori.jpg alt: "Saigo Takamori before 1877" caption: "Saigo Takamori before 1877" attr_text: "Edoardo Chiossone, public domain" attr_url: "https://commons.wikimedia.org/wiki/File:Saigo_Takamori.jpg" %}

# Foreign samurai

English-born "Samurai" and how/whether a foreigner could actually become a *real* samurai. The (probably) African-born "samurai" [Yasuke](https://en.wikipedia.org/wiki/Yasuke), working as personal bodyguard for daimyō Nobunaga.

# Bushido

Creating your warrior narrative *after* your period of real marshall utility. Analogy with European chivalry. The carrying of Bushido culture from Samurai time into WWII Japan. The circumstances where ritual suicide begins to seem like a reasonable option.

# Guns

[Arqebuses](https://en.wikipedia.org/wiki/Arquebus) all over the joint before the period of the film. Samurai gun-kata. Wooden cannons, howitzers, and artillery classification.
