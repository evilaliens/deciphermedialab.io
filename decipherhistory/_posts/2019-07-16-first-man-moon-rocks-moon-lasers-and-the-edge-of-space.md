---
# General post frontmatter
categories:
  - episode
  - just the two of us
  - movie
date: 2019-07-16 07:45:26+00:00
description: 'From first flight to a moon landing in only a few decades. Apollo TV signal encoding. Moon rocks. Lunar laser ranging and confirming relativity.'
layout: podcast_post
redirect_from:
  - /decipherhistory/3
title: 'First Man: moon rocks, moon lasers, and the edge of space'
tags:
  - astronauts
  - geology
  - nasa
  - optical illusions
  - radio
  - science
  - space

# Episode particulars
image:
  path: assets/imgs/media/movies/first_man_2018.jpg
  alt: First Man movie backdrop
links:
  - text: Where does NASA keep the Moon Rocks? by Smarter Every Day
    urls:
      - text: YouTube
        url: 'https://www.youtube.com/watch?v=QxZ_iPldGtI'
  - text: Apollo Television by Bill Wood
    urls:
      - text: NASA
        url: 'https://www.hq.nasa.gov/alsj/ApolloTV-Acrobat5.pdf'
media:
  links:
    - text: iTunes
      url: 'https://geo.itunes.apple.com/us/movie/first-man/id1437642635?mt=6&at=1001l7hP'
    - text: Amazon
      url: 'https://www.amazon.com/First-Man-Ryan-Gosling/dp/B07J264WXC'
  title: First Man
  type: movie
  year: 2018
people:
  - data_name: christopher_peterson
    roles:
      - host
  - data_name: lee_colbert
    roles:
      - host
sponsors:

# Media (podcast) particulars
episode:
  cover_image: assets/imgs/media/movies/first_man_2018.jpg
  number: 3
  guid: '003:2019-07-16 07:45:26+00:00'
  media:
    audio:
      audio/mpeg:
        content_length: 35180643
        duration: '41:52'
        explicit: false
        url: 'https://traffic.libsyn.com/decipherscifi/first_man_--_--_decipher_scifi.mp3'
---
## One Small Step

Worrying the minimum amount about your speech. The difficulty of quoting noisy radio transmissions.

## Because it is haaaahd

Recognizing the small temporal distance from the _first powered flight_ to the first moon landing. The cutting edge of the early space program. Test piloting. Gemini.

## The edge of space

Defining the edge of space. The "Karman Line": transition from atmospheric lift to orbital velocity. Complications and redefinition of where "space" begins. Geopolitics, ruining everything since forever.

## The "right stuff"

Badass engineer pilots. Moving fast and breaking things. Selection testing. Giving prospective astronauts ice-water wet willies. The importance of simulation in the early space program and the difficulty of simulating things we haven't actually ever done or seen up close.How hard it really is to stay conscious under high-g stress.

{% youtube "https://youtu.be/DMKcO-T5Y4o?t=166" %}

## Moon landing

Monocular depth cues. Light and shadow, unfamiliar objects, and depth perception. Equatorial noon on the equinox when stuff looks creepy: [Lahaina Noon](https://en.wikipedia.org/wiki/Lahaina_Noon).

## Lunar Laser Ranging Experiment

Retroreflectors and really really powerful lasers. Tiny photonic returns: 1 out of every 10¹⁷ photons shot at the moon mirror make it back for our detection. Multi-mile laser beams. Confirming relativity ftw.

## ROCKS

... from the moon! And some regolith to boot. Vacuum transport for moon samples and how we work with them on Earth's surface without contaminating. The difficulty of maintaining a a very strong vacuum vs nonreactive gasses. Detecting the provenance of proposed moon rocks. NASA's moon-rock cataloguing system.

## Moon-landing video

Viewership numbers. NASA's custom video encoding and the incredibly analog conversion methods employed to bring it to television.

## What if

What if it didn't work out? The Nixon speach made ready _just_ in case. "[In Event of Moon Disaster](https://www.archives.gov/files/presidential-libraries/events/centennials/nixon/images/exhibit/rn100-6-1-2.pdf)."

## What now

Why we have no rockets now to match the power of the Saturn V. Loss of engine-production expertise. Looking at near-future Moon and Mars missions.
