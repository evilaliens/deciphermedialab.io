$( document ).ready(function() {
  // Get query string from url params
  const query = decodeURIComponent($.urlParam('query'));
  // Set the search box in the document body to show existing search if given
  if (query != 0) {
    $('#body-searchbox').val(query);
  }

  // Arrange search from index
  let index = new FlexSearch('speed');
  let i;
  let addstr;
  for (const item of SearchIndex) {
    // addstr = [item['categories'], item['content'], item['links'], item['media'], item['people'], item['tags'], item['title']].join(' ');
    addstr = serialize(item);
    index.add(item['id'], addstr);
  }

  // Search
  let results = index.search(query)

  // Collect results into something displayable
  let data = [];
  let item;
  for (const id of results) {
    item = SearchIndex[id];
    result_str = serialize(item);
    excerpt = excerpt_string(result_str, query, 40);
    data.push(`
      <li class="mb-3">
        <a href="${item['permalink']}">${item['title']}</a>:<br>
          ${excerpt}
      </li>
    `);
  }
  $('#search-results').append(data.join(''));
});

function serialize(obj) {
  // Stringify an dict's values
  var str = [];
  for(var p in obj)
    if (obj.hasOwnProperty(p)) {
      str.push(obj[p]);
    }
  return str.join("&");
}

function excerpt_string(string, substring, dist) {
  // Cut a chunk from a string around a substring
  substring_idx = string.toLowerCase().indexOf(substring.toLowerCase());
  start = substring_idx - dist;
  if (start < 0) {
    start = 0;
  }
  length = dist + substring.length + dist;
  excerpt = `... ${[string.substr(start, substring_idx - start), '<strong>', string.substr(substring_idx, substring.length), '</strong>', string.substr(substring_idx + substring.length, dist)].join('')} ...`;

  return excerpt
}

$.urlParam = function(name){
  // Get url params
  let results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
  return results[1] || 0;
}
